<?php

use components\Router;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

session_start();

define('ROOT', __DIR__);

require ROOT . '/vendor/autoload.php';

$config = Setup::createAnnotationMetadataConfiguration([ROOT . '/src'], true, null, null, false);
$conn = array(
    'driver' => 'pdo_mysql',
    'dbname' => 'fitness',
    'user' => 'root',
    'password' => '',
);
$entityManager = EntityManager::create($conn, $config);

$router = new Router($entityManager);
$router->call();
