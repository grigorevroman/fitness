<?php

return [
    // возможность управления услугами
    '/admin/service/create/' => 'adminService/create',
    '/admin/service/update/([0-9]+)/' => 'adminService/update/$1',
    '/admin/service/delete/([0-9]+)/' => 'adminService/delete/$1',
    '/admin/service/' => 'adminService/read',

    // возможность управления залами
    '/admin/gym/create/' => 'adminGym/create',
    '/admin/gym/update/([0-9]+)/' => 'adminGym/update/$1',
    '/admin/gym/delete/([0-9]+)/' => 'adminGym/delete/$1',
    '/admin/gym/' => 'adminGym/read',

    // возможность управления тренерами
    '/admin/trainer/create/' => 'adminTrainer/create',
    '/admin/trainer/update/([0-9]+)/' => 'adminTrainer/update/$1',
    '/admin/trainer/delete/([0-9]+)/' => 'adminTrainer/delete/$1',
    '/admin/trainer/' => 'adminTrainer/read',

    // возможность управления пользователями
    '/admin/user/create/' => 'adminUser/create',
    '/admin/user/update/([0-9]+)/' => 'adminUser/update/$1',
    '/admin/user/delete/([0-9]+)/' => 'adminUser/delete/$1',
    '/admin/user/order/([0-9]+)/' => 'adminUser/order/$1',
    '/admin/user/' => 'adminUser/read',

    // возможность управления расписанием
    '/admin/timing/create/' => 'adminTiming/create',
    '/admin/timing/update/([0-9]+)/' => 'adminTiming/update/$1',
    '/admin/timing/delete/([0-9]+)/' => 'adminTiming/delete/$1',
    '/admin/timing/' => 'adminTiming/read',

    // возможность управления админ панелью
    '/admin/' => 'admin/main',

    // возможность управления расписанием для менеджера
    '/manager/timing/create/' => 'managerTiming/create',
    '/manager/timing/update/([0-9]+)/' => 'managerTiming/update/$1',
    '/manager/timing/delete/([0-9]+)/' => 'managerTiming/delete/$1',
    '/manager/timing/' => 'managerTiming/read',

    // страницы доступные только для тренера (страницы для управления аккаута тренера)
    '/trainer/create' => 'trainer/create',
    '/trainer/read' => 'trainer/read',
    '/trainer/update' => 'trainer/update',
    '/trainer/delete' => 'trainer/delete',

    // авторизация, выход, регистрация, изменение данных о своем юзере
    '/user/login/' => 'user/login',
    '/user/logout/' => 'user/logout',
    '/user/registration/' => 'user/registration',
    '/user/update/' => 'user/update',
    '/user/order/' => 'user/order',
    '/user/' => 'user/main',

    // корзина
    //'/cart/checkout/' => 'cart/checkout',
    //'/cart/delete/([0-9]+)/' => 'cart/delete/$1',
    //'/cart/add/([0-9]+)/' => 'cart/add/$1',
    //'/cart/add_ajax/([0-9]+)/' => 'cart/addAjax/$1',
    //'/cart/' => 'cart/index',

    // просмотр услуг и просмотр одной услуги
    '/service/([0-9]+)/' => 'service/detail/$1',
    '/service/' => 'service/main',

    // просмотр зала и просмотр одного зала
    '/gym/([0-9]+)/' => 'gym/detail/$1',
    '/gym/' => 'gym/main',

    // просмотр тренера и просмотр одного тренера
    '/trainer/([0-9]+)/' => 'trainer/detail/$1',
    '/trainer/' => 'trainer/main',

    // просмотр расписания любым пользователем
    '/timetable/' => 'timetable/main',

    // о нас
    '/about/' => 'main/about',

    // отчет
    '/report/' => 'report/main',

    // главная
    '/' => 'main/main',
];
