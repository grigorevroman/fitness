<?php

namespace components;

use models\User;

abstract class Manager
{
    public static function checkManager(\Doctrine\ORM\EntityManager $em)
    {
        $user = User::getUserBySession($em);

        if ($user->getRole() === User::$roles[User::MANAGER] && $user->getActive()) {
            return true;
        }

        die('Доступ запрещен');
    }

}
