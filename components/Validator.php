<?php

namespace components;

class Validator
{
    public static function checkNameAndCode($name, $code, $text = 'Название')
    {
        $error = null;
        if (!$name || !$code) {
            $error = 'Поле "' . $text . '" обязательно для заполнения';
        } else {
            if (!preg_match("/^[0-9A-Za-zА-Яа-я- ]+$/u", $name) || !preg_match("/^[0-9a-z-_]+$/", $code)) {
                $error = 'Допутимые символы для поля "' . $text . '" "/^[0-9A-Za-zА-Яа-я- ]+$/u"';
            }
        }
        return $error;
    }

    public static function checkNumber($number, $name)
    {
        $error = null;
        if (!is_numeric($number) && !empty($number)) {
            $error = 'Поле "' . $name . '" должно быть числовым';
        }
        return $error;
    }

    public static function checkRequired($value, $name)
    {
        $error = null;
        if (!$value) {
            $error = 'Поле "' . $name . '" обязательно для заполнения';
        }
        return $error;
    }

    public static function checkPassword($password)
    {
        $error = null;
        if (!$password) {
            $error = 'Поле "Пароль" обязательно для заполнения';
        } else {
            if (!preg_match("/^[0-9A-Za-z]+$/u", $password)) {
                $error = 'Допутимые символы для поля "Пароль" "/^[0-9A-Za-z]+$/u"';
            } else {
                if (strlen($password) < 6) {
                    $error = 'Поле "Пароль" не должно быть короче 6 символов';
                }
            }
        }
        return $error;
    }

    public static function checkEmail($email)
    {
        $error = null;
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $error = 'Поле "E-mail" должно иметь формат электронной почты';
        }
        return $error;
    }
}
