<?php

namespace components;

/**
 * Класс Cart
 * Компонент для работы корзиной
 */
class Cart
{

    /**
     * Добавление товара в корзину (сессию)
     * @param int $id <p>id товара</p>
     * @return integer <p>Количество товаров в корзине</p>
     */
    public static function addService($id)
    {
        // Приводим $id к типу integer
        $id = intval($id);

        // Пустой массив для товаров в корзине
        $servicesInCart = array();

        // Если в корзине уже есть товары (они хранятся в сессии)
        if (isset($_SESSION['services'])) {
            // То заполним наш массив товарами
            $servicesInCart = $_SESSION['services'];
        }

        // Проверяем есть ли уже такой товар в корзине
        if (array_key_exists($id, $servicesInCart)) {
            // Если такой товар есть в корзине, но был добавлен еще раз, увеличим количество на 1
            $servicesInCart[$id] ++;
        } else {
            // Если нет, добавляем id нового товара в корзину с количеством 1
            $servicesInCart[$id] = 1;
        }

        // Записываем массив с товарами в сессию
        $_SESSION['services'] = $servicesInCart;

        // Возвращаем количество товаров в корзине
        return self::countItems();
    }

    /**
     * Подсчет количество товаров в корзине (в сессии)
     * @return int <p>Количество товаров в корзине</p>
     */
    public static function countItems()
    {
        // Проверка наличия товаров в корзине
        if (isset($_SESSION['services'])) {

            // Если массив с товарами есть
            // Подсчитаем и вернем их количество
            $count = 0;
            foreach ($_SESSION['services'] as $id => $quantity) {
                $count = $count + $quantity;
            }
            return $count;
        } else {
            // Если товаров нет, вернем 0
            return 0;
        }
    }

    public static function getServices()
    {
        if (isset($_SESSION['services'])) {
            return $_SESSION['services'];
        }
        return false;
    }

    public static function getTotalPrice(array $services)
    {
        // Получаем массив с идентификаторами и количеством товаров в корзине
        $servicesInCart = self::getServices();

        // Подсчитываем общую стоимость
        $total = 0;
        if ($servicesInCart) {
            // Если в корзине не пусто
            // Проходим по переданному в метод массиву товаров
            /** @var \entities\Service $item */
            foreach ($services as $item) {
                // Находим общую стоимость: цена товара * количество товара
                $total += $item->getPriceDay() * $servicesInCart[$item->getID()];
            }
        }

        return $total;
    }

    /**
     * Очищает корзину
     */
    public static function clear()
    {
        if (isset($_SESSION['services'])) {
            unset($_SESSION['services']);
        }
    }

    /**
     * Удаляет товар с указанным id из корзины
     * @param integer $id <p>id товара</p>
     */
    public static function deleteService($id)
    {
        // Получаем массив с идентификаторами и количеством товаров в корзине
        $servicesInCart = self::getServices();

        // Удаляем из массива элемент с указанным id
        unset($servicesInCart[$id]);

        // Записываем массив товаров с удаленным элементом в сессию
        $_SESSION['services'] = $servicesInCart;
    }

}
