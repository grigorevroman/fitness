<?php

namespace components;

use Doctrine\ORM\EntityManager;
use models\User;

abstract class Trainer
{
    public static function checkTrainer(EntityManager $em)
    {
        $user = User::getUserBySession($em);

        if ($user->getRole() === User::$roles[User::TRAINER] && $user->getActive()) {
            return true;
        }

        die('Доступ запрещен');
    }

}
