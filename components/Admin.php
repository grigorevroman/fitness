<?php

namespace components;

use Doctrine\ORM\EntityManager;
use models\User;

abstract class Admin
{
    public static function checkAdmin(EntityManager $em)
    {
        $user = User::getUserBySession($em);

        if ($user->getRole() === User::$roles[User::ADMIN] && $user->getActive()) {
            return true;
        }

        die('Доступ запрещен');
    }

}
