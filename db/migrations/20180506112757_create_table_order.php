<?php


use Phinx\Migration\AbstractMigration;

class CreateTableOrder extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('order');
        $table
            ->addColumn('userID', 'integer', ['null' => true])
            ->addColumn('serviceID', 'integer', ['null' => true])
            ->addColumn('gymID', 'integer', ['null' => true])
            ->addColumn('trainerID', 'integer', ['null' => true])
            ->addColumn('price', 'integer', ['null' => true])
            ->addColumn('duration', 'enum', ['values' => ['day', 'month', 'half_year', 'year'], 'null' => true])
            ->addColumn('dateBegin', 'date', ['null' => true])
            ->create();
    }
}
