<?php

use Phinx\Migration\AbstractMigration;

class CreateTableTiming extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('timing');
        $table
            ->addColumn('name', 'string')
            ->addColumn('code', 'string')
            ->addColumn('active', 'enum', ['values' => [1, 0], 'null' => true])
            ->addColumn('sortable', 'integer', ['null' => true])
            ->addColumn('dates', 'string', ['null' => true])
            ->addColumn('serviceID', 'integer', ['null' => true])
            ->addColumn('startTime', 'string', ['null' => true])
            ->addColumn('endTime', 'string', ['null' => true])
            ->addColumn('gymID', 'integer', ['null' => true])
            ->addColumn('trainerID', 'integer', ['null' => true])
            ->create();
    }
}
