<?php

namespace models;

use components\DB;

class Day
{
    public static function create(\entities\Day $day)
    {
        if ($day) {

            $dbh = DB::getConnection();
            $sql = "INSERT INTO day (name, code, active, sortable) " .
                "VALUES (?, ?, ?, ?)";
            $stmt = $dbh->prepare($sql);

            $stmt->execute([
                $day->getName(),
                $day->getCode(),
                $day->getActive(),
                $day->getSortable(),
            ]);

            return $dbh->lastInsertId();
        }
        return false;
    }

    public static function update(\entities\Day $day)
    {
        if ($day) {

            $dbh = DB::getConnection();
            $sql = "UPDATE day SET name = ?, code = ?, dateChange = NOW(), active = ?, sortable = ? WHERE id = ?";
            $stmt = $dbh->prepare($sql);

            $result = $stmt->execute([
                $day->getName(),
                $day->getCode(),
                $day->getActive(),
                $day->getSortable(),
                $day->getID(),
            ]);

            return $result;
        }
        return false;
    }

    public static function delete(\entities\Day $day)
    {
        if ($day) {

            $dbh = DB::getConnection();
            $sql = "DELETE FROM day WHERE id = ?";
            $stmt = $dbh->prepare($sql);
            $result =  $stmt->execute([$day->getID()]);

            return $result;
        }
        return false;
    }

    public static function getDays()
    {
        $dbh = DB::getConnection();
        $sql = "SELECT * FROM day ORDER BY sortable ASC";
        $stmt = $dbh->query($sql);
        if ($stmt) {
            $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);
            return self::getDaysByRows($rows);
        }
        return [];
    }

    public static function getDayByID($id)
    {
        if ($id) {

            $dbh = DB::getConnection();
            $sql = "SELECT * FROM day WHERE id = ?";
            $stmt = $dbh->prepare($sql);
            $stmt->execute([$id]);
            $rows[] = $stmt->fetch(\PDO::FETCH_OBJ);
            $days = self::getDaysByRows($rows);

            if ($day = array_shift($days)) {
                return $day;
            }
        }
        return false;
    }

    public static function getDaysByRows($rows)
    {
        $days = [];
        if (count($rows)) {
            foreach ($rows as $row) {
                if ($row->id) {
                    $days[$row->id] = new \entities\Day(
                        $row->id,
                        $row->name,
                        $row->code,
                        $row->dateCreate,
                        $row->dateChange,
                        $row->active,
                        $row->sortable
                    );
                }
            }
        }
        return $days;
    }

    public static function getCurrentWeek()
    {
        $week = [];

        $date1 = new \DateTime();
        $week[] = $date1->format('Y-m-d');

        $date2 = new \DateTime();
        $date2->modify('+1 day');
        $week[] = $date2->format('Y-m-d');

        $date2 = new \DateTime();
        $date2->modify('+2 day');
        $week[] = $date2->format('Y-m-d');

        $date2 = new \DateTime();
        $date2->modify('+3 day');
        $week[] = $date2->format('Y-m-d');

        $date2 = new \DateTime();
        $date2->modify('+4 day');
        $week[] = $date2->format('Y-m-d');

        $date2 = new \DateTime();
        $date2->modify('+5 day');
        $week[] = $date2->format('Y-m-d');

        $date2 = new \DateTime();
        $date2->modify('+6 day');
        $week[] = $date2->format('Y-m-d');

        return $week;
    }
}