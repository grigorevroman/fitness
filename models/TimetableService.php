<?php

namespace models;

use components\DB;
use Doctrine\ORM\EntityManager;

class TimetableService
{
    public static function create(\entities\TimetableService $timetableService)
    {
        if ($timetableService) {

            $dbh = DB::getConnection();
            $sql = "INSERT INTO timetable_service (name, code, active, sortable, serviceID) " .
                "VALUES (?, ?, ?, ?, ?)";
            $stmt = $dbh->prepare($sql);

            $stmt->execute([
                $timetableService->getName(),
                $timetableService->getCode(),
                $timetableService->getActive(),
                $timetableService->getSortable(),
                $timetableService->getServiceID(),
            ]);

            $timetableService->setID($dbh->lastInsertId());

            return $timetableService->getID();
        }
        return false;
    }

    public static function getTimetableServices(EntityManager $em)
    {
        $dbh = DB::getConnection();
        $sql = "SELECT * FROM timetable_service ORDER BY sortable ASC";
        $stmt = $dbh->query($sql);
        $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);
        return self::getTimetableServiceByRows($rows, $em);
    }

    public static function getTimetableServiceByRows($rows, EntityManager $em)
    {
        $timetableServices = [];
        if (count($rows)) {
            foreach ($rows as $row) {
                if ($row->id) {
                    $timetableServices[$row->id] = new \entities\TimetableService(
                        $row->id,
                        $row->name,
                        $row->code,
                        $row->active,
                        $row->sortable,
                        $row->serviceID,
                        $em->find('src\Service', $row->serviceID)
                    );
                }
            }
        }
        return $timetableServices;
    }
}
