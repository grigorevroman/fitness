<?php

namespace models;

use components\DB;

class Record
{
    public static function create(\entities\Record $record)
    {
        if ($record) {

            $dbh = DB::getConnection();
            $sql = "INSERT INTO record (serviceID, timingID, orderID, userID, date) " .
                "VALUES (?, ?, ?, ?, ?)";
            $stmt = $dbh->prepare($sql);

            $stmt->execute([
                $record->getServiceID(),
                $record->getTimingID(),
                $record->getOrderID(),
                $record->getUserID(),
                $record->getDate(),
            ]);

            return $dbh->lastInsertId();
        }
        return false;
    }

    public static function delete(\entities\Record $record)
    {
        if ($record) {
            $dbh = DB::getConnection();
            $sql = "DELETE FROM record WHERE id = ?";
            $stmt = $dbh->prepare($sql);
            return $stmt->execute([$record->getID()]);
        }
        return false;
    }

    public static function getRecordByData($serviceID, $timingID, $date, $userID, $orderID)
    {
        $dbh = DB::getConnection();
        $sql = "SELECT * FROM record WHERE serviceID = ? AND timingID = ? AND date = ? AND userID = ? AND orderID = ?";
        $stmt = $dbh->prepare($sql);
        $stmt->execute([$serviceID, $timingID, $date, $userID, $orderID]);
        $rows[] = $stmt->fetch(\PDO::FETCH_OBJ);
        $records = self::getRecordByRows($rows);
        if ($record = array_shift($records)) {
            return $record;
        }
        return false;
    }

    public static function n($date, $timing)
    {
        $dbh = DB::getConnection();
        $sql = "SELECT count(*) FROM record WHERE `date` = ? AND timingID = ?";
        $stmt = $dbh->prepare($sql);
        $stmt->execute([$date, $timing]);
        $rows = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $rows['count(*)'];
    }

    public static function getRecordByRows($rows)
    {
        $records = [];
        if (count($rows)) {
            foreach ($rows as $row) {
                if ($row->id) {
                    $records[$row->id] = new \entities\Record(
                        $row->id,
                        $row->orderID,
                        $row->serviceID,
                        $row->timingID,
                        $row->userID,
                        $row->date
                    );
                }
            }
        }
        return $records;
    }

    public static function getRecords()
    {
        $dbh = DB::getConnection();
        $sql = "SELECT * FROM `record` ORDER BY id DESC";
        $stmt = $dbh->query($sql);
        $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);
        $orders = self::getRecordByRows($rows);
        return $orders;
    }
}
