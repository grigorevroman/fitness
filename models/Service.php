<?php

namespace models;

use components\DB;
use Doctrine\ORM\EntityManager;

class Service
{
    const STANDARD = 'standard';
    const PERSONAL = 'personal';
    const GROUPS = 'groups';

    public static $types = [
        self::STANDARD => 'Стандартная тренеровка',
        self::PERSONAL => 'Персональная тренеровка',
        self::GROUPS => 'Групповая тренеровка',
    ];

    public static function create(\entities\FullService $service)
    {
        if ($service) {

            $dbh = DB::getConnection();
            $sql = "INSERT INTO service (name, code, active, sortable, intro, content, preview, type, priceDay, priceMonth, priceHalfYear, priceYear) " .
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $stmt = $dbh->prepare($sql);

            $stmt->execute([
                $service->getName(),
                $service->getCode(),
                $service->getActive(),
                $service->getSortable(),
                $service->getIntro(),
                $service->getContent(),
                $service->getPreview(),
                $service->getType(),
                $service->getPriceDay(),
                $service->getPriceMonth(),
                $service->getPriceHalfYear(),
                $service->getPriceYear()
            ]);

            $service->setID($dbh->lastInsertId());

            if (count($service->getGyms())) {
                /** @var \entities\Gym $gym */
                foreach ($service->getGyms() as $gym) {
                    $sql = "INSERT INTO relation_service_and_gym (service_id, gym_id) VALUES (?, ?)";
                    $stmt = $dbh->prepare($sql);
                    $stmt->execute([$service->getID(), $gym->getID()]);
                }
            }

            if (count($service->getTrainers())) {
                /** @var \entities\Trainer $trainer */
                foreach ($service->getTrainers() as $trainer) {
                    $sql = "INSERT INTO relation_service_and_trainer (service_id, trainer_id) VALUES (?, ?)";
                    $stmt = $dbh->prepare($sql);
                    $stmt->execute([$service->getID(), $trainer->getID()]);
                }
            }

            if ($service->getPreview()) {
                $preview = json_decode($service->getPreview(), true);
                move_uploaded_file($preview['tmp_name'], ROOT . '/upload/' . $preview['name']);
            }

            return $service->getID();
        }
        return false;
    }

    public static function update(\entities\FullService $service)
    {
        if ($service) {

            $dbh = DB::getConnection();
            $sql = "UPDATE service SET name = ?, code = ?, dateChange = NOW(), active = ?, sortable = ?, intro = ?, content = ?, type = ?, priceDay = ?, priceMonth = ?, priceHalfYear = ?, priceYear = ? WHERE id = ?";
            $stmt = $dbh->prepare($sql);

            $result = $stmt->execute([
                $service->getName(),
                $service->getCode(),
                $service->getActive(),
                $service->getSortable(),
                $service->getIntro(),
                $service->getContent(),
                $service->getType(),
                $service->getPriceDay(),
                $service->getPriceMonth(),
                $service->getPriceHalfYear(),
                $service->getPriceYear(),
                $service->getID(),
            ]);

            $sql = "DELETE FROM relation_service_and_gym WHERE service_id = ?";
            $stmt = $dbh->prepare($sql);
            $stmt->execute([$service->getID()]);

            $sql = "DELETE FROM relation_service_and_trainer WHERE service_id = ?";
            $stmt = $dbh->prepare($sql);
            $stmt->execute([$service->getID()]);

            if (count($service->getGyms())) {
                /** @var \entities\Gym $gym */
                foreach ($service->getGyms() as $gym) {
                    $sql = "INSERT INTO relation_service_and_gym (service_id, gym_id) VALUES (?, ?)";
                    $stmt = $dbh->prepare($sql);
                    $stmt->execute([$service->getID(), $gym->getID()]);
                }
            }

            if (count($service->getTrainers())) {
                /** @var \entities\Trainer $trainer */
                foreach ($service->getTrainers() as $trainer) {
                    $sql = "INSERT INTO relation_service_and_trainer (service_id, trainer_id) VALUES (?, ?)";
                    $stmt = $dbh->prepare($sql);
                    $stmt->execute([$service->getID(), $trainer->getID()]);
                }
            }

            return $result;
        }
        return false;
    }

    public static function delete(\src\Service $service, EntityManager $em)
    {
        if ($service) {

            $id = $service->getId();

            $dbh = DB::getConnection();

            $sql = "DELETE FROM relation_service_and_gym WHERE service_id = ?";
            $stmt = $dbh->prepare($sql);
            $stmt->execute([$service->getID()]);

            $sql = "DELETE FROM relation_service_and_trainer WHERE service_id = ?";
            $stmt = $dbh->prepare($sql);
            $stmt->execute([$service->getID()]);

            $em->remove($service);
            $em->flush();

            return $id;
        }
        return false;
    }

    public static function getFullServiceByID($id, EntityManager $em)
    {
        if ($id) {

            $service = $em->find('src\Service', $id);

            if ($service) {

                $gyms = Gym::getGymsByService($service);
                $trainers = Trainer::getTrainersByService($service);

                return new \entities\FullService(
                    $service->getID(),
                    $service->getName(),
                    $service->getCode(),
                    $service->getDateCreate(),
                    $service->getDateChange(),
                    $service->getActive(),
                    $service->getSortable(),
                    $service->getIntro(),
                    $service->getContent(),
                    $service->getPreview(),
                    $service->getType(),
                    $service->getPriceDay(),
                    $service->getPriceMonth(),
                    $service->getPriceHalfYear(),
                    $service->getPriceYear(),
                    $gyms,
                    $trainers
                );
            }
        }
        return null;
    }

    public static function getServicesByGym(\src\Gym $gym)
    {
        $services = [];

        if ($gym) {

            $dbh = DB::getConnection();
            $sql = "select service.* from service " .
                "left join relation_service_and_gym on relation_service_and_gym.service_id = service.id " .
                "left join gym on gym.id = relation_service_and_gym.gym_id " .
                "where gym.id = ?";

            $stmt = $dbh->prepare($sql);
            $stmt->execute([$gym->getID()]);
            $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);
            $services = self::getServicesByRows($rows);
        }
        return $services;
    }

    public static function getServicesByIDs($ids = [])
    {
        $services = [];

        if (count($ids)) {

            $placeholder = null;
            foreach ($ids as $id) {
                $placeholder .= '?,';
            }
            $placeholder = trim($placeholder, ',');

            $dbh = DB::getConnection();
            $sql = "SELECT * FROM service WHERE id IN ($placeholder)";
            $stmt = $dbh->prepare($sql);
            $stmt->execute($ids);
            $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);
            $services = self::getServicesByRows($rows);
        }
        return $services;
    }

    public static function getServicesByRows(array $rows)
    {
        $services = [];
        if (count($rows)) {
            foreach ($rows as $row) {
                if ($row->id) {
                    $services[$row->id] = new \entities\Service(
                        $row->id,
                        $row->name,
                        $row->code,
                        $row->dateCreate,
                        $row->dateChange,
                        $row->active,
                        $row->sortable,
                        $row->intro,
                        $row->content,
                        $row->preview,
                        $row->type,
                        $row->priceDay,
                        $row->priceMonth,
                        $row->priceHalfYear,
                        $row->priceYear
                    );
                }
            }
        }
        return $services;
    }

    public static function getFullServicesByIDs(array $ids, EntityManager $em)
    {
        $fullServices = [];
        if (count($ids)) {
            foreach ($ids as $id) {
                $fullServices[] = self::getFullServiceByID($id, $em);
            }
        }
        return $fullServices;
    }
}
