<?php

namespace models;

use components\DB;
use Doctrine\ORM\EntityManager;

class Trainer
{
    public static function update(\entities\Trainer $trainer)
    {
        if ($trainer) {

            $dbh = DB::getConnection();
            $sql = "UPDATE trainer SET " .
                "name = ?, " .
                "code = ?, " .
                "dateChange = NOW(), " .
                "active = ?, " .
                "sortable = ?, " .
                "intro = ?, " .
                "content = ?, " .
                "userID = ? " .
                "WHERE id = ?";
            $stmt = $dbh->prepare($sql);

            return $stmt->execute([
                $trainer->getName(),
                $trainer->getCode(),
                $trainer->getActive(),
                $trainer->getSortable(),
                $trainer->getIntro(),
                $trainer->getContent(),
                $trainer->getUserID(),
                $trainer->getID(),
            ]);
        }
        return false;
    }

    public static function getTrainers()
    {
        $dbh = DB::getConnection();
        $sql = "SELECT * FROM trainer ORDER BY sortable ASC";
        $stmt = $dbh->query($sql);
        $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);
        return self::getTrainersByRows($rows);
    }

    public static function getTrainersByService(\entities\Service $service)
    {
        $trainers = [];

        if ($service) {

            $dbh = DB::getConnection();
            $sql = "select trainer.* from trainer " .
                "left join relation_service_and_trainer on relation_service_and_trainer.trainer_id = trainer.id " .
                "left join service on service.id = relation_service_and_trainer.service_id " .
                "where service.id = ?";

            $stmt = $dbh->prepare($sql);
            $stmt->execute([$service->getID()]);
            $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);
            $trainers = self::getTrainersByRows($rows);
        }
        return $trainers;
    }

    public static function getTrainersByRows(array $rows)
    {
        $trainers = [];
        if (count($rows)) {
            foreach ($rows as $row) {
                if ($row->id) {
                    $trainers[$row->id] = new \entities\Trainer(
                        $row->id,
                        $row->name,
                        $row->code,
                        $row->dateCreate,
                        $row->dateChange,
                        $row->active,
                        $row->sortable,
                        $row->intro,
                        $row->content,
                        $row->preview,
                        $row->userID
                    );
                }
            }
        }
        return $trainers;
    }
}