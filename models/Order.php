<?php

namespace Models;

use Components\DB;
use Doctrine\ORM\EntityManager;

/**
 * Класс Order - модель для работы с заказами
 */
class Order
{

    /**
     * Сохранение заказа
     * @param string $userName <p>Имя</p>
     * @param string $userPhone <p>Телефон</p>
     * @param string $userComment <p>Комментарий</p>
     * @param integer $userId <p>id пользователя</p>
     * @param array $products <p>Массив с товарами</p>
     * @return boolean <p>Результат выполнения метода</p>
     */
    public static function save($userName, $userPhone, $userComment, $userId, $products)
    {
        // Соединение с БД
        $db = DB::getConnection();

        // Текст запроса к БД
        $sql = 'INSERT INTO order (user_name, user_phone, user_comment, user_id, products) '
            . 'VALUES (:user_name, :user_phone, :user_comment, :user_id, :products)';

        $products = json_encode($products);

        $result = $db->prepare($sql);
        $result->bindParam(':user_name', $userName, \PDO::PARAM_STR);
        $result->bindParam(':user_phone', $userPhone, \PDO::PARAM_STR);
        $result->bindParam(':user_comment', $userComment, \PDO::PARAM_STR);
        $result->bindParam(':user_id', $userId, \PDO::PARAM_STR);
        $result->bindParam(':products', $products, \PDO::PARAM_STR);

        return $result->execute();
    }

    /**
     * Возвращает список заказов
     * @return array <p>Список заказов</p>
     */
    public static function getOrdersList()
    {
        // Соединение с БД
        $db = DB::getConnection();

        // Получение и возврат результатов
        $result = $db->query('SELECT id, user_name, user_phone, date, status FROM product_order ORDER BY id DESC');
        $ordersList = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $ordersList[$i]['id'] = $row['id'];
            $ordersList[$i]['user_name'] = $row['user_name'];
            $ordersList[$i]['user_phone'] = $row['user_phone'];
            $ordersList[$i]['date'] = $row['date'];
            $ordersList[$i]['status'] = $row['status'];
            $i++;
        }
        return $ordersList;
    }

    /**
     * Возвращает текстое пояснение статуса для заказа :<br/>
     * <i>1 - Новый заказ, 2 - В обработке, 3 - Доставляется, 4 - Закрыт</i>
     * @param integer $status <p>Статус</p>
     * @return string <p>Текстовое пояснение</p>
     */
    public static function getStatusText($status)
    {
        switch ($status) {
            case '1':
                return 'Новый заказ';
                break;
            case '2':
                return 'В обработке';
                break;
            case '3':
                return 'Доставляется';
                break;
            case '4':
                return 'Закрыт';
                break;
        }
    }

    /**
     * Возвращает заказ с указанным id
     * @param integer $id <p>id</p>
     * @return array <p>Массив с информацией о заказе</p>
     */
    public static function getOrderById($id)
    {
        // Соединение с БД
        $db = DB::getConnection();

        // Текст запроса к БД
        $sql = 'SELECT * FROM `order` WHERE id = :id';

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, \PDO::PARAM_INT);

        // Указываем, что хотим получить данные в виде массива
        $result->setFetchMode(\PDO::FETCH_ASSOC);

        // Выполняем запрос
        $result->execute();

        // Возвращаем данные
        return $result->fetch();
    }

    /**
     * Удаляет заказ с заданным id
     * @param integer $id <p>id заказа</p>
     * @return boolean <p>Результат выполнения метода</p>
     */
    public static function deleteOrderById($id)
    {
        // Соединение с БД
        $db = DB::getConnection();

        // Текст запроса к БД
        $sql = 'DELETE FROM product_order WHERE id = :id';

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, \PDO::PARAM_INT);
        return $result->execute();
    }

    /**
     * Редактирует заказ с заданным id
     * @param integer $id <p>id товара</p>
     * @param string $userName <p>Имя клиента</p>
     * @param string $userPhone <p>Телефон клиента</p>
     * @param string $userComment <p>Комментарий клиента</p>
     * @param string $date <p>Дата оформления</p>
     * @param integer $status <p>Статус <i>(включено "1", выключено "0")</i></p>
     * @return boolean <p>Результат выполнения метода</p>
     */
    public static function updateOrderById($id, $userName, $userPhone, $userComment, $date, $status)
    {
        // Соединение с БД
        $db = DB::getConnection();

        // Текст запроса к БД
        $sql = "UPDATE product_order
            SET 
                user_name = :user_name, 
                user_phone = :user_phone, 
                user_comment = :user_comment, 
                date = :date, 
                status = :status 
            WHERE id = :id";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, \PDO::PARAM_INT);
        $result->bindParam(':user_name', $userName, \PDO::PARAM_STR);
        $result->bindParam(':user_phone', $userPhone, \PDO::PARAM_STR);
        $result->bindParam(':user_comment', $userComment, \PDO::PARAM_STR);
        $result->bindParam(':date', $date, \PDO::PARAM_STR);
        $result->bindParam(':status', $status, \PDO::PARAM_INT);
        return $result->execute();
    }

    public static function create(\entities\Order $order)
    {
        if ($order) {

            $dbh = DB::getConnection();
            $sql = "INSERT INTO `order` (userID, serviceID, gymID, trainerID, price, dateBegin, duration, dateEnd) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            $stmt = $dbh->prepare($sql);

            $stmt->execute([
                $order->getUserID(),
                $order->getServiceID(),
                $order->getGymID(),
                $order->getTrainerID(),
                $order->getPrice(),
                $order->getDateBegin(),
                $order->getDuration(),
                $order->getDateEnd(),
            ]);

            return $dbh->lastInsertId();
        }
        return false;
    }

    public static function getFullOrdersByUser(\src\User $user)
    {
        if ($user) {

            $orders = self::getOrdersByUser($user);
            return $orders;
        }
        return false;
    }

    public static function getFullOrders()
    {
        $dbh = DB::getConnection();
        $sql = "SELECT * FROM `order` ORDER BY id DESC";
        $stmt = $dbh->query($sql);
        $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);
        $orders = self::getOrdersByRows($rows);
        return $orders;
    }

    public static function getOrdersByUser(\src\User $user)
    {
        if ($user) {

            $dbh = DB::getConnection();
            $sql = "SELECT * FROM `order` WHERE userID = ?";
            $stmt = $dbh->prepare($sql);
            $stmt->execute([$user->getId()]);
            $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);
            $orders = self::getOrdersByRows($rows);
            return $orders;
        }
        return false;
    }

    public static function getOrdersByRows(array $rows)
    {
        $orders = [];
        if (count($rows)) {
            foreach ($rows as $row) {
                if ($row->id) {
                    $orders[$row->id] = new \entities\Order(
                        $row->id,
                        $row->userID,
                        $row->serviceID,
                        $row->gymID,
                        $row->trainerID,
                        $row->price,
                        $row->dateBegin,
                        $row->duration,
                        $row->pay,
                        $row->dateEnd
                    );
                }
            }
        }
        return $orders;
    }

    public static function updateOrderPay($id, $pay)
    {
        if ($id && intval($pay)) {
            $db = DB::getConnection();
            $sql = "UPDATE `order` SET pay = :pay WHERE id = :id";
            $result = $db->prepare($sql);
            $result->bindParam(':pay', $pay, \PDO::PARAM_INT);
            $result->bindParam(':id', $id, \PDO::PARAM_INT);
            return $result->execute();
        }
    }

    public static function getOrdersByServiceIDAndCurrentUser($serviceID, $date, EntityManager $em)
    {
        if ($serviceID) {

            $dbh = DB::getConnection();
            $sql = "SELECT * FROM `order` WHERE serviceID = ? AND userID = ? AND dateBegin <= ? AND dateEnd >= ?";
            $user = User::getUserBySession($em);
            $stmt = $dbh->prepare($sql);
            $stmt->execute([$serviceID, $user->getID(), $date, $date]);
            $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);
            $orders = self::getOrdersByRows($rows);
            return $orders;
        }
    }
}
