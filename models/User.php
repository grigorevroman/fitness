<?php

namespace models;

use components\DB;
use Doctrine\ORM\EntityManager;

class User
{
    const CLIENT = 'client';
    const MANAGER = 'manager';
    const TRAINER = 'trainer';
    const ADMIN = 'admin';

    public static $roles = [
        self::CLIENT => 'Клиент',
        self::MANAGER => 'Менеджер',
        self::TRAINER => 'Тренер',
        self::ADMIN => 'Администратор',
    ];

    const NOT_SELECTED = 'not_selected';
    const MAN = 'man';
    const WOMAN = 'woman';

    public static $genders = [
        self::NOT_SELECTED => 'Не выбран',
        self::MAN => 'Мужской',
        self::WOMAN => 'Женский',
    ];

    public static function delete(\src\User $user, EntityManager $em)
    {
        if ($user) {
            $id = $user->getId();
            $em->remove($user);
            $em->flush();
            return $id;
        }
        return false;
    }

    public static function getUserByEmailAndPassword($email, $password, EntityManager $em)
    {
        $dbh = DB::getConnection();
        $sql = 'SELECT * FROM user WHERE email = ? AND password = ?';
        $stmt = $dbh->prepare($sql);
        $stmt->execute([$email, $password]);
        $rows[] = $stmt->fetch(\PDO::FETCH_OBJ);
        $users = self::getUsersByRows($rows);

        if ($user = array_shift($users)) {
            return $user;
        }
        return false;
    }

    public static function getUsersByRole($role, EntityManager $em)
    {
        $users = [];
        if ($role) {
            return $em->getRepository('src\User')->findBy(['role' => $role]);
        }
        return $users;
    }

    public static function getUsersByRows(array $rows)
    {
        $users = [];
        if (count($rows)) {
            foreach ($rows as $row) {
                if ($row->id) {
                    $users[$row->id] = new \entities\User(
                        $row->id,
                        $row->name,
                        $row->code,
                        $row->dateCreate,
                        $row->dateChange,
                        $row->active,
                        $row->sortable,
                        $row->intro,
                        $row->content,
                        $row->preview,
                        $row->role,
                        $row->email,
                        $row->phone,
                        $row->gender,
                        $row->dateOfBirth,
                        $row->password,
                        $row->new
                    );
                }
            }
        }
        return $users;
    }

    public static function authorization(\src\User $user)
    {
        if ($user) {
            $_SESSION['userID'] = $user->getId();
            return true;
        }
        return false;
    }

    public static function isGuest()
    {
        if ($_SESSION['userID']) {
            return false;
        }
        return true;
    }

    public static function getUserBySession(EntityManager $em)
    {
        if ($_SESSION['userID']) {
            return $em->find('src\User', $_SESSION['userID']);
        }
        return false;
    }
}