<?php

namespace models;

use components\DB;

class Timing
{
    public static function create(\entities\Timing $timing)
    {
        if ($timing) {

            $dbh = DB::getConnection();
            $sql = "INSERT INTO timing (name, code, active, sortable, dates, serviceID, startTime, endTime, gymID, trainerID, numbers) VALUES (?, ?, ?, ?, ?, ?, ? ,?, ?, ?, ?)";
            $stmt = $dbh->prepare($sql);

            $stmt->execute([
                $timing->getName(),
                $timing->getCode(),
                $timing->getActive(),
                $timing->getSortable(),
                $timing->getDates(),
                $timing->getServiceID(),
                $timing->getStartTime(),
                $timing->getEndTime(),
                $timing->getGymID(),
                $timing->getTrainerID(),
                $timing->getNumbers()
            ]);

            $timing->setID($dbh->lastInsertId());

            return $timing->getID();
        }
        return false;
    }

    public static function update(\entities\Timing $timing)
    {
        if ($timing) {

            $dbh = DB::getConnection();
            $sql = "UPDATE timing SET name = ?, code = ?, active = ?, sortable = ?, dates = ?, serviceID = ?, startTime = ?, endTime = ?, gymID = ?, trainerID = ?, numbers = ? WHERE id = ?";
            $stmt = $dbh->prepare($sql);

            $result =  $stmt->execute([
                $timing->getName(),
                $timing->getCode(),
                $timing->getActive(),
                $timing->getSortable(),
                $timing->getDates(),
                $timing->getServiceID(),
                $timing->getStartTime(),
                $timing->getEndTime(),
                $timing->getGymID(),
                $timing->getTrainerID(),
                $timing->getNumbers(),
                $timing->getID()
            ]);

            return $result;
        }
        return false;
    }

    public static function delete(\entities\Timing $timing)
    {
        if ($timing) {
            $dbh = DB::getConnection();
            $sql = "DELETE FROM timing WHERE id = ?";
            $stmt = $dbh->prepare($sql);
            $result =  $stmt->execute([$timing->getID()]);
            return $result;
        }
        return false;
    }

    public static function getDatesSelect()
    {
        $dates = [];
        for ($i = 0; $i < 7; $i++) {
            $date = new \DateTime();
            $date->modify('+' . $i . ' day');
            $dates[] = $date->format('Y-m-d');
        }
        return $dates;
    }

    public static function getTimings()
    {
        $dbh = DB::getConnection();
        $sql = "SELECT * FROM timing ORDER BY startTime ASC";
        $stmt = $dbh->query($sql);
        $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);
        $timings = self::getTimingsByRows($rows);
        return $timings;
    }

    public static function getTimingsByRows(array $rows)
    {
        $timings = [];
        if (count($rows)) {
            foreach ($rows as $row) {
                if ($row->id) {
                    $timings[$row->id] = new \entities\Timing(
                        $row->id,
                        $row->name,
                        $row->code,
                        $row->active,
                        $row->sortable,
                        $row->dates,
                        $row->serviceID,
                        $row->startTime,
                        $row->endTime,
                        $row->gymID,
                        $row->trainerID,
                        $row->numbers
                    );
                }
            }
        }
        return $timings;
    }

    public static function getTimingByID($id)
    {
        if ($id) {
            $dbh = DB::getConnection();
            $sql = "SELECT * FROM timing WHERE id = ?";
            $stmt = $dbh->prepare($sql);
            $stmt->execute([$id]);
            $rows[] = $stmt->fetch(\PDO::FETCH_OBJ);
            $timings = self::getTimingsByRows($rows);
            if ($timing = array_shift($timings)) {
                return $timing;
            }
        }
        return false;
    }
}
