<?php

namespace models;

use components\DB;
use Doctrine\ORM\EntityManager;

class Gym
{
    public static function create(\entities\FullGym $gym)
    {
        if ($gym) {

            $dbh = DB::getConnection();
            $sql = "INSERT INTO gym (name, code, active, sortable, intro, content, preview) " .
                "VALUES (?, ?, ?, ?, ?, ?, ?)";
            $stmt = $dbh->prepare($sql);

            $stmt->execute([
                $gym->getName(),
                $gym->getCode(),
                $gym->getActive(),
                $gym->getSortable(),
                $gym->getIntro(),
                $gym->getContent(),
                $gym->getPreview(),
            ]);

            $gym->setID($dbh->lastInsertId());

            if (count($gym->getServices())) {
                /** @var \entities\Service $service */
                foreach ($gym->getServices() as $service) {
                    $sql = "INSERT INTO relation_service_and_gym (service_id, gym_id) VALUES (?, ?)";
                    $stmt = $dbh->prepare($sql);
                    $stmt->execute([$service->getID(), $gym->getID()]);
                }
            }

            if ($gym->getPreview()) {
                $preview = json_decode($gym->getPreview(), true);
                move_uploaded_file($preview['tmp_name'], ROOT . '/upload/' . $preview['name']);
            }

            return $gym->getID();
        }
        return false;
    }

    public static function update(\entities\FullGym $gym)
    {
        if ($gym) {

            $dbh = DB::getConnection();
            $sql = "UPDATE gym SET " .
                "name = ?, " .
                "code = ?, " .
                "dateChange = NOW(), " .
                "active = ?, " .
                "sortable = ?, " .
                "intro = ?, " .
                "content = ? " .
                "WHERE id = ?";
            $stmt = $dbh->prepare($sql);

            $result =  $stmt->execute([
                $gym->getName(),
                $gym->getCode(),
                $gym->getActive(),
                $gym->getSortable(),
                $gym->getIntro(),
                $gym->getContent(),
                $gym->getID(),
            ]);

            $sql = "DELETE FROM relation_service_and_gym WHERE gym_id = ?";
            $stmt = $dbh->prepare($sql);
            $stmt->execute([$gym->getID()]);

            if (count($gym->getServices())) {
                /** @var \entities\Service $service */
                foreach ($gym->getServices() as $service) {
                    $sql = "INSERT INTO relation_service_and_gym (service_id, gym_id) VALUES (?, ?)";
                    $stmt = $dbh->prepare($sql);
                    $stmt->execute([$service->getID(), $gym->getID()]);
                }
            }

            return $result;
        }
        return false;
    }

    public static function delete(\src\Gym $gym, EntityManager $em)
    {
        if ($gym) {

            $id = $gym->getId();

            $dbh = DB::getConnection();
            $sql = "DELETE FROM relation_service_and_gym WHERE gym_id = ?";
            $stmt = $dbh->prepare($sql);
            $stmt->execute([$gym->getId()]);

            $em->remove($gym);
            $em->flush();

            return $id;
        }
        return false;
    }

    public static function getGymsByIDs($ids = [], EntityManager $em)
    {
        $gyms = [];
        if (count($ids)) {
            $gyms = $em->getRepository('src\Gym')->findBy(['id' => $ids]);
        }
        return $gyms;
    }

    public static function getGymsByService(\entities\Service $service)
    {
        $gyms = [];

        if ($service) {

            $dbh = DB::getConnection();
            $sql = "select gym.* from gym " .
                "left join relation_service_and_gym on relation_service_and_gym.gym_id = gym.id " .
                "left join service on service.id = relation_service_and_gym.service_id " .
                "where service.id = ?";

            $stmt = $dbh->prepare($sql);
            $stmt->execute([$service->getID()]);
            $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);
            $gyms = self::getGymsByRows($rows);
        }

        return $gyms;
    }

    public static function getFullGymByID($id, EntityManager $em)
    {
        if ($id) {

            $gym = $em->find('src\Gym', $id);

            if ($gym) {

                $services = Service::getServicesByGym($gym);

                return new \entities\FullGym(
                    $gym->getID(),
                    $gym->getName(),
                    $gym->getCode(),
                    $gym->getDateCreate(),
                    $gym->getDateChange(),
                    $gym->getActive(),
                    $gym->getSortable(),
                    $gym->getIntro(),
                    $gym->getContent(),
                    $gym->getPreview(),
                    $services
                );
            }
        }
        return null;
    }

    public static function getGymsByRows($rows)
    {
        $gyms = [];
        if (count($rows)) {
            foreach ($rows as $row) {
                if ($row->id) {
                    $gyms[$row->id] = new \entities\Gym(
                        $row->id,
                        $row->name,
                        $row->code,
                        $row->dateCreate,
                        $row->dateChange,
                        $row->active,
                        $row->sortable,
                        $row->intro,
                        $row->content,
                        $row->preview
                    );
                }
            }
        }
        return $gyms;
    }
}