<?php

namespace src;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="record")
 */
class Record
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $serviceId;

    /**
     * @ORM\Column(type="integer")
     */
    private $timingId;

    /**
     * @ORM\Column(type="integer")
     */
    private $orderId;

    /**
     * @ORM\Column(type="integer")
     */
    private $userId;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $date;

    public function __construct(
        $id = 0,
        $serviceId = 0,
        $timingId = 0,
        $orderId = 0,
        $userId = 0,
        $date = ''
    ) {
        $this->id = $id;
        $this->serviceId = $serviceId;
        $this->timingId = $timingId;
        $this->orderId = $orderId;
        $this->userId = $userId;
        $this->date = $date;
    }

    public function getID()
    {
        return $this->id;
    }

    public function setID($id)
    {
        $this->id = $id;
    }

    public function getServiceId()
    {
        return $this->serviceId;
    }

    public function setServiceId($serviceId)
    {
        $this->serviceId = $serviceId;
    }

    public function getTimingId()
    {
        return $this->timingId;
    }

    public function setTimingId($timingId)
    {
        $this->timingId = $timingId;
    }

    public function getOrderId()
    {
        return $this->orderId;
    }

    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    public function geUserId()
    {
        return $this->userId;
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;
    }


    public function geDate()
    {
        return $this->date;
    }

    public function setDate($date)
    {
        $this->date = $date;
    }
}