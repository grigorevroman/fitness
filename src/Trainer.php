<?php

namespace src;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="trainer")
 */
class Trainer
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $code;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $dateChange;

    /**
     * @ORM\Column(type="integer")
     */
    private $active;

    /**
     * @ORM\Column(type="integer")
     */
    private $sortable;

    /**
     * @ORM\Column(type="string")
     */
    private $intro;

    /**
     * @ORM\Column(type="string")
     */
    private $content;

    /**
     * @ORM\Column(type="string")
     */
    private $preview;

    /**
     * @ORM\Column(type="integer")
     */
    private $userID;

    public function __construct(
        $id = 0,
        $name = '',
        $code = '',
        $dateCreate = null,
        $dateChange = null,
        $active = 0,
        $sortable = 0,
        $intro = '',
        $content = '',
        $preview = '',
        $userID = 0
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->code = $code;
        $this->dateCreate = $dateCreate;
        $this->dateChange = $dateChange;
        $this->active = $active;
        $this->sortable = $sortable;
        $this->intro = $intro;
        $this->content = $content;
        $this->preview = $preview;
        $this->userID = $userID;
    }

    public function getID()
    {
        return $this->id;
    }

    public function setID($id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;
    }

    public function getDateChange()
    {
        return $this->dateChange;
    }

    public function setDateChange($dateChange)
    {
        $this->dateChange = $dateChange;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setActive($active)
    {
        $this->active = $active;
    }

    public function getSortable()
    {
        return $this->sortable;
    }

    public function setSortable($sortable)
    {
        $this->sortable = $sortable;
    }

    public function getIntro()
    {
        return $this->intro;
    }

    public function setIntro($intro)
    {
        $this->intro = $intro;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;
    }

    public function getPreview()
    {
        return $this->preview;
    }

    public function getPreviewName()
    {
        $previewJson = $this->preview;
        $preview = json_decode($previewJson, true);
        return $preview['name'];
    }

    public function setPreview($preview)
    {
        $this->preview = $preview;
    }

    public function getUserID()
    {
        return $this->userID;
    }

    public function setUserID($userID)
    {
        $this->userID = $userID;
    }
}