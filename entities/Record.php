<?php

namespace entities;

class Record
{
    private $id;
    private $orderID;
    private $serviceID;
    private $timingID;
    private $userID;
    private $date;

    public function __construct(
        $id = null,
        $orderID = null,
        $serviceID = null,
        $timingID = null,
        $userID = null,
        $date = null
    ) {
        $this->id = $id;
        $this->orderID = $orderID;
        $this->serviceID = $serviceID;
        $this->timingID = $timingID;
        $this->userID = $userID;
        $this->date = $date;
    }

    public function getID()
    {
        return $this->id;
    }

    public function setID($id)
    {
        $this->id = $id;
    }

    public function getOrderID()
    {
        return $this->orderID;
    }

    public function setOrderID($orderID)
    {
        $this->orderID = $orderID;
    }

    public function getServiceID()
    {
        return $this->serviceID;
    }

    public function setServiceID($serviceID)
    {
        $this->serviceID = $serviceID;
    }

    public function getTimingID()
    {
        return $this->timingID;
    }

    public function setTimingID($timingID)
    {
        $this->timingID = $timingID;
    }

    public function getUserID()
    {
        return $this->userID;
    }

    public function setUserID($userID)
    {
        $this->userID = $userID;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date)
    {
        $this->date = $date;
    }
}
