<?php

namespace entities;

class Entity
{
    private $id;
    private $name;
    private $code;
    private $dateCreate;
    private $dateChange;
    private $active;
    private $sortable;
    private $intro;
    private $content;
    private $preview;

    public function __construct(
        $id = 0,
        $name = '',
        $code = '',
        $dateCreate = null,
        $dateChange = null,
        $active = 0,
        $sortable = 0,
        $intro = '',
        $content = '',
        $preview = ''
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->code = $code;
        $this->dateCreate = $dateCreate;
        $this->dateChange = $dateChange;
        $this->active = $active;
        $this->sortable = $sortable;
        $this->intro = $intro;
        $this->content = $content;
        $this->preview = $preview;
    }

    public function getID()
    {
        return $this->id;
    }

    public function setID($id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;
    }

    public function getDateChange()
    {
        return $this->dateChange;
    }

    public function setDateChange($dateChange)
    {
        $this->dateChange = $dateChange;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setActive($active)
    {
        $this->active = $active;
    }

    public function getSortable()
    {
        return $this->sortable;
    }

    public function setSortable($sortable)
    {
        $this->sortable = $sortable;
    }

    public function getIntro()
    {
        return $this->intro;
    }

    public function setIntro($intro)
    {
        $this->intro = $intro;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;
    }

    public function getPreview()
    {
        return $this->preview;
    }

    public function getPreviewName()
    {
        $previewJson = $this->preview;
        $preview = json_decode($previewJson, true);
        return $preview['name'];
    }

    public function setPreview($preview)
    {
        $this->preview = $preview;
    }
}
