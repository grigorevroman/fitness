<?php

namespace entities;

class Service extends Entity
{
    private $type;
    private $priceDay;
    private $priceMonth;
    private $priceHalfYear;
    private $priceYear;

    public function __construct(
        $id = 0,
        $name = '',
        $code = '',
        $dateCreate = null,
        $dateChange = null,
        $active = 0,
        $sortable = 0,
        $intro = '',
        $content = '',
        $preview = '',
        $type = '',
        $priceDay = 0,
        $priceMonth = 0,
        $priceHalfYear = 0,
        $priceYear = 0
    ) {
        parent::__construct($id, $name, $code, $dateCreate, $dateChange, $active, $sortable, $intro, $content, $preview);
        $this->type = $type;
        $this->priceDay = $priceDay;
        $this->priceMonth = $priceMonth;
        $this->priceHalfYear = $priceHalfYear;
        $this->priceYear = $priceYear;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getPriceDay()
    {
        return $this->priceDay;
    }

    public function setPriceDay($priceDay)
    {
        $this->priceDay = $priceDay;
    }

    public function getPriceMonth()
    {
        return $this->priceMonth;
    }

    public function setPriceMonth($priceMonth)
    {
        $this->priceMonth = $priceMonth;
    }

    public function getPriceHalfYear()
    {
        return $this->priceHalfYear;
    }

    public function setPriceHalfYear($priceHalfYear)
    {
        $this->priceHalfYear = $priceHalfYear;
    }

    public function getPriceYear()
    {
        return $this->priceYear;
    }

    public function setPriceYear($priceYear)
    {
        $this->priceYear = $priceYear;
    }
}
