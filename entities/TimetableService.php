<?php

namespace entities;

class TimetableService extends Entity
{
    private $serviceID;
    private $service;

    public function __construct(
        $id = 0,
        $name = null,
        $code = null,
        $active = 0,
        $sortable = 0,
        $serviceID = 0,
        $service = null
    ) {
        parent::__construct(
            $id,
            $name,
            $code,
            null,
            null,
            $active,
            $sortable,
            null,
            null,
            null
        );
        $this->serviceID = $serviceID;
        $this->service = $service;
    }

    public function getServiceID()
    {
        return $this->serviceID;
    }

    public function setServiceID($serviceID)
    {
        $this->serviceID = $serviceID;
    }

    public function getService()
    {
        return $this->service;
    }

    public function setService($service)
    {
        $this->service = $service;
    }
}
