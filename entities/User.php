<?php

namespace entities;

class User extends Entity
{
    private $role;
    private $email;
    private $phone;
    private $gender;
    private $dateOfBirth;
    private $password;
    private $new;

    public function __construct(
        $id = 0,
        $name = '',
        $code = '',
        $dateCreate = null,
        $dateChange = null,
        $active = 0,
        $sortable = 0,
        $intro = '',
        $content = '',
        $preview = '',
        $role = '',
        $email = '',
        $phone = '',
        $gender = '',
        $dateOfBirth = null,
        $password = '',
        $new = ''
    ) {
        parent::__construct($id, $name, $code, $dateCreate, $dateChange, $active, $sortable, $intro, $content, $preview);
        $this->role = $role;
        $this->email = $email;
        $this->phone = $phone;
        $this->gender = $gender;
        $this->dateOfBirth = $dateOfBirth;
        $this->password = $password;
        $this->new = $new;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function setRole($role)
    {
        $this->role = $role;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getNew()
    {
        return $this->new;
    }

    public function setNew($new)
    {
        $this->new = $new;
    }
}