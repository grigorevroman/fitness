<?php

namespace entities;

class FullGym extends Gym
{
    private $services;

    public function __construct(
        $id = 0,
        $name = '',
        $code = '',
        $dateCreate = null,
        $dateChange = null,
        $active = 0,
        $sortable = 0,
        $intro = '',
        $content = '',
        $preview = '',
        array $services = []
    ) {
        parent::__construct(
            $id,
            $name,
            $code,
            $dateCreate,
            $dateChange,
            $active,
            $sortable,
            $intro,
            $content,
            $preview
        );
        $this->services = $services;
    }

    public function getServices()
    {
        return $this->services;
    }

    public function setServices($services)
    {
        $this->services = $services;
    }
}
