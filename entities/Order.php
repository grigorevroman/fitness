<?php

namespace entities;

class Order
{
    private $id;
    private $userID;
    private $serviceID;
    private $gymID;
    private $trainerID;
    private $price;
    private $dateBegin;
    private $duration;
    private $pay;
    private $dateEnd;

    public function __construct(
        $id = 0,
        $userID = 0,
        $serviceID = 0,
        $gymID = 0,
        $trainerID = 0,
        $price = 0,
        $dateBegin = null,
        $duration = '',
        $pay = 0,
        $dateEnd = null
    ) {
        $this->id = $id;
        $this->userID = $userID;
        $this->serviceID = $serviceID;
        $this->gymID = $gymID;
        $this->trainerID = $trainerID;
        $this->price = $price;
        $this->dateBegin = $dateBegin;
        $this->duration = $duration;
        $this->pay = $pay;
        $this->dateEnd = $dateEnd;
    }

    public function getID()
    {
        return $this->id;
    }

    public function setID($id)
    {
        $this->id = $id;
    }

    public function getUserID()
    {
        return $this->userID;
    }

    public function setUserID($userID)
    {
        $this->userID = $userID;
    }

    public function getServiceID()
    {
        return $this->serviceID;
    }

    public function setServiceID($serviceID)
    {
        $this->serviceID = $serviceID;
    }

    public function getGymID()
    {
        return $this->gymID;
    }

    public function setGymID($gymID)
    {
        $this->gymID = $gymID;
    }

    public function getTrainerID()
    {
        return $this->trainerID;
    }

    public function setTrainerID($trainerID)
    {
        $this->trainerID = $trainerID;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getDateBegin()
    {
        return $this->dateBegin;
    }

    public function setDateBegin($dateBegin)
    {
        $this->dateBegin = $dateBegin;
    }

    public function getDuration()
    {
        return $this->duration;
    }

    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    public function getPay()
    {
        return $this->pay;
    }

    public function setPay($pay)
    {
        $this->pay = $pay;
    }

    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;
    }
}
