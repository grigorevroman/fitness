<?php

namespace entities;

class FullService extends Service
{
    private $gyms;
    private $trainers;

    public function __construct(
        $id = 0,
        $name = '',
        $code = '',
        $dateCreate = null,
        $dateChange = null,
        $active = 0,
        $sortable = 0,
        $intro = '',
        $content = '',
        $preview = '',
        $type = '',
        $priceDay = 0,
        $priceMonth = 0,
        $priceHalfYear = 0,
        $priceYear = 0,
        array $gyms = [],
        array $trainers = []
    ) {
        parent::__construct(
            $id,
            $name,
            $code,
            $dateCreate,
            $dateChange,
            $active,
            $sortable,
            $intro,
            $content,
            $preview,
            $type,
            $priceDay,
            $priceMonth,
            $priceHalfYear,
            $priceYear
        );
        $this->gyms = $gyms;
        $this->trainers = $trainers;
    }

    public function getGyms()
    {
        return $this->gyms;
    }

    public function setGyms($gyms)
    {
        $this->gyms = $gyms;
    }

    public function getTrainers()
    {
        return $this->trainers;
    }

    public function setTrainers($trainers)
    {
        $this->trainers = $trainers;
    }
}