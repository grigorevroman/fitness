<?php

namespace entities;

class Timing extends Entity
{
    private $dates;
    private $serviceID;
    private $startTime;
    private $endTime;
    private $gymID;
    private $trainerID;
    private $numbers;

    public function __construct(
        $id = 0,
        $name = '',
        $code = '',
        $active = 0,
        $sortable = 0,
        $dates = '',
        $serviceID = null,
        $startTime = null,
        $endTime = null,
        $gymID = null,
        $trainerID = null,
        $numbers = null
    ) {
        parent::__construct(
            $id,
            $name,
            $code,
            null,
            null,
            $active,
            $sortable,
            null,
            null,
            null
        );
        $this->dates = $dates;
        $this->serviceID = $serviceID;
        $this->startTime = $startTime;
        $this->endTime = $endTime;
        $this->gymID = $gymID;
        $this->trainerID = $trainerID;
        $this->numbers = $numbers;
    }

    public function getDates()
    {
        return $this->dates;
    }

    public function setDates($dates)
    {
        $this->dates = $dates;
    }

    public function getServiceID()
    {
        return $this->serviceID;
    }

    public function setServiceID($serviceID)
    {
        $this->serviceID = $serviceID;
    }

    public function getStartTime()
    {
        return $this->startTime;
    }

    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;
    }

    public function getEndTime()
    {
        return $this->endTime;
    }

    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;
    }

    public function getGymID()
    {
        return $this->gymID;
    }

    public function setGymID($gymID)
    {
        $this->gymID = $gymID;
    }

    public function getTrainerID()
    {
        return $this->trainerID;
    }

    public function setTrainerID($trainerID)
    {
        $this->trainerID = $trainerID;
    }

    public function getNumbers()
    {
        return $this->numbers;
    }

    public function setNumbers($numbers)
    {
        $this->numbers = $numbers;
    }
}
