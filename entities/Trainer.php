<?php

namespace entities;

class Trainer extends Entity
{
    private $userID;

    public function __construct(
        $id = 0,
        $name = '',
        $code = '',
        $dateCreate = null,
        $dateChange = null,
        $active = 0,
        $sortable = 0,
        $intro = '',
        $content = '',
        $preview = '',
        $userID = null
    ) {
        parent::__construct($id, $name, $code, $dateCreate, $dateChange, $active, $sortable, $intro, $content, $preview);
        $this->userID = $userID;
    }

    public function getUserID()
    {
        return $this->userID;
    }

    public function setUserID($userID)
    {
        $this->userID = $userID;
    }
}