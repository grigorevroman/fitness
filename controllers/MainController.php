<?php

use models\Service;
use models\Gym;
use models\Trainer;

class MainController
{
    public function actionMain(\Doctrine\ORM\EntityManager $em)
    {
        $services = $em->getRepository('src\Service')->findAll();
        $gyms = $em->getRepository('src\Gym')->findAll();
        $trainers = Trainer::getTrainers();

        require_once ROOT . '/views/main/main.php';

        return true;
    }

    public function actionAbout(\Doctrine\ORM\EntityManager $em)
    {
        $services = $em->getRepository('src\Service')->findAll();
        $gyms = $em->getRepository('src\Gym')->findAll();
        $trainers = Trainer::getTrainers();

        require_once ROOT . '/views/main/about.php';

        return true;
    }
}
