<?php

use models\Day;
use components\Validator;
use components\Helper;
use components\Admin;

class AdminTimetableDayController extends Admin
{
    public static function actionCreate(\Doctrine\ORM\EntityManager $em)
    {
        self::checkAdmin($em);

        $errors = [];

        $day = new entities\Day();

        if ($_POST['day_create']) {

            $day->setName($_POST['name']);
            $day->setCode(strtolower(Helper::translit($day->getName())));
            $day->setActive($_POST['active'] === 'on' ? 1 : 0);
            $day->setSortable($_POST['sortable']);

            if ($error = Validator::checkNameAndCode($day->getName(), $day->getCode())) {
                $errors[] = $error;
            }
            if ($error = Validator::checkNumber($day->getSortable(), 'Сортировка')) {
                $errors[] = $error;
            }

            if (!count($errors)) {
                if ($id = Day::create($day)) {
                    header('Location: /admin/timetable/day/update/' . $id . '/');
                } else {
                    $errors[] = 'День не был добавлен';
                }
            }
        }

        require_once ROOT . '/views/admin/timetable/day/create.php';

        return true;
    }

    public static function actionRead(\Doctrine\ORM\EntityManager $em)
    {
        $days = Day::getDays();

        require_once ROOT . '/views/admin/timetable/day/read.php';

        return true;
    }

    public static function actionUpdate($id, \Doctrine\ORM\EntityManager $em)
    {
        self::checkAdmin($em);

        if ($id) {

            $errors = [];

            $day = Day::getDayByID($id);

            if ($day) {

                if ($_POST['day_update']) {

                    $day->setName($_POST['name']);
                    $day->setCode(strtolower(Helper::translit($day->getName())));
                    $day->setActive($_POST['active'] === 'on' ? 1 : 0);
                    $day->setSortable($_POST['sortable']);

                    if ($error = Validator::checkNameAndCode($day->getName(), $day->getCode())) {
                        $errors[] = $error;
                    }
                    if ($error = Validator::checkNumber($day->getSortable(), 'Сортировка')) {
                        $errors[] = $error;
                    }

                    if (!count($errors)) {
                        if (Day::update($day)) {
                            header('Location: /admin/timetable/day/update/' . $id . '/');
                        } else {
                            $errors[] = 'День не был изменен';
                        }
                    }
                }
            }

            require_once ROOT . '/views/admin/timetable/day/update.php';

            return true;
        }
        return false;
    }

    public static function actionDelete($id, \Doctrine\ORM\EntityManager $em)
    {
        self::checkAdmin($em);

        if ($id) {

            $errors = [];

            $day = Day::getDayByID($id);

            if ($day) {

                if ($_POST['day_delete']) {

                    if (Day::delete($day)) {
                        header('Location: /admin/timetable/day/');
                    } else {
                        $errors[] = 'День не был удален';
                    }
                }
            }

            require_once ROOT . '/views/admin/timetable/day/delete.php';

            return true;
        }
        return false;
    }
}