<?php

use components\Admin;
use components\Helper;
use components\Validator;
use models\Timing;
use models\Service;
use models\Gym;
use models\Trainer;

class AdminTimingController extends Admin
{
    public function actionCreate(\Doctrine\ORM\EntityManager $em)
    {
        self::checkAdmin($em);

        $errors = [];

        $timing = new entities\Timing();

        if ($_POST['timing_create']) {

            $timing->setName($_POST['name']);
            $timing->setCode(strtolower(Helper::translit($timing->getName())));
            $timing->setActive($_POST['active'] === 'on' ? 1 : 0);
            $timing->setSortable($_POST['sortable']);
            $timing->setDates(json_encode($_POST['dates'], true));
            $timing->setServiceID($_POST['serviceID']);
            $timing->setStartTime($_POST['startTime']);
            $timing->setEndTime($_POST['endTime']);
            $timing->setGymID($_POST['gymID']);
            $timing->setTrainerID($_POST['trainerID']);
            $timing->setNumbers($_POST['numbers']);

            if ($error = Validator::checkNameAndCode($timing->getName(), $timing->getCode())) {
                $errors[] = $error;
            }
            if ($error = Validator::checkNumber($timing->getSortable(), 'Сортировка')) {
                $errors[] = $error;
            }
            if ($error = Validator::checkNumber($timing->getNumbers(), 'Количество мест')) {
                $errors[] = $error;
            }

            if (!count($errors)) {
                if ($id = Timing::create($timing)) {
                    header('Location: /admin/timing/update/' . $id . '/');
                } else {
                    $errors[] = 'Услуга не была добавлена в расписание';
                }
            }
        }

        $datesSelect = Timing::getDatesSelect();
        $servicesSelect = $em->getRepository('src\Service')->findAll();
        $gymsSelect = $em->getRepository('src\Gym')->findAll();
        $trainersSelect = Trainer::getTrainers();

        require_once ROOT . '/views/admin/timing/create.php';

        return true;
    }

    public function actionRead(\Doctrine\ORM\EntityManager $em)
    {
        self::checkAdmin($em);

        $timings = Timing::getTimings();

        require_once ROOT . '/views/admin/timing/read.php';

        return true;
    }

    public function actionUpdate($id, \Doctrine\ORM\EntityManager $em)
    {
        if ($id) {

            self::checkAdmin($em);

            $errors = [];

            $timing = Timing::getTimingByID($id);

            if ($_POST['timing_update']) {

                $timing->setName($_POST['name']);
                $timing->setCode(strtolower(Helper::translit($timing->getName())));
                $timing->setActive($_POST['active'] === 'on' ? 1 : 0);
                $timing->setSortable($_POST['sortable']);
                $timing->setDates(json_encode($_POST['dates'], true));
                $timing->setServiceID($_POST['serviceID']);
                $timing->setStartTime($_POST['startTime']);
                $timing->setEndTime($_POST['endTime']);
                $timing->setGymID($_POST['gymID']);
                $timing->setTrainerID($_POST['trainerID']);
                $timing->setNumbers($_POST['numbers']);

                if ($error = Validator::checkNameAndCode($timing->getName(), $timing->getCode())) {
                    $errors[] = $error;
                }
                if ($error = Validator::checkNumber($timing->getSortable(), 'Сортировка')) {
                    $errors[] = $error;
                }
                if ($error = Validator::checkNumber($timing->getNumbers(), 'Количество мест')) {
                    $errors[] = $error;
                }

                if (!count($errors)) {
                    if ($id = Timing::update($timing)) {
                        header('Location: /admin/timing/update/' . $timing->getID() . '/');
                    } else {
                        $errors[] = 'Услуга не была изменена в расписании';
                    }
                }
            }

            $datesSelect = Timing::getDatesSelect();
            $servicesSelect = $em->getRepository('src\Service')->findAll();
            $gymsSelect = $em->getRepository('src\Gym')->findAll();
            $trainersSelect = Trainer::getTrainers();

            require_once ROOT . '/views/admin/timing/update.php';
        }
        return true;
    }

    public static function actionDelete($id, \Doctrine\ORM\EntityManager $em)
    {
        self::checkAdmin($em);
        if ($id) {
            $errors = [];
            $timing = Timing::getTimingByID($id);
            if ($timing) {
                if ($_POST['timing_delete']) {
                    if (Timing::delete($timing)) {
                        header('Location: /admin/timing/');
                    } else {
                        $errors[] = 'Услуга не была удалена из расписания';
                    }
                }
            }
            require_once ROOT . '/views/admin/timing/delete.php';
            return true;
        }
        return false;
    }
}
