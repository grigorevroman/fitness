<?php

use components\Admin;

class AdminTimetableController extends Admin
{
    public static function actionRead(\Doctrine\ORM\EntityManager $em)
    {
        self::checkAdmin($em);

        require_once ROOT . '/views/admin/timetable/read.php';

        return true;
    }
}