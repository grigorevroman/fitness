<?php

use components\Manager;
use components\Helper;
use components\Validator;
use models\Timing;
use models\Service;
use models\Gym;
use models\Trainer;

class ManagerTimingController extends Manager
{
    public function actionCreate(\Doctrine\ORM\EntityManager $em)
    {
        self::checkManager($em);

        $errors = [];

        $timing = new entities\Timing();

        if ($_POST['timing_create']) {

            $timing->setName($_POST['name']);
            $timing->setCode(strtolower(Helper::translit($timing->getName())));
            $timing->setActive($_POST['active'] === 'on' ? 1 : 0);
            $timing->setSortable($_POST['sortable']);
            $timing->setDates(json_encode($_POST['dates'], true));
            $timing->setServiceID($_POST['serviceID']);
            $timing->setStartTime($_POST['startTime']);
            $timing->setEndTime($_POST['endTime']);
            $timing->setGymID($_POST['gymID']);
            $timing->setTrainerID($_POST['trainerID']);
            $timing->setNumbers($_POST['numbers']);

            if ($error = Validator::checkNameAndCode($timing->getName(), $timing->getCode())) {
                $errors[] = $error;
            }
            if ($error = Validator::checkNumber($timing->getSortable(), 'Сортировка')) {
                $errors[] = $error;
            }
            if ($error = Validator::checkNumber($timing->getNumbers(), 'Количество мест')) {
                $errors[] = $error;
            }

            if (!count($errors)) {
                if ($id = Timing::create($timing)) {
                    header('Location: /manager/timing/update/' . $id . '/');
                } else {
                    $errors[] = 'Услуга не была добавлена в расписание';
                }
            }
        }

        $datesSelect = Timing::getDatesSelect();
        $servicesSelect = $em->getRepository('src\Service')->findAll();
        $gymsSelect = $em->getRepository('src\Gym')->findAll();
        $trainersSelect = Trainer::getTrainers();
        $services = $em->getRepository('src\Service')->findAll();
        $gyms = $em->getRepository('src\Gym')->findAll();
        $trainers = Trainer::getTrainers();

        require_once ROOT . '/views/manager/timing/create.php';

        return true;
    }

    public function actionRead(\Doctrine\ORM\EntityManager $em)
    {
        self::checkManager($em);

        $timings = Timing::getTimings();

        $services = $em->getRepository('src\Service')->findAll();
        $gyms = $em->getRepository('src\Gym')->findAll();
        $trainers = Trainer::getTrainers();

        require_once ROOT . '/views/manager/timing/read.php';

        return true;
    }

    public function actionUpdate($id, \Doctrine\ORM\EntityManager $em)
    {
        if ($id) {

            self::checkManager($em);

            $errors = [];

            $timing = Timing::getTimingByID($id);

            if ($_POST['timing_update']) {

                $timing->setName($_POST['name']);
                $timing->setCode(strtolower(Helper::translit($timing->getName())));
                $timing->setActive($_POST['active'] === 'on' ? 1 : 0);
                $timing->setSortable($_POST['sortable']);
                $timing->setDates(json_encode($_POST['dates'], true));
                $timing->setServiceID($_POST['serviceID']);
                $timing->setStartTime($_POST['startTime']);
                $timing->setEndTime($_POST['endTime']);
                $timing->setGymID($_POST['gymID']);
                $timing->setTrainerID($_POST['trainerID']);
                $timing->setNumbers($_POST['numbers']);

                if ($error = Validator::checkNameAndCode($timing->getName(), $timing->getCode())) {
                    $errors[] = $error;
                }
                if ($error = Validator::checkNumber($timing->getSortable(), 'Сортировка')) {
                    $errors[] = $error;
                }
                if ($error = Validator::checkNumber($timing->getNumbers(), 'Количество мест')) {
                    $errors[] = $error;
                }

                if (!count($errors)) {
                    if ($id = Timing::update($timing)) {
                        header('Location: /manager/timing/update/' . $timing->getID() . '/');
                    } else {
                        $errors[] = 'Услуга не была изменена в расписании';
                    }
                }
            }

            $datesSelect = Timing::getDatesSelect();
            $servicesSelect = $em->getRepository('src\Service')->findAll();
            $gymsSelect = $em->getRepository('src\Gym')->findAll();
            $trainersSelect = Trainer::getTrainers();
            $services = $em->getRepository('src\Service')->findAll();
            $gyms = $em->getRepository('src\Gym')->findAll();
            $trainers = Trainer::getTrainers();

            require_once ROOT . '/views/manager/timing/update.php';
        }
        return true;
    }

    public static function actionDelete($id, \Doctrine\ORM\EntityManager $em)
    {
        self::checkManager($em);
        if ($id) {
            $errors = [];
            $timing = Timing::getTimingByID($id);
            if ($timing) {
                if ($_POST['timing_delete']) {
                    if (Timing::delete($timing)) {
                        header('Location: /manager/timing/');
                    } else {
                        $errors[] = 'Услуга не была удалена из расписания';
                    }
                }
            }
            $services = $em->getRepository('src\Service')->findAll();
            $gyms = $em->getRepository('src\Gym')->findAll();
            $trainers = Trainer::getTrainers();
            require_once ROOT . '/views/manager/timing/delete.php';
            return true;
        }
        return false;
    }
}
