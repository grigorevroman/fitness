<?php

use models\Service;
use models\Gym;
use models\Trainer;
use models\Order;
use models\Record;

class ReportController
{
    public function actionMain(\Doctrine\ORM\EntityManager $em)
    {
        $services = $em->getRepository('src\Service')->findAll();
        $gyms = $em->getRepository('src\Gym')->findAll();
        $trainers = Trainer::getTrainers();

        $orders = Order::getFullOrders();
        $records = Record::getRecords();

        require_once ROOT . '/views/report/main.php';

        return true;
    }
}