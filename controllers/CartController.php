<?php

use components\Cart;
use models\Service;
use models\User;
use models\Order;

/**
 * Контроллер CartController
 * Корзина
 */
class CartController
{

    /**
     * Action для добавления товара в корзину синхронным запросом<br/>
     * (для примера, не используется)
     * @param integer $id <p>id товара</p>
     */
    public function actionAdd($id, \Doctrine\ORM\EntityManager $em)
    {
        // Добавляем товар в корзину
        Cart::addService($id);

        // Возвращаем пользователя на страницу с которой он пришел
        $referrer = $_SERVER['HTTP_REFERER'];
        header("Location: $referrer");
    }

    /**
     * @param $id
     * @return bool
     */
    public function actionAddAjax($id, \Doctrine\ORM\EntityManager $em)
    {
        // Добавляем товар в корзину и печатаем результат: количество товаров в корзине
        echo Cart::addService($id);
        return true;
    }

    /**
     * Action для добавления товара в корзину синхронным запросом
     * @param integer $id <p>id товара</p>
     */
    public function actionDelete($id, \Doctrine\ORM\EntityManager $em)
    {
        // Удаляем заданный товар из корзины
        Cart::deleteService($id);

        // Возвращаем пользователя в корзину
        header("Location: /cart/");
    }

    /**
     * Action для страницы "Корзина"
     */
    public function actionIndex(\Doctrine\ORM\EntityManager $em)
    {
        // Получим идентификаторы и количество товаров в корзине
        $servicesInCart = Cart::getServices();

        if ($servicesInCart) {

            $fullServices = [];
            foreach ($servicesInCart as $id => $count) {
                for ($i = 0; $i < $count; $i++) {
                    $fullServices[] = Service::getFullServiceByID($id, $em);
                }
            }

            // Если в корзине есть товары, получаем полную информацию о товарах для списка
            // Получаем массив только с идентификаторами товаров
            $servicesIDs = array_keys($servicesInCart);

            // Получаем массив с полной информацией о необходимых товарах
            $services = Service::getServicesByIDs($servicesIDs);

            // Получаем общую стоимость товаров
            $totalPrice = Cart::getTotalPrice($services);
        }

        // Подключаем вид
        require_once(ROOT . '/views/cart/index.php');
        return true;
    }

    /**
     * Action для страницы "Оформление покупки"
     */
    public function actionCheckout(\Doctrine\ORM\EntityManager $em)
    {
        // Получием данные из корзины
        $servicesInCart = Cart::getServices();

        // Если товаров нет, отправляем пользователи искать товары на главную
        if ($servicesInCart == false) {
            header("Location: /");
        }

        // Находим общую стоимость
        $servicesIDs = array_keys($servicesInCart);
        $services = Service::getServicesByIds($servicesIDs);
        $totalPrice = Cart::getTotalPrice($services);

        // Количество товаров
        $totalQuantity = Cart::countItems();

        // Поля для формы
        $userName = false;
        $userPhone = false;
        $userComment = false;

        // Статус успешного оформления заказа
        $result = false;

        // Проверяем является ли пользователь гостем
        if (!User::isGuest()) {
            // Если пользователь не гость
            // Получаем информацию о пользователе из БД
            $userId = User::checkLogged();
            $user = $em->find('src\User', $userId);
            $userName = $user->getId();
        } else {
            // Если гость, поля формы останутся пустыми
            $userId = null;
        }

        // Обработка формы
        if (isset($_POST['submit'])) {
            // Если форма отправлена
            // Получаем данные из формы
            $userName = $_POST['userName'];
            $userPhone = $_POST['userPhone'];
            $userComment = $_POST['userComment'];

            // Флаг ошибок
            $errors = false;

            // Валидация полей
            if (!User::checkName($userName)) {
                $errors[] = 'Неправильное имя';
            }
            if (!User::checkPhone($userPhone)) {
                $errors[] = 'Неправильный телефон';
            }

            if ($errors == false) {
                // Если ошибок нет
                // Сохраняем заказ в базе данных

                $result = Order::save($userName, $userPhone, $userComment, $userId, $productsInCart);

                if ($result) {
                    // Если заказ успешно сохранен
                    // Оповещаем администратора о новом заказе по почте
                    $adminEmail = 'instrument64@mail.ru';
                    $message = '<a href="instrument64/admin/order/">Список заказов</a>';
                    $subject = 'Новый заказ!';
                    mail($adminEmail, $subject, $message);

                    // Очищаем корзину
                    Cart::clear();
                }
            }
        }

        // Подключаем вид
        require_once(ROOT . '/views/cart/checkout.php');
        return true;
    }

}
