<?php

use models\Trainer;
use models\User;
use components\Validator;
use components\Helper;
use components\Admin;

class AdminTrainerController extends Admin
{
    public static function actionCreate(\Doctrine\ORM\EntityManager $em)
    {
        self::checkAdmin($em);

        $errors = [];

        $trainer = new src\Trainer();

        if ($_POST['trainer_create']) {

            $trainer->setName($_POST['name']);
            $trainer->setCode(strtolower(Helper::translit($trainer->getName())));
            $trainer->setActive($_POST['active'] === 'on' ? 1 : 0);
            $trainer->setSortable($_POST['sortable']);
            $trainer->setIntro($_POST['intro']);
            $trainer->setContent($_POST['content']);
            $trainer->setPreview($_FILES['preview']['name'] ? json_encode($_FILES['preview'], true) : '');
            $trainer->setUserID($_POST['userID']);

            if ($error = Validator::checkNameAndCode($trainer->getName(), $trainer->getCode(), 'Имя')) {
                $errors[] = $error;
            }
            if ($error = Validator::checkNumber($trainer->getSortable(), 'Сортировка')) {
                $errors[] = $error;
            }

            if (!count($errors)) {

                $em->persist($trainer);
                $em->flush();
                if ($trainer->getPreview()) {
                    $preview = json_decode($trainer->getPreview(), true);
                    move_uploaded_file($preview['tmp_name'], ROOT . '/upload/' . $preview['name']);
                }
                header('Location: /admin/trainer/update/' . $trainer->getId() . '/');
            }
        }

        $usersWithRoleTrainer = User::getUsersByRole(User::$roles[User::TRAINER], $em);

        require_once ROOT . '/views/admin/trainer/create.php';

        return true;
    }

    public static function actionRead(\Doctrine\ORM\EntityManager $em)
    {
        self::checkAdmin($em);

        $trainers = Trainer::getTrainers();

        require_once ROOT . '/views/admin/trainer/read.php';

        return true;
    }

    public static function actionUpdate($id, \Doctrine\ORM\EntityManager $em)
    {
        self::checkAdmin($em);

        if ($id) {

            $errors = [];

            $trainer = $em->find('src\Trainer', $id);

            if ($trainer) {

                if ($_POST['trainer_update']) {

                    $trainer->setName($_POST['name']);
                    $trainer->setCode(strtolower(Helper::translit($trainer->getName())));
                    $trainer->setActive($_POST['active'] === 'on' ? 1 : 0);
                    $trainer->setSortable($_POST['sortable']);
                    $trainer->setIntro($_POST['intro']);
                    $trainer->setContent($_POST['content']);
                    $trainer->setUserID($_POST['userID']);

                    if ($error = Validator::checkNameAndCode($trainer->getName(), $trainer->getCode(), 'Имя')) {
                        $errors[] = $error;
                    }
                    if ($error = Validator::checkNumber($trainer->getSortable(), 'Сортировка')) {
                        $errors[] = $error;
                    }

                    if (!count($errors)) {
                        if (Trainer::update($trainer)) {
                            header('Location: /admin/trainer/update/' . $id . '/');
                        } else {
                            $errors[] = 'Тренер не был изменен';
                        }
                    }
                }
            }

            $usersWithRoleTrainer = User::getUsersByRole(User::$roles[User::TRAINER], $em);

            require_once ROOT . '/views/admin/trainer/update.php';

            return true;
        }
        return false;
    }

    public static function actionDelete($id, \Doctrine\ORM\EntityManager $em)
    {
        self::checkAdmin($em);
        if (!$id) {
            return false;
        }
        $trainer = $em->find('src\Trainer', $id);
        if ($trainer) {
            if ($_POST['trainer_delete']) {
                $em->remove($trainer);
                $em->flush();
                header('Location: /admin/trainer/');
            }
        }
        require_once ROOT . '/views/admin/trainer/delete.php';
        return true;
    }
}