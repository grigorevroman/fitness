<?php

use models\Service;
use models\Gym;
use models\Trainer;
use components\Validator;
use components\Helper;
use components\Admin;

class AdminServiceController extends Admin
{
    public static function actionCreate(\Doctrine\ORM\EntityManager $em)
    {
        self::checkAdmin($em);

        $errors = [];

        $service = new entities\FullService();

        if ($_POST['service_create']) {

            $service->setName($_POST['name']);
            $service->setCode(strtolower(Helper::translit($service->getName())));
            $service->setActive($_POST['active'] === 'on' ? 1 : 0);
            $service->setSortable($_POST['sortable']);
            $service->setIntro($_POST['intro']);
            $service->setContent($_POST['content']);
            $service->setPreview($_FILES['preview']['name'] ? json_encode($_FILES['preview'], true) : '');
            $service->setType(Service::$types[$_POST['type']]);
            $service->setPriceDay($_POST['priceDay']);
            $service->setPriceMonth($_POST['priceMonth']);
            $service->setPriceHalfYear($_POST['priceHalfYear']);
            $service->setPriceYear($_POST['priceYear']);
            $service->setGyms(Gym::getGymsByIDs($_POST['gyms'], $em));
            $service->setTrainers($em->getRepository('src\Trainers')->findBy(['id' => $_POST['trainers']]));

            if ($error = Validator::checkNameAndCode($service->getName(), $service->getCode())) {
                $errors[] = $error;
            }
            if ($error = Validator::checkNumber($service->getSortable(), 'Сортировка')) {
                $errors[] = $error;
            }
            if ($error = Validator::checkNumber($service->getPriceDay(), 'Цена за день')) {
                $errors[] = $error;
            }
            if ($error = Validator::checkNumber($service->getPriceMonth(), 'Цена за месяц')) {
                $errors[] = $error;
            }
            if ($error = Validator::checkNumber($service->getPriceHalfYear(), 'Цена за пол-года')) {
                $errors[] = $error;
            }
            if ($error = Validator::checkNumber($service->getPriceYear(), 'Цена за год')) {
                $errors[] = $error;
            }

            if (!count($errors)) {
                if ($id = Service::create($service)) {
                    header('Location: /admin/service/update/' . $id . '/');
                } else {
                    $errors[] = 'Услуга не была добавлена';
                }
            }
        }

        $serviceTypes = Service::$types;
        $gyms = $em->getRepository('src\Gym')->findAll();
        $trainers = Trainer::getTrainers();

        require_once ROOT . '/views/admin/service/create.php';

        return true;
    }

    public static function actionRead(\Doctrine\ORM\EntityManager $em)
    {
        self::checkAdmin($em);

        $services = $em->getRepository('src\Service')->findAll();

        require_once ROOT . '/views/admin/service/read.php';

        return true;
    }

    public static function actionUpdate($id, \Doctrine\ORM\EntityManager $em)
    {
        self::checkAdmin($em);

        if ($id) {

            $errors = [];

            $service = Service::getFullServiceByID($id, $em);

            if ($service) {

                if ($_POST['service_update']) {

                    $service->setName($_POST['name']);
                    $service->setCode(strtolower(Helper::translit($service->getName())));
                    $service->setActive($_POST['active'] === 'on' ? 1 : 0);
                    $service->setSortable($_POST['sortable']);
                    $service->setIntro($_POST['intro']);
                    $service->setContent($_POST['content']);
                    $service->setType(Service::$types[$_POST['type']]);
                    $service->setPriceDay($_POST['priceDay']);
                    $service->setPriceMonth($_POST['priceMonth']);
                    $service->setPriceHalfYear($_POST['priceHalfYear']);
                    $service->setPriceYear($_POST['priceYear']);
                    $service->setGyms(Gym::getGymsByIDs($_POST['gyms'], $em));
                    $service->setTrainers($em->getRepository('src\Trainers')->findBy(['id' => $_POST['trainers']]));

                    if ($error = Validator::checkNameAndCode($service->getName(), $service->getCode())) {
                        $errors[] = $error;
                    }
                    if ($error = Validator::checkNumber($service->getSortable(), 'Сортировка')) {
                        $errors[] = $error;
                    }
                    if ($error = Validator::checkNumber($service->getPriceDay(), 'Цена за день')) {
                        $errors[] = $error;
                    }
                    if ($error = Validator::checkNumber($service->getPriceMonth(), 'Цена за месяц')) {
                        $errors[] = $error;
                    }
                    if ($error = Validator::checkNumber($service->getPriceHalfYear(), 'Цена за пол-года')) {
                        $errors[] = $error;
                    }
                    if ($error = Validator::checkNumber($service->getPriceYear(), 'Цена за год')) {
                        $errors[] = $error;
                    }

                    if (!count($errors)) {
                        if (Service::update($service)) {
                            header('Location: /admin/service/update/' . $id . '/');
                        } else {
                            $errors[] = 'Услуга не была изменена';
                        }
                    }
                }
            }

            $serviceTypes = Service::$types;
            $gyms = $em->getRepository('src\Gym')->findAll();
            $trainers = Trainer::getTrainers();

            require_once ROOT . '/views/admin/service/update.php';

            return true;
        }
        return false;
    }

    public static function actionDelete($id, \Doctrine\ORM\EntityManager $em)
    {
        self::checkAdmin($em);

        if ($id) {

            $errors = [];

            $service = $em->find('src\Service', $id);

            if ($service) {

                if ($_POST['service_delete']) {

                    if (Service::delete($service, $em)) {
                        header('Location: /admin/service/');
                    } else {
                        $errors[] = 'Услуга не была удалена';
                    }
                }
            }

            require_once ROOT . '/views/admin/service/delete.php';

            return true;
        }
        return false;
    }
}
