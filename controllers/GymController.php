<?php

use models\Service;
use models\Gym;
use models\Trainer;

class GymController
{
    public static function actionMain(\Doctrine\ORM\EntityManager $em)
    {
        $services = $em->getRepository('src\Service')->findAll();
        $gyms = $em->getRepository('src\Gym')->findAll();
        $trainers = Trainer::getTrainers();

        require_once ROOT . '/views/gym/main.php';

        return true;
    }

    public static function actionDetail($id, \Doctrine\ORM\EntityManager $em)
    {
        if ($id) {

            $detailGym = Gym::getFullGymByID($id, $em);

            if (!$detailGym->getActive()) {
                header('Location: /gym/');
            }

            $services = $em->getRepository('src\Service')->findAll();
            $gyms = $em->getRepository('src\Gym')->findAll();
            $trainers = Trainer::getTrainers();

            require_once ROOT . '/views/gym/detail.php';

            return true;
        }
        return false;
    }
}
