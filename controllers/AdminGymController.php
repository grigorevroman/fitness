<?php

use models\Gym;
use models\Service;
use components\Validator;
use components\Helper;
use components\Admin;

class AdminGymController extends Admin
{
    public static function actionCreate(\Doctrine\ORM\EntityManager $em)
    {
        self::checkAdmin($em);

        $errors = [];

        $gym = new entities\FullGym();

        if ($_POST['gym_create']) {

            $gym->setName($_POST['name']);
            $gym->setCode(strtolower(Helper::translit($gym->getName())));
            $gym->setActive($_POST['active'] === 'on' ? 1 : 0);
            $gym->setSortable($_POST['sortable']);
            $gym->setIntro($_POST['intro']);
            $gym->setContent($_POST['content']);
            $gym->setPreview($_FILES['preview']['name'] ? json_encode($_FILES['preview'], true) : '');
            $gym->setServices(Service::getServicesByIDs($_POST['services']));

            if ($error = Validator::checkNameAndCode($gym->getName(), $gym->getCode())) {
                $errors[] = $error;
            }
            if ($error = Validator::checkNumber($gym->getSortable(), 'Сортировка')) {
                $errors[] = $error;
            }

            if (!count($errors)) {
                if ($id = Gym::create($gym)) {
                    header('Location: /admin/gym/update/' . $id . '/');
                } else {
                    $errors[] = 'Зал не был добавлен';
                }
            }
        }

        $services = $em->getRepository('src\Service')->findAll();

        require_once ROOT . '/views/admin/gym/create.php';

        return true;
    }

    public static function actionRead(\Doctrine\ORM\EntityManager $em)
    {
        self::checkAdmin($em);

        $gyms = $em->getRepository('src\Gym')->findAll();

        require_once ROOT . '/views/admin/gym/read.php';

        return true;
    }

    public static function actionUpdate($id, \Doctrine\ORM\EntityManager $em)
    {
        self::checkAdmin($em);

        if ($id) {

            $errors = [];

            $gym = Gym::getFullGymByID($id, $em);

            if ($gym) {

                if ($_POST['gym_update']) {

                    $gym->setName($_POST['name']);
                    $gym->setCode(strtolower(Helper::translit($gym->getName())));
                    $gym->setActive($_POST['active'] === 'on' ? 1 : 0);
                    $gym->setSortable($_POST['sortable']);
                    $gym->setIntro($_POST['intro']);
                    $gym->setContent($_POST['content']);
                    $gym->setServices(Service::getServicesByIDs($_POST['services']));

                    if ($error = Validator::checkNameAndCode($gym->getName(), $gym->getCode())) {
                        $errors[] = $error;
                    }
                    if ($error = Validator::checkNumber($gym->getSortable(), 'Сортировка')) {
                        $errors[] = $error;
                    }

                    if (!count($errors)) {
                        if (Gym::update($gym)) {
                            header('Location: /admin/gym/update/' . $id . '/');
                        } else {
                            $errors[] = 'Зал не был изменен';
                        }
                    }
                }
            }

            $services = $em->getRepository('src\Service')->findAll();

            require_once ROOT . '/views/admin/gym/update.php';

            return true;
        }
        return false;
    }

    public static function actionDelete($id, \Doctrine\ORM\EntityManager $em)
    {
        self::checkAdmin($em);

        if ($id) {

            $errors = [];

            $gym = $em->find('src\Gym', $id);

            if ($gym) {

                if ($_POST['gym_delete']) {

                    if (Gym::delete($gym, $em)) {
                        header('Location: /admin/gym/');
                    } else {
                        $errors[] = 'Зал не был удален';
                    }
                }
            }

            require_once ROOT . '/views/admin/gym/delete.php';

            return true;
        }
        return false;
    }
}