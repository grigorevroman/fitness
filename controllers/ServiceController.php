<?php

use models\Service;
use models\Gym;
use models\Trainer;
use models\Order;
use models\User;

class ServiceController
{
    public static function actionMain(\Doctrine\ORM\EntityManager $em)
    {
        $services = $em->getRepository('src\Service')->findAll();
        $gyms = $em->getRepository('src\Gym')->findAll();
        $trainers = $em->getRepository('src\Trainer')->findAll();

        require_once ROOT . '/views/service/main.php';

        return true;
    }

    public static function actionDetail($id, \Doctrine\ORM\EntityManager $em)
    {
        if ($id) {

            $detailService = Service::getFullServiceByID($id, $em);

            if (!$detailService->getActive()) {
                header('Location: /service/');
            }

            $errors = [];

            $order = new entities\Order();

            if ($_POST['order_create']) {

                $price = explode('-', $_POST['price']);

                $dateEnd = new DateTime($_POST['dateBegin']);
                switch ($price[0]) {
                    case 'month':
                        date_modify($dateEnd, '+1 month');
                        break;
                    case 'half_year':
                        date_modify($dateEnd, '+6 month');
                        break;
                    case 'year':
                        date_modify($dateEnd, '+12 month');
                        break;
                }

                $order->setUserID($_SESSION['userID']);
                $order->setServiceID($id);
                $order->setTrainerID($_POST['trainerID']);
                $order->setGymID($_POST['gymID']);
                $order->setDuration($price[0]);
                $order->setPrice($price[1]);
                $order->setDateBegin($_POST['dateBegin']);
                $order->setDateEnd($dateEnd->format("Y-m-d"));

                $orderAdd = false;
                if (!count($errors)) {
                    if (Order::create($order)) {
                        $orderAdd = true;
                    } else {
                        $errors[] = 'Заказ не был оформлен';
                    }
                }
            }

            $services = $em->getRepository('src\Service')->findAll();
            $gyms = $em->getRepository('src\Gym')->findAll();
            $trainers = $em->getRepository('src\Trainer')->findAll();

            require_once ROOT . '/views/service/detail.php';

            return true;
        }
        return false;
    }
}
