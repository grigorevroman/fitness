<?php

use models\Service;
use models\Gym;
use models\Trainer;
use models\User;
use components\Helper;
use components\Validator;

class TrainerController extends \components\Trainer
{
    public static function actionMain(\Doctrine\ORM\EntityManager $em)
    {
        $services = $em->getRepository('src\Service')->findAll();
        $gyms = $em->getRepository('src\Gym')->findAll();
        $trainers = Trainer::getTrainers();

        require_once ROOT . '/views/trainer/main.php';

        return true;
    }

    public static function actionDetail($id, \Doctrine\ORM\EntityManager $em)
    {
        if ($id) {

            $detailTrainer = $em->find('src\Trainer', $id);

            if (!$detailTrainer->getActive()) {
                header('Location: /trainer/');
            }

            $services = $em->getRepository('src\Service')->findAll();
            $gyms = $em->getRepository('src\Gym')->findAll();
            $trainers = Trainer::getTrainers();

            require_once ROOT . '/views/trainer/detail.php';

            return true;
        }
        return false;
    }

    public static function actionCreate(\Doctrine\ORM\EntityManager $em)
    {
        self::checkTrainer($em);

        $errors = [];

        $user = User::getUserBySession($em);

        if ($user->getRole() === User::$roles[User::TRAINER] && $user->getActive()) {

            if ($em->getRepository('src\Trainer')->findBy(['userID' => $user->getId()])[0]) {
                header('Location: /trainer/read/');
            }

            $trainer = new entities\Trainer();

            if ($_POST['trainer_create']) {

                $trainer->setName($_POST['name']);
                $trainer->setCode(strtolower(Helper::translit($trainer->getName())));
                $trainer->setActive($_POST['active'] === 'on' ? 1 : 0);
                $trainer->setSortable($_POST['sortable']);
                $trainer->setIntro($_POST['intro']);
                $trainer->setContent($_POST['content']);
                $trainer->setPreview($_FILES['preview']['name'] ? json_encode($_FILES['preview'], true) : '');
                $trainer->setUserID($user->getID());

                if ($error = Validator::checkNameAndCode($trainer->getName(), $trainer->getCode(), 'Имя')) {
                    $errors[] = $error;
                }
                if ($error = Validator::checkNumber($trainer->getSortable(), 'Сортировка')) {
                    $errors[] = $error;
                }

                if (!count($errors)) {
                    $em->persist($trainer);
                    $em->flush();
                    if ($trainer->getPreview()) {
                        $preview = json_decode($trainer->getPreview(), true);
                        move_uploaded_file($preview['tmp_name'], ROOT . '/upload/' . $preview['name']);
                    }
                    header('Location: /trainer/create/');
                }
            }

            $usersWithRoleTrainer = User::getUsersByRole(User::$roles[User::TRAINER], $em);

            $services = $em->getRepository('src\Service')->findAll();
            $gyms = $em->getRepository('src\Gym')->findAll();
            $trainers = Trainer::getTrainers();

            require_once ROOT . '/views/trainer/create.php';
        }

        return true;
    }

    public static function actionRead(\Doctrine\ORM\EntityManager $em)
    {
        self::checkTrainer($em);

        $user = User::getUserBySession($em);

        if ($user->getRole() === User::$roles[User::TRAINER] && $user->getActive()) {
            $services = $em->getRepository('src\Service')->findAll();
            $gyms = $em->getRepository('src\Gym')->findAll();
            $trainers = $em->getRepository('src\Trainer')->findAll();
            require_once ROOT . '/views/trainer/read.php';
        }

        return true;
    }

    public static function actionUpdate(\Doctrine\ORM\EntityManager $em)
    {
        self::checkTrainer($em);

        $errors = [];

        $user = User::getUserBySession($em);

        if ($user->getRole() === User::$roles[User::TRAINER] && $user->getActive()) {

            if ($trainer = $em->getRepository('src\Trainer')->findBy(['userID' => $user->getId()])[0]) {

                if ($_POST['trainer_update']) {

                    $trainer->setName($_POST['name']);
                    $trainer->setCode(strtolower(Helper::translit($trainer->getName())));
                    $trainer->setActive($_POST['active'] === 'on' ? 1 : 0);
                    $trainer->setSortable($trainer->getSortable());
                    $trainer->setIntro($_POST['intro']);
                    $trainer->setContent($_POST['content']);
                    $trainer->setUserID($trainer->getUserID());

                    if ($error = Validator::checkNameAndCode($trainer->getName(), $trainer->getCode(), 'Имя')) {
                        $errors[] = $error;
                    }
                    if ($error = Validator::checkNumber($trainer->getSortable(), 'Сортировка')) {
                        $errors[] = $error;
                    }

                    if (!count($errors)) {
                        if (Trainer::update($trainer)) {
                            header('Location: /trainer/update/');
                        } else {
                            $errors[] = 'Тренер не был изменен';
                        }
                    }
                }
            }

            $services = $em->getRepository('src\Service')->findAll();
            $gyms = $em->getRepository('src\Gym')->findAll();
            $trainers = Trainer::getTrainers();

            require_once ROOT . '/views/trainer/update.php';
        }
        return true;
    }

    public static function actionDelete(\Doctrine\ORM\EntityManager $em)
    {
        self::checkTrainer($em);
        $user = User::getUserBySession($em);
        if ($user->getRole() === User::$roles[User::TRAINER] && $user->getActive()) {
            if ($trainer = $em->getRepository('src\Trainer')->findBy(['userID' => $user->getId()])[0]) {
                if ($_POST['trainer_delete']) {
                    $em->remove($trainer);
                    $em->flush();
                    header('Location: /trainer/read/');
                }
            }
        }

        $services = $em->getRepository('src\Service')->findAll();
        $gyms = $em->getRepository('src\Gym')->findAll();
        $trainers = Trainer::getTrainers();

        require_once ROOT . '/views/trainer/delete.php';

        return true;
    }
}
