<?php

use models\User;
use models\Order;
use components\Validator;
use components\Helper;
use components\Admin;
use models\Service;
use models\Trainer;

class AdminUserController extends Admin
{
    public static function actionCreate(\Doctrine\ORM\EntityManager $em)
    {
        self::checkAdmin($em);

        $errors = [];

        $user = new src\User();

        if ($_POST['user_create']) {

            $user->setName($_POST['name']);
            $user->setCode(strtolower(Helper::translit($user->getName())));
            $user->setActive($_POST['active'] === 'on' ? 1 : 0);
            $user->setSortable($_POST['sortable']);
            $user->setIntro($_POST['intro']);
            $user->setContent($_POST['content']);
            $user->setPreview($_FILES['preview']['name'] ? json_encode($_FILES['preview'], true) : '');
            $user->setRole(User::$roles[$_POST['role']]);
            $user->setPhone($_POST['phone']);
            $user->setEmail($_POST['email']);
            $user->setGender(User::$genders[$_POST['gender']]);
            $user->setDateOfBirth(new DateTime($_POST['dateOfBirth']));
            $user->setPassword($_POST['password']);
            $user->setNew($_POST['new']);

            if ($error = Validator::checkNameAndCode($user->getName(), $user->getCode(), 'Имя')) {
                $errors[] = $error;
            }
            if ($error = Validator::checkNumber($user->getSortable(), 'Сортировка')) {
                $errors[] = $error;
            }
            if ($error = Validator::checkRequired($user->getEmail(), 'E-mail')) {
                $errors[] = $error;
            }
            if ($error = Validator::checkRequired($user->getPhone(), 'Телефон')) {
                $errors[] = $error;
            }
            if ($error = Validator::checkEmail($user->getEmail())) {
                $errors[] = $error;
            }
            if ($error = Validator::checkPassword($user->getPassword())) {
                $errors[] = $error;
            }

            if (!count($errors)) {
                $em->persist($user);
                $em->flush();
                if ($user->getPreview()) {
                    $preview = json_decode($user->getPreview(), true);
                    move_uploaded_file($preview['tmp_name'], ROOT . '/upload/' . $preview['name']);
                }
                header('Location: /admin/user/update/' . $user->getId() . '/');
            }
        }

        $roles = User::$roles;
        $genders = User::$genders;

        require_once ROOT . '/views/admin/user/create.php';

        return true;
    }

    public static function actionRead(\Doctrine\ORM\EntityManager $em)
    {
        self::checkAdmin($em);
        $users = $em->getRepository('src\User')->findAll();
        require_once ROOT . '/views/admin/user/read.php';
        return true;
    }

    public static function actionUpdate($id, \Doctrine\ORM\EntityManager $em)
    {
        self::checkAdmin($em);

        if ($id) {

            $errors = [];

            $user = $em->find('src\User', $id);

            if ($user) {

                if ($_POST['user_update']) {

                    $user->setName($_POST['name']);
                    $user->setCode(strtolower(Helper::translit($user->getName())));
                    $user->setActive($_POST['active'] === 'on' ? 1 : 0);
                    $user->setSortable($_POST['sortable']);
                    $user->setIntro($_POST['intro']);
                    $user->setContent($_POST['content']);
                    $user->setRole(User::$roles[$_POST['role']]);
                    $user->setEmail($_POST['email']);
                    $user->setPhone($_POST['phone']);
                    $user->setGender(User::$genders[$_POST['gender']]);
                    $user->setDateOfBirth(new DateTime($_POST['dateOfBirth']));
                    $user->setPassword($_POST['password']);
                    $user->setNew($_POST['new']);

                    if ($error = Validator::checkNameAndCode($user->getName(), $user->getCode(), 'Имя')) {
                        $errors[] = $error;
                    }
                    if ($error = Validator::checkNumber($user->getSortable(), 'Сортировка')) {
                        $errors[] = $error;
                    }
                    if ($error = Validator::checkRequired($user->getEmail(), 'E-mail')) {
                        $errors[] = $error;
                    }
                    if ($error = Validator::checkRequired($user->getPhone(), 'Телефон')) {
                        $errors[] = $error;
                    }
                    if ($error = Validator::checkEmail($user->getEmail())) {
                        $errors[] = $error;
                    }
                    if ($error = Validator::checkPassword($user->getPassword())) {
                        $errors[] = $error;
                    }

                    if (!count($errors)) {
                        $em->persist($user);
                        $em->flush();
                        if ($user->getPreview()) {
                            $preview = json_decode($user->getPreview(), true);
                            move_uploaded_file($preview['tmp_name'], ROOT . '/upload/' . $preview['name']);
                        }
                        header('Location: /admin/user/update/' . $id . '/');
                    }
                }
            }

            $roles = User::$roles;
            $genders = User::$genders;

            $services = $em->getRepository('src\Service')->findAll();
            $gyms = $em->getRepository('src\Gym')->findAll();
            $trainers = Trainer::getTrainers();

            $orders = Order::getFullOrdersByUser($user);

            require_once ROOT . '/views/admin/user/update.php';

            return true;
        }
        return false;
    }

    public static function actionDelete($id, \Doctrine\ORM\EntityManager $em)
    {
        self::checkAdmin($em);

        if ($id) {

            $errors = [];

            $user = $em->find('src\User', $id);

            if ($user) {

                if ($_POST['user_delete']) {

                    if (User::delete($user, $em)) {
                        header('Location: /admin/user/');
                    } else {
                        $errors[] = 'Услуга не была удалена';
                    }
                }
            }

            require_once ROOT . '/views/admin/user/delete.php';

            return true;
        }
        return false;
    }

    public static function actionOrder($id, \Doctrine\ORM\EntityManager $em)
    {
        self::checkAdmin($em);

        if ($id) {

            $user = $em->find('src\User', $id);

            $orders = Order::getFullOrdersByUser($user);

            $services = $em->getRepository('src\Service')->findAll();
            $gyms = $em->getRepository('src\Gym')->findAll();
            $trainers = Trainer::getTrainers();

            if ($_POST['pay_submit']) {
                if ($_POST['order_id']) {
                    $id = $_POST['order_id'];
                    $pay = 0;
                    if ($_POST['pay']) {
                        $pay = 1;
                    }
                    Order::updateOrderPay($id, $pay);
                    header('Location: ' . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
                }
            }

            require_once ROOT . '/views/admin/user/order.php';

            return true;
        }
        return false;
    }
}
