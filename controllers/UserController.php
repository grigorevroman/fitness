<?php

use models\Service;
use models\Trainer;
use models\User;
use models\Order;
use components\Validator;
use components\Helper;

class UserController
{
    public static function actionLogin(\Doctrine\ORM\EntityManager $em)
    {
        $errors = [];

        $user = new entities\User();

        if ($_POST['login']) {

            $user->setEmail($_POST['email']);
            $user->setPassword($_POST['password']);

            if ($error = Validator::checkEmail($user->getEmail())) {
                $errors[] = $error;
            }
            if ($error = Validator::checkPassword($user->getPassword())) {
                $errors[] = $error;
            }

            if (!count($errors)) {

                $currentUser = $em->getRepository('src\User')
                    ->findBy(['email' => $user->getEmail(), 'password' => $user->getPassword()])[0];

                if ($currentUser) {

                    $authorization = User::authorization($currentUser);

                    if ($authorization) {

                        if ($currentUser->getRole() === User::$roles[User::ADMIN]) {
                            header('Location: /admin/');
                        } else {
                            header('Location: /');
                        }
                    }
                } else {
                    $errors[] = 'Неправильные данные для входа на сайт';
                }
            }
        }

        $services = $em->getRepository('src\Service')->findAll();
        $gyms = $em->getRepository('src\Gym')->findAll();
        $trainers = Trainer::getTrainers();

        require_once ROOT . '/views/user/login.php';

        return true;
    }

    public static function actionLogout()
    {
        unset($_SESSION['userID']);
        header('Location: /');

        return true;
    }

    public static function actionRegistration(\Doctrine\ORM\EntityManager $em)
    {
        $errors = [];
        $isRegistration = false;

        $user = new src\User();

        if ($_POST['registration']) {

            $user->setName($_POST['name']);
            $user->setCode(strtolower(Helper::translit($user->getName())));
            $user->setActive(1);
            $user->setSortable(0);
            $user->setIntro('');
            $user->setContent('');
            $user->setPreview($_FILES['preview']['name'] ? json_encode($_FILES['preview'], true) : '');
            $user->setRole(User::$roles[User::CLIENT]);
            $user->setPhone($_POST['phone']);
            $user->setEmail($_POST['email']);
            $user->setGender(User::$genders[$_POST['gender']]);
            $user->setDateOfBirth(new DateTime($_POST['dateOfBirth']));
            $user->setPassword($_POST['password']);
            $user->setNew($_POST['new']);

            if ($error = Validator::checkNameAndCode($user->getName(), $user->getCode(), 'Имя')) {
                $errors[] = $error;
            }
            if ($error = Validator::checkNumber($user->getSortable(), 'Сортировка')) {
                $errors[] = $error;
            }
            if ($error = Validator::checkRequired($user->getEmail(), 'E-mail')) {
                $errors[] = $error;
            }
            if ($error = Validator::checkRequired($user->getPhone(), 'Телефон')) {
                $errors[] = $error;
            }
            if ($error = Validator::checkPassword($user->getPassword())) {
                $errors[] = $error;
            }

            if (!count($errors)) {
                $em->persist($user);
                $em->flush();
                if ($user->getPreview()) {
                    $preview = json_decode($user->getPreview(), true);
                    move_uploaded_file($preview['tmp_name'], ROOT . '/upload/' . $preview['name']);
                }
                $isRegistration = true;
            }
        }

        $services = $em->getRepository('src\Service')->findAll();
        $gyms = $em->getRepository('src\Gym')->findAll();
        $trainers = Trainer::getTrainers();

        $genders = User::$genders;

        require_once ROOT . '/views/user/registration.php';

        return true;
    }

    public static function actionUpdate(\Doctrine\ORM\EntityManager $em)
    {
        $errors = [];

        $user = User::getUserBySession($em);

        if (!$user) {
            header('Location: /');
        }

        if ($user->getID()) {

            if ($_POST['user_update']) {

                $user->setName($_POST['name']);
                $user->setCode(strtolower(Helper::translit($user->getName())));
                $user->setActive($user->getActive());
                $user->setSortable($user->getSortable());
                $user->setIntro($_POST['intro']);
                $user->setContent($_POST['content']);
                $user->setRole($user->getRole());
                $user->setEmail($_POST['email']);
                $user->setPhone($_POST['phone']);
                $user->setGender(User::$genders[$_POST['gender']]);
                $user->setDateOfBirth(new DateTime($_POST['dateOfBirth']));
                $user->setPassword($_POST['password']);
                $user->setNew($_POST['new']);

                if ($error = Validator::checkNameAndCode($user->getName(), $user->getCode(), 'Имя')) {
                    $errors[] = $error;
                }
                if ($error = Validator::checkNumber($user->getSortable(), 'Сортировка')) {
                    $errors[] = $error;
                }
                if ($error = Validator::checkRequired($user->getEmail(), 'E-mail')) {
                    $errors[] = $error;
                }
                if ($error = Validator::checkRequired($user->getPhone(), 'Телефон')) {
                    $errors[] = $error;
                }
                if ($error = Validator::checkEmail($user->getEmail())) {
                    $errors[] = $error;
                }
                if ($error = Validator::checkPassword($user->getPassword())) {
                    $errors[] = $error;
                }

                if (!count($errors)) {
                    $em->persist($user);
                    $em->flush();
                    if ($user->getPreview()) {
                        $preview = json_decode($user->getPreview(), true);
                        move_uploaded_file($preview['tmp_name'], ROOT . '/upload/' . $preview['name']);
                    }
                    header('Location: /user/update/');
                }
            }
        }

        $services = $em->getRepository('src\Service')->findAll();
        $gyms = $em->getRepository('src\Gym')->findAll();
        $trainers = Trainer::getTrainers();

        $genders = User::$genders;

        require_once ROOT . '/views/user/update.php';

        return true;
    }

    public static function actionOrder(\Doctrine\ORM\EntityManager $em)
    {
        $user = User::getUserBySession($em);

        if (!$user) {
            header('Location: /');
        }

        $orders = Order::getFullOrdersByUser($user);

        $services = $em->getRepository('src\Service')->findAll();
        $gyms = $em->getRepository('src\Gym')->findAll();
        $trainers = Trainer::getTrainers();

        require_once ROOT . '/views/user/order.php';

        return true;
    }

    public static function actionMain(\Doctrine\ORM\EntityManager $em)
    {
        $user = User::getUserBySession($em);

        if (!$user) {
            header('Location: /');
        }

        $services = $em->getRepository('src\Service')->findAll();
        $gyms = $em->getRepository('src\Gym')->findAll();
        $trainers = Trainer::getTrainers();

        require_once ROOT . '/views/user/main.php';

        return true;
    }
}
