<?php

use models\Timing;
use models\User;
use models\Order;
use models\Record;

class TimetableController
{
    public function actionMain(\Doctrine\ORM\EntityManager $em)
    {
        if (User::isGuest()) {
            header('Location: /');
        }

        $dates = Timing::getDatesSelect();
        /** @var entities\Timing[] $timings */
        $timings = Timing::getTimings();

        $table = [];
        foreach ($dates as $date) {
            $table[$date] = [];
            foreach ($timings as $timing) {
                if (count(json_decode($timing->getDates(), true))) {
                    if (in_array($date, json_decode($timing->getDates(), true))) {
                        $table[$date][$timing->getID()] = $timing;
                    }
                }
            }
        }

        $currentUser = User::getUserBySession($em);
        $currentUserOrders = Order::getFullOrdersByUser($currentUser);

        $record = new \entities\Record();

        if ($_POST['record_submit']) {

            $record->setServiceID($_POST['serviceID']);
            $record->setTimingID($_POST['timingID']);
            $record->setOrderID($_POST['orderID']);
            $record->setUserID($_POST['userID']);
            $record->setDate($_POST['date']);

            Record::create($record);
        }

        if ($_POST['record_submit_1']) {
            $record->setID($_POST['recordID']);
            Record::delete($record);
        }

        require_once ROOT . '/views/timetable/main.php';

        return true;
    }
}