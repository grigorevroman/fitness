<?php

use models\TimetableService;
use models\Service;
use components\Admin;
use components\Helper;
use components\Validator;

class AdminTimetableServiceController extends Admin
{
    public static function actionCreate(\Doctrine\ORM\EntityManager $em)
    {
        self::checkAdmin($em);

        $errors = [];

        $timetableService = new entities\TimetableService();

        if ($_POST['timetable_service_create']) {

            $timetableService->setName($_POST['name']);
            $timetableService->setCode(strtolower(Helper::translit($timetableService->getName())));
            $timetableService->setActive($_POST['active'] === 'on' ? 1 : 0);
            $timetableService->setSortable($_POST['sortable']);
            $timetableService->setServiceID($_POST['serviceID']);
            $timetableService->setService($em->find('src\Service', $_POST['serviceID']));

            if ($error = Validator::checkNameAndCode($timetableService->getName(), $timetableService->getCode())) {
                $errors[] = $error;
            }
            if ($error = Validator::checkNumber($timetableService->getSortable(), 'Сортировка')) {
                $errors[] = $error;
            }

            if (!count($errors)) {
                if ($id = TimetableService::create($timetableService)) {
                    header('Location: /admin/timetable/service/update/' . $id . '/');
                } else {
                    $errors[] = 'Услуга не была добавлена';
                }
            }
        }

        $services = $em->getRepository('src\Service')->findAll();

        require_once ROOT . '/views/admin/timetable/service/create.php';

        return true;
    }

    public static function actionRead(\Doctrine\ORM\EntityManager $em)
    {
        self::checkAdmin($em);

        $timetableService = TimetableService::getTimetableServices($em);

        require_once ROOT . '/views/admin/timetable/service/read.php';

        return true;
    }
}
