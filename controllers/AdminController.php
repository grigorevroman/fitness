<?php

use components\Admin;

class AdminController extends Admin
{
    public function actionMain(\Doctrine\ORM\EntityManager $em)
    {
        self::checkAdmin($em);

        require_once ROOT . '/views/admin/main.php';

        return true;
    }
}