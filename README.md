# README #

### Deployment instructions ###

* Установить Open Server и запустить.
* Установть Git Bash и запустить.
* В Git Bash перейти в папку C:/openserver/domains/.
* Склонировать репозиторий при помощи команды: git clone https://grigorevroman@bitbucket.org/grigorevroman/fitness.git.
* Настроить домен: fitness/ в Open Server и указать на папку C:/openserver/domains/fitness/.
* Установить Composer.
* В консоли Open Server перейти в папку: C:/openserver/domains/fitness/ и выполнить команду: composer install.
* Установить node.js.
* Выполнить в Git Bash в корне проекта команду: npm install.
* Создать БД fitness (host: localhost, user: root, pass пустой).
* Настройка phinx. Скопировать файл phinx.yml из папки /deploy/ в папку /vendor/bin/. Установить в файле БД по умолчанию.
* Выполнить миграции из папки /vendor/bin/ командой phinx migrate.

* Запустить проект в браузере: fitness/.
