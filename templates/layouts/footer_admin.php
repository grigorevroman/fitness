</div>
<div class="footer">
    <div class="tm-footer">
        <div class="uk-container uk-container-center uk-text-center">
            <ul class="uk-subnav uk-subnav-line uk-flex-center">
                <li><a href="/">Дионика</a></li>
                <li><a href="/service/">Услуги</a></li>
                <li><a href="/gym/">Залы</a></li>
                <li><a href="/trainer/">Тренеры</a></li>
                <li><a href="/about/">О нас</a></li>
                <li><a href="/timetable/">Расписание</a></li>
            </ul>
            <div class="uk-panel" style="top: -5px;">
                <p>Контактная информация:</p>
                <p>тел. 89371455745</p>
                <p>г. Саратов ул. Мамонтовой, 4 а</p>
                <p>Будни 7:00 - 23:00</p>
                <p>Выходные 09:00 - 21:00</p>
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>