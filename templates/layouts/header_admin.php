<!DOCUMENT html>
<html>
<head>
    <meta charset="utf-8">
    <title>Дионика</title>
    <link rel="stylesheet" href="/node_modules/codemirror/lib/codemirror.css">
    <link rel="stylesheet" type="text/css" href="/node_modules/uikit/dist/css/uikit.almost-flat.min.css">
    <link rel="stylesheet" type="text/css" href="/node_modules/uikit/dist/css/components/form-advanced.almost-flat.min.css">
    <link rel="stylesheet" type="text/css" href="/node_modules/uikit/dist/css/components/htmleditor.almost-flat.min.css">
    <link rel="stylesheet" type="text/css" href="/node_modules/uikit/dist/css/components/form-select.almost-flat.min.css">
    <link rel="stylesheet" type="text/css" href="/node_modules/uikit/dist/css/components/datepicker.almost-flat.min.css">
    <link rel="stylesheet" type="text/css" href="/node_modules/uikit/dist/css/components/slideshow.almost-flat.min.css">
    <link rel="stylesheet" type="text/css" href="/node_modules/uikit/dist/css/components/slidenav.almost-flat.min.css">
    <link rel="stylesheet" type="text/css" href="/node_modules/uikit/dist/css/components/dotnav.almost-flat.min.css">
    <link rel="stylesheet" type="text/css" href="/node_modules/uikit/dist/css/components/slider.almost-flat.min.css">
    <link rel="stylesheet" type="text/css" href="/node_modules/uikit/dist/css/components/form-password.almost-flat.css">
    <link rel="stylesheet" type="text/css" href="/node_modules/uikit/dist/css/components/tooltip.almost-flat.css">
    <link rel="stylesheet" type="text/css" href="/node_modules/chosen-js/chosen.min.css">
    <link rel="stylesheet" type="text/css" href="/templates/css/style.css">
    <script src="/node_modules/uikit/vendor/jquery.js"></script>
    <script src="/node_modules/codemirror/lib/codemirror.js"></script>
    <script src="/node_modules/codemirror/mode/markdown/markdown.js"></script>
    <script src="/node_modules/codemirror/addon/mode/overlay.js"></script>
    <script src="/node_modules/codemirror/mode/xml/xml.js"></script>
    <script src="/node_modules/codemirror/mode/gfm/gfm.js"></script>
    <script src="/node_modules/marked/lib/marked.js"></script>
    <script src="/node_modules/uikit/dist/js/uikit.min.js"></script>
    <script src="/node_modules/uikit/dist/js/components/htmleditor.js"></script>
    <script src="/node_modules/uikit/dist/js/components/form-select.min.js"></script>
    <script src="/node_modules/uikit/dist/js/components/datepicker.min.js"></script>
    <script src="/node_modules/uikit/dist/js/components/slideshow.min.js"></script>
    <script src="/node_modules/uikit/dist/js/components/slider.min.js"></script>
    <script src="/node_modules/uikit/dist/js/components/form-password.min.js"></script>
    <script src="/node_modules/uikit/dist/js/components/tooltip.min.js"></script>
    <script src="/node_modules/jquery.maskedinput/src/jquery.maskedinput.js"></script>
    <script src="/node_modules/chosen-js/chosen.jquery.min.js"></script>
    <script src="/templates/js/scripts.js"></script>
</head>
<body>
<div class="wrapper">
    <div class="content uk-container uk-container-center">
        <nav class="uk-navbar uk-margin-top">
            <ul class="uk-navbar-nav">
                <li><a href="/">Дионика</a></li>
                <li class="uk-parent" data-uk-dropdown="" aria-haspopup="true" aria-expanded="false">
                    <a href="">Меню <i class="uk-icon-caret-down"></i></a>
                    <div class="uk-dropdown uk-dropdown-navbar uk-dropdown-bottom" aria-hidden="true" tabindex="">
                        <ul class="uk-nav uk-nav-navbar">
                            <li><a href="/service/">Услуги</a></li>
                            <li><a href="/gym/">Залы</a></li>
                            <li><a href="/trainer/">Тренеры</a></li>
                        </ul>
                    </div>
                </li>
                <?if(!models\User::isGuest()):?>
                    <li><a href="/timetable/">Расписание</a></li>
                    <?if($currentUser = models\User::getUserBySession($em)):?>
                        <?if($currentUser->getRole() === models\User::$roles[models\User::ADMIN]):?>
                            <li><a href="/report/">Отчет</a></li>
                        <?endif;?>
                    <?endif;?>
                <?else:?>
                    <li><a class="uk-button" data-uk-tooltip title="Для просмотра расписания необходимо авторизоваться" href="/user/login/">Расписание</a></li>
                <?endif;?>
                <li><a href="/about/">О нас</a></li>
            </ul>
            <div class="uk-navbar-flip">
                <ul class="uk-navbar-nav">
                    <?if(models\User::isGuest()):?>
                        <li><a href="/user/login/">Войти</a></li>
                        <li><a href="/user/registration/">Регистрация</a></li>
                    <?elseif($currentUser = models\User::getUserBySession($em)):?>
                        <?if($currentUser->getRole() === models\User::$roles[models\User::ADMIN]):?>
                            <li><a href="/user/"><?=$currentUser->getName()?></a></li>
                            <li><a href="/admin/">Администратор</a></li>
                        <?elseif($currentUser->getRole() === models\User::$roles[models\User::TRAINER]):?>
                            <li><a href="/user/"><?=$currentUser->getName()?></a></li>
                            <li><a href="/trainer/read/">Тренер</a></li>
                        <?elseif($currentUser->getRole() === models\User::$roles[models\User::MANAGER]):?>
                            <li><a href="/user/"><?=$currentUser->getName()?></a></li>
                            <li><a href="/manager/timing/">Менеджер</a></li>
                        <?elseif($currentUser->getRole() === models\User::$roles[models\User::CLIENT]):?>
                            <li><a href="/user/"><?=$currentUser->getName()?></a></li>
                        <?endif;?>
                        <li><a href="/user/logout/">Выйти</a></li>
                    <?endif;?>
                </ul>
            </div>
        </nav>