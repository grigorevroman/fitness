<?
/** @var entities\Service[] $services */
/** @var entities\Gym[] $gyms */
/** @var entities\Trainer[] $trainers */
/** @var entities\FullService $detailService */
/** @var $orderAdd boolean */
require_once ROOT . '/templates/layouts/header_admin.php'?>
<div class="uk-grid uk-grid-small uk-margin-top uk-margin-large-bottom">
    <div class="uk-width-4-5">
        <h1><?=$detailService->getName()?></h1>
        <ul class="uk-breadcrumb">
            <li><a href="/">Дионика</a></li>
            <li><a href="/service/">Услуги</a></li>
            <li class="uk-active"><span><?=$detailService->getName()?></span></li>
        </ul>
        <div>
            <img src="<?=$detailService->getPreviewName() ? '/upload/' . $detailService->getPreviewName() : '/templates/images/placeholder1000x400.jpg'?>" class="uk-margin-bottom">
        </div>
        <div class="uk-grid uk-margin-large-bottom">
            <div class="uk-width-2-5">
                <table class="uk-table uk-table-hover">
                    <tr>
                        <td>Одна тренировка</td>
                        <td style="text-align: right;"><?=$detailService->getPriceDay()?> <i class="uk-icon-rub"></i></td>
                    </tr>
                    <tr>
                        <td>Абонемент на месяц</td>
                        <td style="text-align: right;"><?=$detailService->getPriceMonth()?> <i class="uk-icon-rub"></i></td>
                    </tr>
                    <tr>
                        <td>Абонемент на пол-года</td>
                        <td style="text-align: right;"><?=$detailService->getPriceHalfYear()?> <i class="uk-icon-rub"></i></td>
                    </tr>
                    <tr>
                        <td>Абонемент на год</td>
                        <td style="text-align: right;"><?=$detailService->getPriceYear()?> <i class="uk-icon-rub"></i></td>
                    </tr>
                </table>
            </div>
            <div class="uk-width-3-5"><?=$detailService->getContent()?></div>
        </div>
        <?if(count($detailService->getGyms())):?>
            <div class="uk-margin-large-bottom">
                <p class="uk-h3">Залы</p>
                <?
                /** @var entities\Gym $gym */
                foreach($detailService->getGyms() as $gym):?>
                    <div class="uk-grid uk-grid-small uk-margin-bottom">
                        <div class="uk-width-3-5">
                            <img src="<?=$gym->getPreviewName() ? '/upload/' . $gym->getPreviewName() : '/templates/images/placeholder1000x400.jpg'?>">
                        </div>
                        <div class="uk-width-2-5">
                            <p class="uk-h3"><?=$gym->getName()?></p>
                            <p><?=$gym->getIntro()?></p>
                            <a class="uk-button uk-button-primary" href="/gym/<?=$gym->getID()?>/">Смотреть</a>
                        </div>
                    </div>
                <?endforeach;?>
            </div>
        <?endif;?>
        <?if(count($detailService->getTrainers())):?>
            <p class="uk-h3">Тренеры</p>
            <div class="uk-grid uk-grid-small uk-margin-large-bottom">
                <?
                /** @var entities\Trainer $trainer */
                foreach($detailService->getTrainers() as $trainer):?>
                    <div class="uk-width-1-4">
                        <div class="uk-panel uk-panel-box">
                            <div class="uk-panel-teaser">
                                <img src="<?=$trainer->getPreviewName() ? '/upload/' . $trainer->getPreviewName() : '/templates/images/placeholder_avatar.jpg'?>" draggable="false">
                            </div>
                            <p>
                                <a class="uk-button uk-button-primary" href="/trainer/<?=$trainer->getID()?>/" draggable="false">
                                    <?=$trainer->getName()?>
                                </a>
                            </p>
                            <p><?=$trainer->getIntro()?></p>
                        </div>
                    </div>
                <?endforeach;?>
            </div>
        <?endif;?>
        <div class="uk-panel-box">
            <p class="uk-h3">Оформить услугу</p>
            <?if($orderAdd):?>
                <div class="uk-alert uk-alert-success" data-uk-alert="">
                    <a href="" class="uk-alert-close uk-close"></a>
                    <p>Услуга была добавлена</p>
                </div>
            <?endif;?>
            <?if(models\User::isGuest()):?>
                <div>Для оформления необходимо <a href="/user/login/">войти</a></div>
            <?else:?>
                <div class="uk-grid uk-grid-small">
                    <form method="post" enctype="multipart/form-data">
                        <div class="uk-form uk-form-stacked uk-margin-bottom">
                            <div class="uk-form-row">
                                <label class="uk-form-label" for="chosen-select-price">Продолжительность и цена:</label>
                                <select id="chosen-select-price" class="chosen-select-price" name="price" tabindex="-1">
                                    <option value="day-<?=$detailService->getPriceDay()?>">Одна тренеровка: <?=$detailService->getPriceDay()?>р.</option>
                                    <option value="month-<?=$detailService->getPriceMonth()?>">Абонемент на месяц: <?=$detailService->getPriceMonth()?>р.</option>
                                    <option value="half_year-<?=$detailService->getPriceHalfYear()?>">Абонемент на пол-года: <?=$detailService->getPriceHalfYear()?>р.</option>
                                    <option value="year-<?=$detailService->getPriceYear()?>">Абонемент на год: <?=$detailService->getPriceYear()?>р.</option>
                                </select>
                                <script>
                                    $('.chosen-select-price').chosen();
                                </script>
                            </div>
                            <div class="uk-form-row">
                                <label class="uk-form-label" for="dateBegin">Дата начала тренеровок:</label>
                                <div class="uk-form-controls">
                                    <input type="text" data-uk-datepicker="{format:'YYYY-MM-DD'}" name="dateBegin" id="dateBegin" value="<?=date("Y-m-d")?>">
                                </div>
                            </div>
                        </div>
                        <input type="submit" name="order_create" class="uk-button uk-button-success" value="Оформить">
                    </form>
                </div>
            <?endif;?>
        </div>
    </div>
    <div class="uk-width-1-5">
        <div class="uk-panel-box">
            <ul class="uk-nav uk-nav-parent-icon uk-nav-side" data-uk-nav="">
                <li class="uk-parent uk-active">
                    <a href="#">Услуги</a>
                    <ul class="uk-nav-sub">
                        <?foreach($services as $service):?>
                            <?if($service->getActive()):?>
                                <li class="<?=$service->getID() === $detailService->getID() ? 'uk-active' : ''?>"><a href="/service/<?=$service->getID()?>/"><?=$service->getName()?></a></li>
                            <?endif;?>
                        <?endforeach;?>
                    </ul>
                </li>
                <li class="uk-parent">
                    <a href="#">Залы</a>
                    <ul class="uk-nav-sub">
                        <?foreach($gyms as $gym):?>
                            <?if($gym->getActive()):?>
                                <li><a href="/gym/<?=$gym->getID()?>/"><?=$gym->getName()?></a></li>
                            <?endif;?>
                        <?endforeach;?>
                    </ul>
                </li>
                <li class="uk-parent">
                    <a href="#">Тренеры</a>
                    <ul class="uk-nav-sub">
                        <?foreach($trainers as $trainer):?>
                            <?if($trainer->getActive()):?>
                                <li><a href="/trainer/<?=$trainer->getID()?>/"><?=$trainer->getName()?></a></li>
                            <?endif;?>
                        <?endforeach;?>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<?require_once ROOT . '/templates/layouts/footer_admin.php'?>