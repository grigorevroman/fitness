<?require_once ROOT . '/templates/layouts/header_admin.php'?>
<div class="uk-grid uk-grid-small uk-margin-top uk-margin-large-bottom">
    <div class="uk-width-4-5">
        <h1>Администратор</h1>
        <ul class="uk-breadcrumb">
            <li><a href="/">Дионика</a></li>
            <li class="uk-active"><span>Администратор</span></li>
        </ul>
        <div>
            <a class="uk-button uk-button-primary uk-margin-small-bottom" href="/admin/service/">Управление услугами</a>
            <a class="uk-button uk-button-primary uk-margin-small-bottom" href="/admin/gym/">Управление залами</a>
            <a class="uk-button uk-button-primary uk-margin-small-bottom" href="/admin/trainer/">Управление тренерами</a>
            <a class="uk-button uk-button-primary uk-margin-small-bottom" href="/admin/user/">Управление пользователями</a>
            <a class="uk-button uk-button-primary uk-margin-small-bottom" href="/admin/timing/">Управление расписанием</a>
        </div>
    </div>
    <div class="uk-width-1-5">
        <div class="uk-panel-box">
            <ul class="uk-nav uk-nav-side">
                <li><a href="/admin/service/">Управление услугами</a></li>
                <li><a href="/admin/gym/">Управление залами</a></li>
                <li><a href="/admin/trainer/">Управление тренерами</a></li>
                <li><a href="/admin/user/">Управление пользователями</a></li>
                <li><a href="/admin/timing/">Управление расписанием</a></li>
            </ul>
        </div>
    </div>
</div>
<?require_once ROOT . '/templates/layouts/footer_admin.php'?>