<?
/** @var entities\Service[] $services */
require_once ROOT . '/templates/layouts/header_admin.php'?>
<div class="uk-grid uk-grid-small uk-margin-top uk-margin-large-bottom">
    <div class="uk-width-4-5">
        <h1>Управление услугами</h1>
        <ul class="uk-breadcrumb">
            <li><a href="/">Дионика</a></li>
            <li><a href="/admin/"><span>Администратор</span></a></li>
            <li class="uk-active"><span>Управление услугами</span></li>
        </ul>
        <div>
            <?foreach($services as $service):?>
                <div class="uk-margin-small-bottom uk-h3">
                    <?=$service->getName()?>
                    <a class="uk-button uk-button-primary <?=$service->getActive() ? '' : 'button-inactive'?>" href="/admin/service/update/<?=$service->getID()?>/">Изменить</a>
                    <a class="uk-button uk-button-danger <?=$service->getActive() ? '' : 'button-inactive'?>" href="/admin/service/delete/<?=$service->getID()?>/">Удалить</a>
                </div>
            <?endforeach;?>
            <a class="uk-button uk-button-success uk-margin-small-top" href="/admin/service/create/">Добавить</a>
        </div>
    </div>
    <div class="uk-width-1-5">
        <div class="uk-panel-box">
            <ul class="uk-nav uk-nav-side">
                <li class="uk-active"><a href="/admin/service/">Управление услугами</a></li>
                <li><a href="/admin/gym/">Управление залами</a></li>
                <li><a href="/admin/trainer/">Управление тренерами</a></li>
                <li><a href="/admin/user/">Управление пользователями</a></li>
                <li><a href="/admin/timing/">Управление расписанием</a></li>
            </ul>
        </div>
    </div>
</div>
<?require_once ROOT . '/templates/layouts/footer_admin.php'?>