<?
/** @var $serviceTypes[] */
/** @var $errors[] */
/** @var entities\FullService $service */
/** @var entities\Gym[] $gyms */
/** @var entities\Trainer[] $trainers */
require_once ROOT . '/templates/layouts/header_admin.php'?>
<div class="uk-grid uk-grid-small uk-margin-top uk-margin-large-bottom">
    <div class="uk-width-4-5">
        <h1>Изменить услугу</h1>
        <ul class="uk-breadcrumb">
            <li><a href="/">Дионика</a></li>
            <li><a href="/admin/"><span>Администратор</span></a></li>
            <li><a href="/admin/service/">Управление услугами</a></li>
            <li class="uk-active"><span>Изменить услугу</span></li>
        </ul>
        <?foreach($errors as $error):?>
            <div class="uk-alert uk-alert-danger" data-uk-alert="">
                <a href="" class="uk-alert-close uk-close"></a>
                <p><?=$error?></p>
            </div>
        <?endforeach;?>
        <form method="post" enctype="multipart/form-data">
            <div class="uk-form uk-form-stacked uk-margin-bottom">
                <div class="uk-form-row">
                    <label class="uk-form-label" for="name">Название</label>
                    <div class="uk-form-controls">
                        <input type="text" id="name" placeholder="Название" name="name" value="<?=$service->getName()?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="active">Активность</label>
                    <div class="uk-form-controls">
                        <input type="checkbox" id="active" name="active" <?=$service->getActive() ? 'checked' : ''?>>
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="sortable">Сортировка</label>
                    <div class="uk-form-controls">
                        <input type="text" id="sortable" placeholder="Сортировка" name="sortable" value="<?=$service->getSortable()?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="intro">Вступление</label>
                    <div class="uk-form-controls">
                        <input type="text" id="intro" placeholder="Вступление" name="intro" value="<?=$service->getIntro()?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="content">Контент</label>
                    <div class="uk-form-controls">
                        <textarea data-uk-htmleditor="{maxsplitsize:600}" data-uk-check-display="1" id="content" name="content"><?=$service->getContent()?></textarea>
                    </div>
                </div>
                <?if($service->getPreviewName()):?>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="preview">Превью</label>
                        <div class="uk-form-controls">
                            <div class="uk-margin-small-bottom"><img width="250" src="/upload/<?=$service->getPreviewName()?>" alt=""></div>
                        </div>
                    </div>
                <?endif;?>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="type">Тип</label>
                    <div class="uk-form-controls">
                        <select id="type" name="type" data-placeholder="Тип" class="chosen-select-type" tabindex="-1">
                            <option value="">Нет</option>
                            <?foreach ($serviceTypes as $typeCode => $typeName):?>
                                <option value="<?=$typeCode?>" <?=$typeName === $service->getType() ? 'selected' : ''?>><?=$typeName?></option>
                            <?endforeach;?>
                        </select>
                        <script>
                            $('.chosen-select-type').chosen();
                        </script>
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="priceDay">Одна тренировка</label>
                    <div class="uk-form-controls">
                        <input type="text" id="priceDay" placeholder="Цена за день" name="priceDay" value="<?=$service->getPriceDay()?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="priceMonth">Абонемент на месяц</label>
                    <div class="uk-form-controls">
                        <input type="text" id="priceMonth" placeholder="Цена за месяц" name="priceMonth" value="<?=$service->getPriceMonth()?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="priceHalfYear">Абонемент на пол-года</label>
                    <div class="uk-form-controls">
                        <input type="text" id="priceHalfYear" placeholder="Цена за пол-года" name="priceHalfYear" value="<?=$service->getPriceHalfYear()?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="priceYear">Абонемент на год</label>
                    <div class="uk-form-controls">
                        <input type="text" id="priceYear" placeholder="Цена за год" name="priceYear" value="<?=$service->getPriceYear()?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="gyms">Залы</label>
                    <div class="uk-form-controls">
                        <select id="gyms" name="gyms[]" data-placeholder="Залы" class="chosen-select-gyms" tabindex="-1" multiple="">
                            <?foreach ($gyms as $gym):?>
                                <option value="<?=$gym->getID()?>" <?=array_key_exists($gym->getID(), $service->getGyms()) ? 'selected' : ''?>>
                                    <?=$gym->getName()?>
                                </option>
                            <?endforeach;?>
                        </select>
                        <script>
                            $('.chosen-select-gyms').chosen();
                        </script>
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="trainers">Тренеры</label>
                    <div class="uk-form-controls">
                        <select id="trainers" name="trainers[]" data-placeholder="Тренеры" class="chosen-select-trainers" tabindex="-1" multiple="">
                            <?foreach ($trainers as $trainer):?>
                                <option value="<?=$trainer->getID()?>" <?=array_key_exists($trainer->getID(), $service->getTrainers()) ? 'selected' : ''?>>
                                    <?=$trainer->getName()?>
                                </option>
                            <?endforeach;?>
                        </select>
                        <script>
                            $('.chosen-select-trainers').chosen();
                        </script>
                    </div>
                </div>
            </div>
            <input type="submit" name="service_update" class="uk-button uk-button-success" value="Изменить">
        </form>
    </div>
    <div class="uk-width-1-5">
        <div class="uk-panel-box">
            <ul class="uk-nav uk-nav-side">
                <li class="uk-active"><a href="/admin/service/">Управление услугами</a></li>
                <li><a href="/admin/gym/">Управление залами</a></li>
                <li><a href="/admin/trainer/">Управление тренерами</a></li>
                <li><a href="/admin/user/">Управление пользователями</a></li>
                <li><a href="/admin/timing/">Управление расписанием</a></li>
            </ul>
        </div>
    </div>
</div>
<?require_once ROOT . '/templates/layouts/footer_admin.php'?>