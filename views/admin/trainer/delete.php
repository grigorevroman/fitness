<?
/** @var $errors[] */
/** @var entities\Trainer $trainer */
require_once ROOT . '/templates/layouts/header_admin.php'?>
<div class="uk-grid uk-grid-small uk-margin-top uk-margin-large-bottom">
    <div class="uk-width-4-5">
        <h1>Удалить тренера</h1>
        <ul class="uk-breadcrumb">
            <li><a href="/">Дионика</a></li>
            <li><a href="/admin/"><span>Администратор</span></a></li>
            <li><a href="/admin/trainer/">Управление тренерами</a></li>
            <li class="uk-active"><span>Удалить тренера</span></li>
        </ul>
        <?if($trainer):?>
            <div class="uk-alert uk-alert-danger"><p>Вы хотите удалить тренера "<?=$trainer->getName()?>"?</p></div>
            <form method="post">
                <input class="uk-button uk-button-danger" type="submit" name="trainer_delete" value="Удалить">
            </form>
        <?endif;?>
    </div>
    <div class="uk-width-1-5">
        <div class="uk-panel-box">
            <ul class="uk-nav uk-nav-side">
                <li><a href="/admin/service/">Управление услугами</a></li>
                <li><a href="/admin/gym/">Управление залами</a></li>
                <li class="uk-active"><a href="/admin/trainer/">Управление тренерами</a></li>
                <li><a href="/admin/user/">Управление пользователями</a></li>
                <li><a href="/admin/timing/">Управление расписанием</a></li>
            </ul>
        </div>
    </div>
</div>
<?require_once ROOT . '/templates/layouts/footer_admin.php'?>