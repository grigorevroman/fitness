<?
/** @var $errors[] */
/** @var entities\Timing $timing */
/** @var $datesSelect[] */
/** @var entities\Service[] $servicesSelect */
/** @var entities\Gym[] $gymsSelect */
/** @var entities\Trainer[] $trainersSelect */
require_once ROOT . '/templates/layouts/header_admin.php' ?>
<div class="uk-grid uk-grid-small uk-margin-top uk-margin-large-bottom">
    <div class="uk-width-4-5">
        <h1>Добавить в расписание</h1>
        <ul class="uk-breadcrumb">
            <li><a href="/">Дионика</a></li>
            <li><a href="/admin/"><span>Администратор</span></a></li>
            <li><a href="/admin/timing/">Управление расписанием</a></li>
            <li class="uk-active"><span>Добавить в расписание</span></li>
        </ul>
        <?foreach($errors as $error):?>
            <div class="uk-alert uk-alert-danger" data-uk-alert="">
                <a href="" class="uk-alert-close uk-close"></a>
                <p><?=$error?></p>
            </div>
        <?endforeach;?>
        <form method="post" enctype="multipart/form-data">
            <div class="uk-form uk-form-stacked uk-margin-bottom">
                <div class="uk-form-row">
                    <label class="uk-form-label" for="name">Название</label>
                    <div class="uk-form-controls">
                        <input type="text" id="name" placeholder="Название" name="name" value="<?=$timing->getName()?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="active">Активность</label>
                    <div class="uk-form-controls">
                        <input type="checkbox" id="active" name="active" <?=$timing->getActive() ? 'checked' : ''?>>
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="sortable">Сортировка</label>
                    <div class="uk-form-controls">
                        <input type="text" id="sortable" placeholder="Сортировка" name="sortable" value="<?=$timing->getSortable()?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="dates">Даты</label>
                    <div class="uk-form-controls">
                        <?if (count(json_decode($timing->getDates(), true))) {
                            $d = json_decode($timing->getDates(), true);
                        } else {
                            $d = [];
                        }?>
                        <select id="dates" name="dates[]" data-placeholder="Даты" class="chosen-select-dates" tabindex="-1" multiple="">
                            <?foreach ($datesSelect as $dateSelect):?>
                                <option value="<?=$dateSelect?>" <?=(in_array($dateSelect, $d) ? 'selected' : '')?>><?=$dateSelect?></option>
                            <?endforeach;?>
                        </select>
                        <script>
                            $('.chosen-select-dates').chosen();
                        </script>
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="serviceID">Услуга</label>
                    <div class="uk-form-controls">
                        <select id="serviceID" name="serviceID" data-placeholder="Услуга" class="chosen-select-service-id" tabindex="-1">
                            <option value="">Нет</option>
                            <?foreach ($servicesSelect as $serviceSelect):?>
                                <option value="<?=$serviceSelect->getID()?>" <?=($serviceSelect->getID() === $timing->getServiceID() ? 'selected' : '')?>><?=$serviceSelect->getName()?></option>
                            <?endforeach;?>
                        </select>
                        <script>
                            $('.chosen-select-service-id').chosen();
                        </script>
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="startTime">Время начала тренеровки</label>
                    <div class="uk-form-controls">
                        <input type="text" id="startTime" placeholder="Время начала тренеровки" name="startTime" value="<?=$timing->getStartTime()?>">
                        <script>
                            jQuery(function($){
                                $("#startTime").mask("99:99");
                            });
                        </script>
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="endTime">Время конца тренеровки</label>
                    <div class="uk-form-controls">
                        <input type="text" id="endTime" placeholder="Время конца тренеровки" name="endTime" value="<?=$timing->getEndTime()?>">
                        <script>
                            jQuery(function($){
                                $("#endTime").mask("99:99");
                            });
                        </script>
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="gymID">Зал</label>
                    <div class="uk-form-controls">
                        <select id="gymID" name="gymID" data-placeholder="Зал" class="chosen-select-gym-id" tabindex="-1">
                            <option value="">Нет</option>
                            <?foreach ($gymsSelect as $gymSelect):?>
                                <option value="<?=$gymSelect->getID()?>" <?=($gymSelect->getID() === $timing->getGymID() ? 'selected' : '')?>><?=$gymSelect->getName()?></option>
                            <?endforeach;?>
                        </select>
                        <script>
                            $('.chosen-select-gym-id').chosen();
                        </script>
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="trainerID">Тренер</label>
                    <div class="uk-form-controls">
                        <select id="trainerID" name="trainerID" data-placeholder="Тренер" class="chosen-select-trainer-id" tabindex="-1">
                            <option value="">Нет</option>
                            <?foreach ($trainersSelect as $trainerSelect):?>
                                <option value="<?=$trainerSelect->getID()?>" <?=($trainerSelect->getID() === $timing->getTrainerID() ? 'selected' : '')?>><?=$trainerSelect->getName()?></option>
                            <?endforeach;?>
                        </select>
                        <script>
                            $('.chosen-select-trainer-id').chosen();
                        </script>
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="numbers">Количество мест</label>
                    <div class="uk-form-controls">
                        <input type="text" id="numbers" placeholder="Количество мест" name="numbers" value="<?=$timing->getNumbers()?>">
                    </div>
                </div>
            </div>
            <input type="submit" name="timing_create" class="uk-button uk-button-success" value="Добавить">
        </form>
    </div>
    <div class="uk-width-1-5">
        <div class="uk-panel-box">
            <ul class="uk-nav uk-nav-side">
                <li><a href="/admin/service/">Управление услугами</a></li>
                <li><a href="/admin/gym/">Управление залами</a></li>
                <li><a href="/admin/trainer/">Управление тренерами</a></li>
                <li><a href="/admin/user/">Управление пользователями</a></li>
                <li class="uk-active"><a href="/admin/timing/">Управление расписанием</a></li>
            </ul>
        </div>
    </div>
</div>
<? require_once ROOT . '/templates/layouts/footer_admin.php' ?>