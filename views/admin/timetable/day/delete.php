<?
/** @var $errors[] */
/** @var entities\Day $day */
require_once ROOT . '/templates/layouts/header_admin.php'?>
    <div class="uk-grid uk-grid-small uk-margin-top uk-margin-large-bottom">
        <div class="uk-width-4-5">
            <h1>Удалить зал</h1>
            <ul class="uk-breadcrumb">
                <li><a href="/">Дионика</a></li>
                <li><a href="/admin/"><span>Администратор</span></a></li>
                <li><a href="/admin/timetable/">Управление расписанием</a></li>
                <li><a href="/admin/timetable/day/">Управление днями</a></li>
                <li class="uk-active"><span>Удалить день</span></li>
            </ul>
            <?foreach($errors as $error):?>
                <div class="uk-alert uk-alert-danger" data-uk-alert="">
                    <a href="" class="uk-alert-close uk-close"></a>
                    <p><?=$error?></p>
                </div>
            <?endforeach;?>
            <?if($day):?>
                <div class="uk-alert uk-alert-danger"><p>Вы хотите удалить день "<?=$day->getName()?>"?</p></div>
                <form method="post">
                    <input class="uk-button uk-button-danger" type="submit" name="day_delete" value="Удалить">
                </form>
            <?endif;?>
        </div>
        <div class="uk-width-1-5">
            <div class="uk-panel-box">
                <ul class="uk-nav uk-nav-side">
                    <li><a href="/admin/service/">Управление услугами</a></li>
                    <li><a href="/admin/gym/">Управление залами</a></li>
                    <li><a href="/admin/trainer/">Управление тренерами</a></li>
                    <li><a href="/admin/user/">Управление пользователями</a></li>
                    <li class="uk-active"><a href="/admin/timetable/">Управление расписанием</a></li>
                </ul>
            </div>
        </div>
    </div>
<?require_once ROOT . '/templates/layouts/footer_admin.php'?>