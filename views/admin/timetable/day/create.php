<?
/** @var $errors[] */
/** @var entities\Day $day */
require_once ROOT . '/templates/layouts/header_admin.php'?>
    <div class="uk-grid uk-grid-small uk-margin-top uk-margin-large-bottom">
        <div class="uk-width-4-5">
            <h1>Добавить день</h1>
            <ul class="uk-breadcrumb">
                <li><a href="/">Дионика</a></li>
                <li><a href="/admin/"><span>Администратор</span></a></li>
                <li><a href="/admin/timetable/day/">Управление днями</a></li>
                <li class="uk-active"><span>Добавить день</span></li>
            </ul>
            <?foreach($errors as $error):?>
                <div class="uk-alert uk-alert-danger" data-uk-alert="">
                    <a href="" class="uk-alert-close uk-close"></a>
                    <p><?=$error?></p>
                </div>
            <?endforeach;?>
            <form method="post" enctype="multipart/form-data">
                <div class="uk-form uk-form-stacked uk-margin-bottom">
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="name">Дата</label>
                        <div class="uk-form-controls">
                            <input data-uk-datepicker="{format:'YYYY-MM-DD'}" type="text" id="name" placeholder="Дата" name="name" value="<?=$day->getName()?>">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="active">Активность</label>
                        <div class="uk-form-controls">
                            <input type="checkbox" id="active" name="active" <?=$day->getActive() ? 'checked' : ''?>>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="sortable">Сортировка</label>
                        <div class="uk-form-controls">
                            <input type="text" id="sortable" placeholder="Сортировка" name="sortable" value="<?=$day->getSortable()?>">
                        </div>
                    </div>
                </div>
                <input type="submit" name="day_create" class="uk-button uk-button-success" value="Добавить">
            </form>
        </div>
        <div class="uk-width-1-5">
            <div class="uk-panel-box">
                <ul class="uk-nav uk-nav-side">
                    <li><a href="/admin/service/">Управление услугами</a></li>
                    <li><a href="/admin/gym/">Управление залами</a></li>
                    <li><a href="/admin/trainer/">Управление тренерами</a></li>
                    <li><a href="/admin/user/">Управление пользователями</a></li>
                    <li class="uk-active"><a href="/admin/timetable/">Управление расписанием</a></li>
                </ul>
            </div>
        </div>
    </div>
<?require_once ROOT . '/templates/layouts/footer_admin.php'?>