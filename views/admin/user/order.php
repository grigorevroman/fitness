<?php
/** @var entities\Order $orders */
/** @var entities\Service[] $services */
/** @var entities\Gym[] $gyms */
/** @var entities\Trainer[] $trainers */
/** @var entities\User $user */
/** @var $errors[] */
/** @var $genders */
/** @var $isRegistration boolean */
require_once ROOT . '/templates/layouts/header_admin.php'?>
<div class="uk-grid uk-grid-small uk-margin-top uk-margin-large-bottom">
    <div class="uk-width-4-5">
        <h1>Заказы пользователя</h1>
        <ul class="uk-breadcrumb">
            <li><a href="/">Дионика</a></li>
            <li><a href="/admin/"><span>Администратор</span></a></li>
            <li><a href="/admin/user/"><span>Управление пользователями</span></a></li>
            <li class="uk-active"><span>Заказы</span></li>
        </ul>
        <?
        /** @var entities\Order $order */
        foreach($orders as $order):?>
            <?$service = \models\Service::getFullServiceByID($order->getServiceID(), $em)?>
            <?$gym = $em->find('src\Gym', $order->getGymID());?>
            <?$trainer = $em->find('src\Trainer', $order->getTrainerID())?>
            <div class="uk-panel-box uk-margin-bottom">
                <h2>Номер заказа: <?=$order->getID()?></h2>
                <hr>
                <p>Название услуги: <?=$service->getName() ? $service->getName() : ''?></p>
                <p>Краткое описание услуги: <?=$service->getIntro() ? $service->getIntro() : ''?></p>
                <?if($service->getPreviewName()):?>
                    <p><img width="400" src="/upload/<?=$service->getPreviewName()?>"></p>
                <?endif;?>
                <hr>
                <p>Цена: <?=$order->getPrice()?></p>
                <p>Продолжительность:
                    <?if($order->getDuration() == 'day'):?>
                        День
                    <?elseif ($order->getDuration() == 'month'):?>
                        Месяц
                    <?elseif ($order->getDuration() == 'half_year'):?>
                        Пол-года
                    <?elseif ($order->getDuration() == 'year'):?>
                        Год
                    <?endif;?>
                </p>
                <p>Дата начала тренеровки: <?=$order->getDateBegin() ? $order->getDateBegin() : ''?></p>
                <p>Дата завершения действия абонемента: <?=$order->getDateEnd() ? $order->getDateEnd() : ''?></p>
                <?if($order->getPay()):?>
                    <div class="uk-alert uk-alert-success" data-uk-alert="">
                        <a href="" class="uk-alert-close uk-close"></a>
                        <p>Оплачено</p>
                    </div>
                <?else:?>
                    <form action="" method="post">
                        <label for="pay">Клиент оплатил:</label>
                        <input type="checkbox" name="pay" id="pay" <?=$order->getPay() ? 'checked' : ''?>>
                        <input type="hidden" value="<?=$order->getID()?>" name="order_id">
                        <div class="uk-margin-small-top">
                            <input type="submit" value="Изменить статус оплаты" class="uk-button uk-button-primary" name="pay_submit">
                        </div>
                    </form>
                <?endif;?>
            </div>
        <?endforeach;?>
    </div>
    <div class="uk-width-1-5">
        <div class="uk-panel-box">
            <ul class="uk-nav uk-nav-side">
                <li><a href="/admin/service/">Управление услугами</a></li>
                <li><a href="/admin/gym/">Управление залами</a></li>
                <li><a href="/admin/trainer/">Управление тренерами</a></li>
                <li class="uk-active"><a href="/admin/user/">Управление пользователями</a></li>
                <li><a href="/admin/timing/">Управление расписанием</a></li>
            </ul>
        </div>
    </div>
</div>
<?require_once ROOT . '/templates/layouts/footer_admin.php'?>
