<?
/** @var entities\Gym[] $gyms */
require_once ROOT . '/templates/layouts/header_admin.php'?>
<div class="uk-grid uk-grid-small uk-margin-top uk-margin-large-bottom">
    <div class="uk-width-4-5">
        <h1>Управление залами</h1>
        <ul class="uk-breadcrumb">
            <li><a href="/">Дионика</a></li>
            <li><a href="/admin/"><span>Администратор</span></a></li>
            <li class="uk-active"><span>Управление залами</span></li>
        </ul>
        <div>
            <?foreach($gyms as $gym):?>
                <div class="uk-margin-small-bottom uk-h3">
                    <?=$gym->getName()?>
                    <a class="uk-button uk-button-primary <?=$gym->getActive() ? '' : 'button-inactive'?>" href="/admin/gym/update/<?=$gym->getID()?>/">Изменить</a>
                    <a class="uk-button uk-button-danger <?=$gym->getActive() ? '' : 'button-inactive'?>" href="/admin/gym/delete/<?=$gym->getID()?>/">Удалить</a>
                </div>
            <?endforeach;?>
            <a class="uk-button uk-button-success uk-margin-small-top" href="/admin/gym/create/">Добавить</a>
        </div>
    </div>
    <div class="uk-width-1-5">
        <div class="uk-panel-box">
            <ul class="uk-nav uk-nav-side">
                <li><a href="/admin/service/">Управление услугами</a></li>
                <li class="uk-active"><a href="/admin/gym/">Управление залами</a></li>
                <li><a href="/admin/trainer/">Управление тренерами</a></li>
                <li><a href="/admin/user/">Управление пользователями</a></li>
                <li><a href="/admin/timing/">Управление расписанием</a></li>
            </ul>
        </div>
    </div>
</div>
<?require_once ROOT . '/templates/layouts/footer_admin.php'?>