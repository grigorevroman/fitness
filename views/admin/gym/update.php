<?
/** @var $errors[] */
/** @var entities\FullGym $gym */
/** @var entities\Service[] $services */
require_once ROOT . '/templates/layouts/header_admin.php'?>
<div class="uk-grid uk-grid-small uk-margin-top uk-margin-large-bottom">
    <div class="uk-width-4-5">
        <h1>Изменить зал</h1>
        <ul class="uk-breadcrumb">
            <li><a href="/">Дионика</a></li>
            <li><a href="/admin/"><span>Администратор</span></a></li>
            <li><a href="/admin/gym/">Управление залами</a></li>
            <li class="uk-active"><span>Изменить зал</span></li>
        </ul>
        <?foreach($errors as $error):?>
            <div class="uk-alert uk-alert-danger" data-uk-alert="">
                <a href="" class="uk-alert-close uk-close"></a>
                <p><?=$error?></p>
            </div>
        <?endforeach;?>
        <form method="post" enctype="multipart/form-data">
            <div class="uk-form uk-form-stacked uk-margin-bottom">
                <div class="uk-form-row">
                    <label class="uk-form-label" for="name">Название</label>
                    <div class="uk-form-controls">
                        <input type="text" id="name" placeholder="Название" name="name" value="<?=$gym->getName()?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="active">Активность</label>
                    <div class="uk-form-controls">
                        <input type="checkbox" id="active" name="active" <?=$gym->getActive() ? 'checked' : ''?>>
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="sortable">Сортировка</label>
                    <div class="uk-form-controls">
                        <input type="text" id="sortable" placeholder="Сортировка" name="sortable" value="<?=$gym->getSortable()?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="intro">Вступление</label>
                    <div class="uk-form-controls">
                        <input type="text" id="intro" placeholder="Вступление" name="intro" value="<?=$gym->getIntro()?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="content">Контент</label>
                    <div class="uk-form-controls">
                        <textarea data-uk-htmleditor="{maxsplitsize:600}" data-uk-check-display="1" id="content" name="content"><?=$gym->getContent()?></textarea>
                    </div>
                </div>
                <?if($gym->getPreviewName()):?>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="preview">Превью</label>
                        <div class="uk-form-controls">
                            <div class="uk-margin-small-bottom"><img width="250" src="/upload/<?=$gym->getPreviewName()?>" alt=""></div>
                        </div>
                    </div>
                <?endif;?>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="services">Услуги</label>
                    <div class="uk-form-controls">
                        <select id="services" name="services[]" data-placeholder="Услуги" class="chosen-select-services" tabindex="-1" multiple="">
                            <?foreach ($services as $service):?>
                                <option value="<?=$service->getID()?>" <?=array_key_exists($service->getID(), $gym->getServices()) ? 'selected' : ''?>>
                                    <?=$service->getName()?>
                                </option>
                            <?endforeach;?>
                        </select>
                        <script>
                            $('.chosen-select-services').chosen();
                        </script>
                    </div>
                </div>
            </div>
            <input type="submit" name="gym_update" class="uk-button uk-button-success" value="Изменить">
        </form>
    </div>
    <div class="uk-width-1-5">
        <div class="uk-panel-box">
            <ul class="uk-nav uk-nav-side">
                <li><a href="/admin/service/">Управление услугами</a></li>
                <li class="uk-active"><a href="/admin/gym/">Управление залами</a></li>
                <li><a href="/admin/trainer/">Управление тренерами</a></li>
                <li><a href="/admin/user/">Управление пользователями</a></li>
                <li><a href="/admin/timing/">Управление расписанием</a></li>
            </ul>
        </div>
    </div>
</div>
<?require_once ROOT . '/templates/layouts/footer_admin.php'?>