<?
/** @var entities\Service[] $services */
/** @var entities\Gym[] $gyms */
/** @var entities\Trainer[] $trainers */
/** @var entities\FullService $detailService */
/** @var entities\Order[] $orders */
/** @var entities\Record[] $records */
/** @var $orderAdd boolean */
require_once ROOT . '/templates/layouts/header_admin.php'?>
    <div class="uk-grid uk-grid-small uk-margin-top uk-margin-large-bottom">
        <div class="uk-width-4-5">
            <h1>Отчет</h1>
            <ul class="uk-breadcrumb">
                <li><a href="/">Дионика</a></li>
                <li class="uk-active"><span>Отчет</span></li>
            </ul>
            <div class="uk-row-first">
                <ul class="uk-tab" data-uk-tab="{connect:'#tab-content'}">
                    <li class="uk-active" aria-expanded="true"><a href="#">Оформления услуг</a></li>
                    <li aria-expanded="false" class=""><a href="#">Записи в расписании</a></li>
                    <li class="uk-tab-responsive uk-active uk-hidden" aria-haspopup="true" aria-expanded="false"><a>Tab</a><div class="uk-dropdown uk-dropdown-small" aria-hidden="true"><ul class="uk-nav uk-nav-dropdown"></ul><div></div></div></li></ul>
                <ul id="tab-content" class="uk-switcher uk-margin">
                    <li class="uk-active" aria-hidden="false">
                        <?foreach($orders as $order):?>
                            <?
                            /** @var entities\User $user */
                            $user = $em->find('src\User', $id);
                            /** @var entities\Service $service */
                            $service = $em->find('src\Service', $order->getServiceID());
                            ?>
                            <pre>
                                <table class="uk-table">
                                    <tr>
                                        <td>Заказ</td>
                                        <td>Пользователь</td>
                                        <td>Услуга</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>Номер: <?=$order->getID()?></p>
                                            <p>Цена: <?=$order->getPrice()?></p>
                                            <p>Дата начала действия: <?=$order->getDateBegin()?></p>
                                            <p>Дата завершения действия: <?=$order->getDateEnd()?></p>
                                            <p>Продолжительность:
                                                <?if($order->getDuration() === 'day'):?>День<?endif;?>
                                                <?if($order->getDuration() === 'month'):?>Месяц<?endif;?>
                                                <?if($order->getDuration() === 'half_year'):?>Пол-года<?endif;?>
                                                <?if($order->getDuration() === 'year'):?>Год<?endif;?>
                                            </p>
                                            <p>Оплачено:
                                                <?if($order->getPay()):?>Да<?else:?>Нет<?endif;?>
                                            </p>
                                        </td>
                                        <td>
                                            <?if($user):?>
                                                <p>Номер: <?=$user->getID()?></p>
                                                <p>Имя: <?=$user->getName()?></p>
                                                <p>E-mail: <?=$user->getEmail()?></p>
                                                <p>Телефон: <?=$user->getPhone()?></p>
                                                <p>Пол: <?=$user->getGender()?></p>
                                                <p>Дата рождения: <?=$user->getDateOfBirth()->format("d.m.Y")?></p>
                                                <p>Новичек:
                                                    <?if($user->getNew()):?>Да<?else:?>Нет<?endif;?>
                                                </p>
                                            <?endif;?>
                                        </td>
                                        <td>
                                            <?if($service):?>
                                                <p>Номер: <?=$service->getID()?></p>
                                                <p>Название: <?=$service->getName()?></p>
                                            <?endif;?>
                                        </td>
                                    </tr>
                                </table>
                            </pre>
                        <?endforeach;?>
                    </li>
                    <li aria-hidden="true" class="">
                        <?if(count($records)):?>
                            <?foreach($records as $record):?>
                                <?
                                $service = $em->find('src\Service', $record->getServiceID());
                                /** @var entities\Order $order */
                                $order = models\Order::getOrderByID($record->getOrderID());
                                /** @var entities\User $user */
                                $user = $em->find('src\User', $id);
                                ?>
                                <pre>
                                    <table class="uk-table">
                                        <tr>
                                            <td>Запись</td>
                                            <td>Услуга</td>
                                            <td>Заказ</td>
                                            <td>Пользователь</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p>Номер: <?=$record->getID()?></p>
                                                <p>Дата: <?=$record->getDate()?></p>
                                            </td>
                                            <td>
                                                <?if($service):?>
                                                    <p>Номер: <?=$service->getID()?></p>
                                                    <p>Название: <?=$service->getName()?></p>
                                                <?endif;?>
                                            </td>
                                            <td>
                                                <?if($order):?>
                                                    <p>Номер: <?=$order['id']?></p>
                                                    <p>Цена: <?=$order['price']?></p>
                                                    <p>Дата начала действия: <?=$order['dateBegin']?></p>
                                                    <p>Дата завершения действия: <?=$order['dateEnd']?></p>
                                                    <p>Продолжительность:
                                                        <?if($order['duration'] === 'day'):?>День<?endif;?>
                                                        <?if($order['duration'] === 'month'):?>Месяц<?endif;?>
                                                        <?if($order['duration'] === 'half_year'):?>Пол-года<?endif;?>
                                                        <?if($order['duration'] === 'year'):?>Год<?endif;?>
                                                    </p>
                                                    <p>Оплачено:
                                                        <?if($order['pay']):?>Да<?else:?>Нет<?endif;?>
                                                    </p>
                                                <?endif;?>
                                            </td>
                                            <td>
                                                <?if($user):?>
                                                    <p>Номер: <?=$user->getID()?></p>
                                                    <p>Имя: <?=$user->getName()?></p>
                                                    <p>E-mail: <?=$user->getEmail()?></p>
                                                    <p>Телефон: <?=$user->getPhone()?></p>
                                                    <p>Пол: <?=$user->getGender()?></p>
                                                    <p>Дата рождения: <?=$user->getDateOfBirth()?></p>
                                                    <p>Новичек:
                                                        <?if($user->getNew()):?>Да<?else:?>Нет<?endif;?>
                                                    </p>
                                                <?endif;?>
                                            </td>
                                        </tr>
                                    </table>
                                </pre>
                            <?endforeach;?>
                        <?endif;?>
                    </li>
                </ul>
            </div>
        </div>
        <div class="uk-width-1-5">
            <div class="uk-panel-box">
                <ul class="uk-nav uk-nav-parent-icon uk-nav-side" data-uk-nav="">
                    <li class="uk-parent">
                        <a href="#">Услуги</a>
                        <ul class="uk-nav-sub">
                            <?foreach($services as $service):?>
                                <?if($service->getActive()):?>
                                    <li><a href="/service/<?=$service->getID()?>/"><?=$service->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                    <li class="uk-parent">
                        <a href="#">Залы</a>
                        <ul class="uk-nav-sub">
                            <?foreach($gyms as $gym):?>
                                <?if($gym->getActive()):?>
                                    <li><a href="/gym/<?=$gym->getID()?>/"><?=$gym->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                    <li class="uk-parent">
                        <a href="#">Тренеры</a>
                        <ul class="uk-nav-sub">
                            <?foreach($trainers as $trainer):?>
                                <?if($trainer->getActive()):?>
                                    <li><a href="/trainer/<?=$trainer->getID()?>/"><?=$trainer->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
<?require_once ROOT . '/templates/layouts/footer_admin.php'?>