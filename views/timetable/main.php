<?
/** @var $table[] */
/** @var \entities\Order[] $currentUserOrders */

use models\Service;
use models\Gym;
use models\Trainer;
use models\Order;
use models\Record;

$currentUserOrdersServicesIDs = [];
if (count($currentUserOrders)) {
    foreach ($currentUserOrders as $currentUserOrder) {
        $currentUserOrdersServicesIDs[] = $currentUserOrder->getServiceID();
    }
}

$dbh = components\DB::getConnection();

require_once ROOT . '/templates/layouts/header_admin.php'?>
    <div class="uk-margin-top uk-margin-large-bottom">
        <h1>Расписание</h1>
        <ul class="uk-breadcrumb">
            <li><a href="/">Дионика</a></li>
            <li class="uk-active"><span>Расписание</span></li>
        </ul>
        <table class="uk-table timetable">
            <tr>
                <?foreach($table as $date => $timings):
                    $date = (new DateTime($date))->format("d.m.Y");
                    ?>
                    <td>
                        <div class="uk-panel uk-panel-box" style="text-align: center; background-color: #fff7f7;"><?=$date?></div>
                    </td>
                <?endforeach;?>
            </tr>
            <tr>
                <?foreach($table as $date => $timings):?>
                    <td style="padding-bottom: 0;">
                        <?
                        /** @var entities\Timing $timing */
                        foreach($timings as $timing):?>
                            <?if($timing->getActive()):
                                $service = null;
                                $o = [];
                                if ($timing->getServiceID()) {
                                    $service = $em->find('src\Service', $timing->getServiceID());
                                    $o = Order::getOrdersByServiceIDAndCurrentUser($service->getID(), $date, $em);
                                }
                                $gym = null;
                                if ($timing->getGymID()) {
                                    $gym = $em->find('src\Gym', $timing->getGymID());
                                }
                                $trainer = null;
                                if ($timing->getTrainerID()) {
                                    $trainer = $em->find('src\Trainer', $timing->getTrainerID());
                                }
                                $n = Record::n($date, $timing->getID());
                                ?>
                                <div class="uk-panel uk-panel-box" style="padding: 5px 5px 5px 5px; margin-bottom: 8px; font-size: 12px; background-color: #f9fff7;">
                                    <div style="text-align: center; padding: 5px 0 5px 0;"><b><?=$timing->getName()?></b></div>
                                    <hr style="margin: 5px 0;">
                                    <div style="padding-bottom: 5px; padding-top: 5px;"><b>Услуга:</b> <?=($service ? $service->getName() : '')?></div>
                                    <div style="padding-bottom: 5px; padding-top: 5px;"><b>Зал:</b> <?=($gym ? $gym->getName() : '')?></div>
                                    <div style="padding-bottom: 5px; padding-top: 5px;"><b>Тренер:</b> <?=($trainer ? $trainer->getName() : '')?></div>
                                    <hr style="margin: 5px 0;">
                                    <div style="padding-bottom: 5px; padding-top: 5px;"><b>Время:</b> c <?=$timing->getStartTime()?> до <?=$timing->getEndTime()?></div>
                                    <div style="padding-bottom: 5px; padding-top: 5px;"><b>Количество мест:</b> <?=$timing->getNumbers() - $n;?></div>
                                    <hr style="margin: 5px 0;">

                                    <?
                                    /** @var models\User $user */
                                    $user = models\User::getUserBySession($em);
                                    if ($user->getRole() == 'Администратор' || $user->getRole() == 'Менеджер' || $user->getRole() == 'Тренер') {

                                        echo '<div style="padding-bottom: 5px; padding-top: 5px;"><b>Записанные клиенты:</b></div>';
                                        $sql = "select user.* from user
                                            join record on record.userID = user.id
                                            join timing on record.timingID = timing.id
                                            where record.date = ?
                                            and record.serviceID = ?
                                            and record.timingID = ?";
                                        $stmt = $dbh->prepare($sql);
                                        $stmt->execute([$date, $service->getID(), $timing->getID()]);
                                        $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);

                                        /** @var entities\User[] $users */
                                        $users = models\User::getUsersByRows($rows);
                                        foreach ($users as $u) {
                                            echo $u->getName() . ' ' . ($u->getName() ? '(Новичок)' : '') . '<br>';
                                        }

                                        echo '<hr style="margin: 5px 0;">';
                                    }
                                    ?>

                                    <?if(in_array($service->getID(), $currentUserOrdersServicesIDs)):?>
                                        <?if(count($o)):?>
                                            <?foreach($o as $k => $v):?>
                                                <?$record = \models\Record::getRecordByData($service->getID(), $timing->getID(), $date, models\User::getUserBySession($em)->getID(), $v->getID());?>
                                                <?if($record):?>
                                                    <div class="uk-margin-small-bottom">Вы записаны:</div>
                                                    <form action="" method="post">
                                                        <input type="hidden" name="recordID" value="<?=$record->getID()?>">
                                                        <input type="submit" name="record_submit_1" class="uk-button uk-button-danger uk-margin-small-bottom" value="Отписка (№<?=$v->getID()?>)">
                                                    </form>
                                                <?else:?>
                                                    <?if($timing->getNumbers() - $n):?>
                                                        <div class="uk-margin-small-bottom">Услуга оформлена, можно записаться:</div>
                                                    <?endif;?>
                                                    <?if($timing->getNumbers() - $n):?>
                                                        <form action="" method="post">
                                                            <input type="hidden" name="serviceID" value="<?=$service->getID()?>">
                                                            <input type="hidden" name="timingID" value="<?=$timing->getID()?>">
                                                            <input type="hidden" name="date" value="<?=$date?>">
                                                            <input type="hidden" name="userID" value="<?=models\User::getUserBySession($em)->getID()?>">
                                                            <input type="hidden" name="orderID" value="<?=$v->getID()?>">
                                                            <input type="submit" name="record_submit" class="uk-button uk-button-success uk-margin-small-bottom" value="Запись (№<?=$v->getID()?>)">
                                                        </form>
                                                    <?endif;?>
                                                <?endif;?>
                                            <?endforeach;?>
                                        <?else:?>
                                            <div class="uk-margin-small-bottom">Услуга оформлена, но на другую дату</div>
                                        <?endif;?>
                                        <?if($timing->getNumbers() - $n):?>
                                            <div class="uk-margin-small-bottom">Услуга уже оформлена, но можно оформить еще:</div>
                                        <?else:?>
                                            <div class="uk-margin-small-bottom">Услуга уже оформлена, но места закончились:</div>
                                        <?endif;?>
                                    <?else:?>
                                        <div class="uk-margin-small-bottom">Для записи необходимо оформить услугу:</div>
                                    <?endif;?>
                                    <a class="uk-button uk-button-primary uk-margin-small-bottom" href="/service/<?=$service->getID()?>/">Оформить услугу</a>
                                </div>
                            <?endif;?>
                        <?endforeach;?>
                    </td>
                <?endforeach;?>
            </tr>
        </table>
    </div>
<?require_once ROOT . '/templates/layouts/footer_admin.php'?>