<?
/** @var entities\Service[] $services */
/** @var entities\Gym[] $gyms */
/** @var entities\Trainer[] $trainers */
/** @var entities\FullService[] $fullServices */
/** @var array $servicesInCart */
/** @var $totalPrice int */
require_once ROOT . '/templates/layouts/header_admin.php'?>
    <div class="uk-grid uk-grid-small uk-margin-top uk-margin-large-bottom">
        <div class="uk-width-4-5">
            <h1>Корзина</h1>
            <ul class="uk-breadcrumb">
                <li><a href="/">Дионика</a></li>
                <li class="uk-active"><span>Корзина</span></li>
            </ul>
            <div>
                <form method="post" enctype="multipart/form-data">
                    <?
                    $j = 1;
                    foreach($fullServices as $service):?>
                        <div class="uk-panel-box uk-margin-bottom">
                            <h3><?=$service->getName()?></h3>
                            <p><?=$service->getType()?></p>
                            <p><?=$service->getIntro()?></p>
                            <hr>

                            <div class="uk-margin-bottom">
                                <label for="chosen-select-price-<?=$j?>">Продолжительность и цена:</label>
                                <select id="chosen-select-price-<?=$j?>" class="chosen-select-price-<?=$j?>" name="chosen-select-price-<?=$j?>" tabindex="-1">
                                    <option value="<?=$service->getPriceDay()?>">Одна тренеровка: <?=$service->getPriceDay()?>р.</option>
                                    <option value="<?=$service->getPriceMonth()?>">Абонемент на месяц: <?=$service->getPriceMonth()?>р.</option>
                                    <option value="<?=$service->getPriceHalfYear()?>">Абонемент на пол-года: <?=$service->getPriceHalfYear()?>р.</option>
                                    <option value="<?=$service->getPriceYear()?>">Абонемент на год: <?=$service->getPriceYear()?>р.</option>
                                </select>
                                <script>
                                    $('.chosen-select-price-<?=$j?>').chosen();
                                </script>
                            </div>

                            <?if(count($service->getGyms())):?>
                                <div class="uk-margin-bottom">
                                    <label for="chosen-select-gym-<?=$j?>">Зал:</label>
                                    <select id="chosen-select-gym-<?=$j?>" class="chosen-select-gym-<?=$j?>" name="chosen-select-gym-<?=$j?>" tabindex="-1">
                                        <?
                                        /** @var entities\Gym $gym */
                                        foreach($service->getGyms() as $gym):?>
                                            <option><?=$gym->getName()?></option>
                                        <?endforeach;?>
                                    </select>
                                    <script>
                                        $('.chosen-select-gym-<?=$j?>').chosen();
                                    </script>
                                </div>
                            <?endif;?>

                            <?if(count($service->getTrainers())):?>
                                <div class="uk-margin-bottom">
                                    <label for="chosen-select-trainer-<?=$j?>">Тренер:</label>
                                    <select id="chosen-select-trainer-<?=$j?>" class="chosen-select-trainer-<?=$j?>" name="chosen-select-trainer-<?=$j?>" tabindex="-1">
                                        <?
                                        /** @var entities\Trainer $trainer */
                                        foreach($service->getTrainers() as $trainer):?>
                                            <option><?=$trainer->getName()?></option>
                                        <?endforeach;?>
                                    </select>
                                    <script>
                                        $('.chosen-select-trainer-<?=$j?>').chosen();
                                    </script>
                                </div>
                            <?endif;?>

                        </div>
                    <?
                    $j++;
                    endforeach;?>
                    <input type="submit" name="cart" class="uk-button uk-button-success" value="Оформить">
                </form>
            </div>
            <?if($servicesInCart):?>
                <table class="uk-table">
                    <thead>
                        <tr>
                            <th>Название</th>
                            <th>Стоимость</th>
                            <th>Количество</th>
                            <th>Удалить</th>
                        </tr>
                    </thead>
                    <?foreach($services as $service):?>
                        <tbody>
                            <tr>
                                <td><a href="/service/<?=$service->getID()?>/"><?=$service->getName()?></a></td>
                                <td><?=number_format($service->getPriceDay(), 0, '', ' ')?> <i class="uk-icon-rub"></i></td>
                                <td><?=$servicesInCart[$service->getID()]?></td>
                                <td><a style="color: red;" href="/cart/delete/<?=$service->getID()?>/">Удалить</a></td>
                            </tr>
                        </tbody>
                    <?endforeach;?>
                </table>
                    <p>Общая стоимость: <?=number_format($totalPrice, 0, '', ' ')?> <i class="uk-icon-rub"></i></p>
                <p><a class="btn btn-default checkout uk-button uk-button-success" href="/cart/checkout/"><i class="fa fa-shopping-cart"></i> Оформить заказ</a></p>
            <?php else: ?>
                <p>Корзина пуста</p>
                <a class="btn btn-default checkout uk-button uk-button-primary" href="/"><i class="fa fa-shopping-cart"></i> Вернуться к покупкам</a>
            <?php endif; ?>
        </div>
        <div class="uk-width-1-5">
            <div class="uk-panel-box">
                <ul class="uk-nav uk-nav-parent-icon uk-nav-side" data-uk-nav="">
                    <li class="uk-parent">
                        <a href="#">Услуги</a>
                        <ul class="uk-nav-sub">
                            <?foreach($services as $service):?>
                                <?if($service->getActive()):?>
                                    <li><a href="/service/<?=$service->getID()?>/"><?=$service->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                    <li class="uk-parent">
                        <a href="#">Залы</a>
                        <ul class="uk-nav-sub">
                            <?foreach($gyms as $gym):?>
                                <?if($gym->getActive()):?>
                                    <li><a href="/gym/<?=$gym->getID()?>/"><?=$gym->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                    <li class="uk-parent">
                        <a href="#">Тренеры</a>
                        <ul class="uk-nav-sub">
                            <?foreach($trainers as $trainer):?>
                                <?if($trainer->getActive()):?>
                                    <li><a href="/trainer/<?=$trainer->getID()?>/"><?=$trainer->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
<?require_once ROOT . '/templates/layouts/footer_admin.php'?>