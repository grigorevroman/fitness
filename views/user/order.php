<?
/** @var entities\Service[] $services */
/** @var entities\Gym[] $gyms */
/** @var entities\Trainer[] $trainers */
/** @var entities\Order[] $orders */
/** @var entities\User $user */
/** @var $errors[] */
/** @var $genders */
/** @var $isRegistration boolean */
require_once ROOT . '/templates/layouts/header_admin.php'?>
    <div class="uk-grid uk-grid-small uk-margin-top uk-margin-large-bottom">
        <div class="uk-width-4-5">
            <h1>Заказы пользователя</h1>
            <ul class="uk-breadcrumb">
                <li><a href="/">Дионика</a></li>
                <li><a href="/user/">Пользователь</a></li>
                <li class="uk-active"><span>Заказы пользователя</span></li>
            </ul>
            <?foreach($orders as $order):?>
                <?$service = \models\Service::getFullServiceByID($order->getServiceID(), $em)?>
                <?$gym = $em->find('src\Gym', $order->getGymID());?>
                <?$trainer = $em->find('src\Trainer', $order->getTrainerID())?>
                <div class="uk-panel-box uk-margin-bottom">
                    <h2>Номер заказа: <?=$order->getID()?></h2>
                    <hr>
                    <?if($service):?>
                        <p>Название услуги: <?=$service->getName() ? $service->getName() : ''?></p>
                        <p>Краткое описание услуги: <?=$service->getIntro() ? $service->getIntro() : ''?></p>
                        <?if($service->getPreviewName()):?>
                            <p><img width="400" src="/upload/<?=$service->getPreviewName()?>"></p>
                        <?endif;?>
                        <hr>
                    <?endif;?>
                    <?if($gym):?>
                        <p>Зал: <?=$gym->getName() ? $gym->getName() : ''?></p>
                        <p>Краткое описание зала: <?=$gym->getIntro() ? $gym->getIntro() : ''?></p>
                        <?if($gym->getPreviewName()):?>
                            <p><img width="400" src="/upload/<?=$gym->getPreviewName()?>"></p>
                        <?endif;?>
                        <hr>
                    <?endif;?>
                    <?if($trainer):?>
                        <p>Тренер: <?=$trainer->getName() ? $trainer->getName() : ''?></p>
                        <?if($trainer->getPreviewName()):?>
                            <p><img width="200" src="/upload/<?=$trainer->getPreviewName()?>"></p>
                        <?endif;?>
                        <hr>
                    <?endif;?>
                    <?if($order):?>
                        <p>Цена: <?=$order->getPrice()?></p>
                        <p>Продолжительность:
                            <?if($order->getDuration() == 'day'):?>
                                День
                            <?elseif ($order->getDuration() == 'month'):?>
                                Месяц
                            <?elseif ($order->getDuration() == 'half_year'):?>
                                Пол-года
                            <?elseif ($order->getDuration() == 'year'):?>
                                Год
                            <?endif;?>
                        </p>
                        <p>Дата начала действия абонемента: <?=$order->getDateBegin() ? $order->getDateBegin() : ''?></p>
                        <p>Дата завершения действия абонемента: <?=$order->getDateEnd() ? $order->getDateEnd() : ''?></p>
                        <?if($order->getPay()):?>
                            <div class="uk-alert uk-alert-success" data-uk-alert="">
                                <a href="" class="uk-alert-close uk-close"></a>
                                <p>Оплачено</p>
                            </div>
                        <?else:?>
                            <div class="uk-alert uk-alert-danger" data-uk-alert="">
                                <a href="" class="uk-alert-close uk-close"></a>
                                <p>Ожидает оплаты</p>
                            </div>
                        <?endif;?>
                    <?endif;?>
                </div>
            <?endforeach;?>
        </div>
        <div class="uk-width-1-5">
            <div class="uk-panel-box">
                <ul class="uk-nav uk-nav-parent-icon uk-nav-side" data-uk-nav="">
                    <li class="uk-parent">
                        <a href="#">Услуги</a>
                        <ul class="uk-nav-sub">
                            <?foreach($services as $service):?>
                                <?if($service->getActive()):?>
                                    <li><a href="/service/<?=$service->getID()?>/"><?=$service->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                    <li class="uk-parent">
                        <a href="#">Залы</a>
                        <ul class="uk-nav-sub">
                            <?foreach($gyms as $gym):?>
                                <?if($gym->getActive()):?>
                                    <li><a href="/gym/<?=$gym->getID()?>/"><?=$gym->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                    <li class="uk-parent">
                        <a href="#">Тренеры</a>
                        <ul class="uk-nav-sub">
                            <?foreach($trainers as $trainer):?>
                                <?if($trainer->getActive()):?>
                                    <li><a href="/trainer/<?=$trainer->getID()?>/"><?=$trainer->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
<?require_once ROOT . '/templates/layouts/footer_admin.php'?>