<?
/** @var entities\Service[] $services */
/** @var entities\Gym[] $gyms */
/** @var entities\Trainer[] $trainers */
/** @var entities\User $user */
/** @var $errors[] */
/** @var $genders */
/** @var $isRegistration boolean */
require_once ROOT . '/templates/layouts/header_admin.php'?>
    <div class="uk-grid uk-grid-small uk-margin-top uk-margin-large-bottom">
        <div class="uk-width-4-5">
            <h1>Изменить пользователя</h1>
            <ul class="uk-breadcrumb">
                <li><a href="/">Дионика</a></li>
                <li><a href="/user/">Пользователь</a></li>
                <li class="uk-active"><span>Изменить пользователя</span></li>
            </ul>
            <?foreach($errors as $error):?>
                <div class="uk-alert uk-alert-danger" data-uk-alert="">
                    <a href="" class="uk-alert-close uk-close"></a>
                    <p><?=$error?></p>
                </div>
            <?endforeach;?>
            <form method="post" enctype="multipart/form-data">
                <div class="uk-form uk-form-stacked uk-margin-bottom">
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="name">Имя</label>
                        <div class="uk-form-controls">
                            <input type="text" id="name" placeholder="Имя" name="name" value="<?=$user->getName()?>">
                        </div>
                    </div>
                    <?if($user->getPreviewName()):?>
                        <div class="uk-form-row">
                            <label class="uk-form-label" for="preview">Превью</label>
                            <div class="uk-form-controls">
                                <div class="uk-margin-small-bottom"><img width="250" src="/upload/<?=$user->getPreviewName()?>" alt=""></div>
                            </div>
                        </div>
                    <?endif;?>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="email">E-mail</label>
                        <div class="uk-form-controls">
                            <input type="email" id="email" placeholder="E-mail" name="email" value="<?=$user->getEmail()?>">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="phone">Телефон</label>
                        <div class="uk-form-controls">
                            <input type="text" id="phone" placeholder="Телефон" name="phone" value="<?=$user->getPhone()?>">
                            <script>
                                jQuery(function($){
                                    $("#phone").mask("+7 (999) 999-99-99");
                                });
                            </script>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <span class="uk-form-label">Пол</span>
                        <div class="uk-form-controls">
                            <?foreach($genders as $genderCode => $genderName):?>
                                <input type="radio" id="gender_<?=$genderCode?>" name="gender" value="<?=$genderCode?>" <?=$genderName === $user->getGender() ? 'checked' : ''?>>
                                <label for="gender_<?=$genderCode?>"><?=$genderName?></label><br>
                            <?endforeach;?>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="dateOfBirth">Дата рождения</label>
                        <div class="uk-form-controls">
                            <input type="text" data-uk-datepicker="{format:'YYYY-MM-DD'}" name="dateOfBirth" id="dateOfBirth" value="<?=$user->getDateOfBirth()->format("d.m.Y")?>">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="password">Пароль</label>
                        <div class="uk-form-controls">
                            <div class="uk-form-password">
                                <input type="password" id="password" name="password" placeholder="Пароль" value="<?=$user->getPassword()?>">
                                <a href="" class="uk-form-password-toggle" data-uk-form-password="{lblShow:'Показать', lblHide:'Скрыть'}">Показать</a>
                            </div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <span class="uk-form-label">Новичек</span>
                        <div class="uk-form-controls">
                            <input type="radio" id="new_yes" name="new" value="1" <?=1 == $user->getNew() ? 'checked' : ''?>>
                            <label for="new_yes">Да</label><br>
                        </div>
                        <div class="uk-form-controls">
                            <input type="radio" id="new_no" name="new" value="0" <?=0 == $user->getNew() ? 'checked' : ''?>>
                            <label for="new_no">Нет</label><br>
                        </div>
                    </div>
                </div>
                <input type="submit" name="user_update" class="uk-button uk-button-success" value="Изменить">
            </form>
        </div>
        <div class="uk-width-1-5">
            <div class="uk-panel-box">
                <ul class="uk-nav uk-nav-parent-icon uk-nav-side" data-uk-nav="">
                    <li class="uk-parent">
                        <a href="#">Услуги</a>
                        <ul class="uk-nav-sub">
                            <?foreach($services as $service):?>
                                <?if($service->getActive()):?>
                                    <li><a href="/service/<?=$service->getID()?>/"><?=$service->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                    <li class="uk-parent">
                        <a href="#">Залы</a>
                        <ul class="uk-nav-sub">
                            <?foreach($gyms as $gym):?>
                                <?if($gym->getActive()):?>
                                    <li><a href="/gym/<?=$gym->getID()?>/"><?=$gym->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                    <li class="uk-parent">
                        <a href="#">Тренеры</a>
                        <ul class="uk-nav-sub">
                            <?foreach($trainers as $trainer):?>
                                <?if($trainer->getActive()):?>
                                    <li><a href="/trainer/<?=$trainer->getID()?>/"><?=$trainer->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
<?require_once ROOT . '/templates/layouts/footer_admin.php'?>