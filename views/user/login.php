<?
/** @var entities\Service[] $services */
/** @var entities\Gym[] $gyms */
/** @var entities\Trainer[] $trainers */
/** @var entities\User $user */
/** @var $errors[] */
/** @var $genders */
/** @var $isRegistration boolean */
require_once ROOT . '/templates/layouts/header_admin.php'?>
    <div class="uk-grid uk-grid-small uk-margin-top uk-margin-large-bottom">
        <div class="uk-width-4-5">
            <h1>Войти</h1>
            <ul class="uk-breadcrumb">
                <li><a href="/">Дионика</a></li>
                <li class="uk-active"><span>Войти</span></li>
            </ul>
            <?if($isRegistration):?>
                <div class="uk-alert uk-alert-success" data-uk-alert="">
                    <a href="" class="uk-alert-close uk-close"></a>
                    <p>Вы успешно зарегестрированы</p>
                </div>
            <?else:?>
                <?foreach($errors as $error):?>
                    <div class="uk-alert uk-alert-danger" data-uk-alert="">
                        <a href="" class="uk-alert-close uk-close"></a>
                        <p><?=$error?></p>
                    </div>
                <?endforeach;?>
                <form method="post" enctype="multipart/form-data">
                    <div class="uk-form uk-form-stacked uk-margin-bottom">
                        <div class="uk-form-row">
                            <label class="uk-form-label" for="email">E-mail</label>
                            <div class="uk-form-controls">
                                <input type="email" id="email" placeholder="E-mail" name="email" value="<?=$user->getEmail()?>">
                            </div>
                        </div>
                        <div class="uk-form-row">
                            <label class="uk-form-label" for="password">Пароль</label>
                            <div class="uk-form-controls">
                                <div class="uk-form-password">
                                    <input type="password" id="password" name="password" placeholder="Пароль" value="<?=$user->getPassword()?>">
                                    <a href="" class="uk-form-password-toggle" data-uk-form-password="{lblShow:'Показать', lblHide:'Скрыть'}">Показать</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="submit" name="login" class="uk-button uk-button-success" value="Войти">
                </form>
            <?endif;?>
        </div>
        <div class="uk-width-1-5">
            <div class="uk-panel-box">
                <ul class="uk-nav uk-nav-parent-icon uk-nav-side" data-uk-nav="">
                    <li class="uk-parent">
                        <a href="#">Услуги</a>
                        <ul class="uk-nav-sub">
                            <?foreach($services as $service):?>
                                <?if($service->getActive()):?>
                                    <li><a href="/service/<?=$service->getID()?>/"><?=$service->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                    <li class="uk-parent">
                        <a href="#">Залы</a>
                        <ul class="uk-nav-sub">
                            <?foreach($gyms as $gym):?>
                                <?if($gym->getActive()):?>
                                    <li><a href="/gym/<?=$gym->getID()?>/"><?=$gym->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                    <li class="uk-parent">
                        <a href="#">Тренеры</a>
                        <ul class="uk-nav-sub">
                            <?foreach($trainers as $trainer):?>
                                <?if($trainer->getActive()):?>
                                    <li><a href="/trainer/<?=$trainer->getID()?>/"><?=$trainer->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
<?require_once ROOT . '/templates/layouts/footer_admin.php'?>