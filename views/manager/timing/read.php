<?
/** @var entities\Timing[] $timings */
/** @var entities\Service[] $services */
/** @var entities\Gym[] $gyms */
/** @var entities\Trainer[] $trainers */
require_once ROOT . '/templates/layouts/header_admin.php' ?>
    <div class="uk-grid uk-grid-small uk-margin-top uk-margin-large-bottom">
        <div class="uk-width-4-5">
            <h1>Управление расписанием</h1>
            <ul class="uk-breadcrumb">
                <li><a href="/">Дионика</a></li>
                <li class="uk-active"><span">Управление расписанием</span></li>
            </ul>
            <div>
                <?foreach($timings as $timing):?>
                    <div class="uk-margin-small-bottom uk-h3">
                        <?=$timing->getName()?>
                        <a class="uk-button uk-button-primary <?=$timing->getActive() ? '' : 'button-inactive'?>" href="/manager/timing/update/<?=$timing->getID()?>/">Изменить</a>
                        <a class="uk-button uk-button-danger <?=$timing->getActive() ? '' : 'button-inactive'?>" href="/manager/timing/delete/<?=$timing->getID()?>/">Удалить</a>
                    </div>
                <?endforeach;?>
                <a class="uk-button uk-button-success uk-margin-small-top" href="/manager/timing/create/">Добавить</a>
            </div>
        </div>
        <div class="uk-width-1-5">
            <div class="uk-panel-box">
                <ul class="uk-nav uk-nav-parent-icon uk-nav-side" data-uk-nav="">
                    <li class="uk-parent">
                        <a href="#">Услуги</a>
                        <ul class="uk-nav-sub">
                            <?foreach($services as $service):?>
                                <?if($service->getActive()):?>
                                    <li><a href="/service/<?=$service->getID()?>/"><?=$service->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                    <li class="uk-parent">
                        <a href="#">Залы</a>
                        <ul class="uk-nav-sub">
                            <?foreach($gyms as $gym):?>
                                <?if($gym->getActive()):?>
                                    <li><a href="/gym/<?=$gym->getID()?>/"><?=$gym->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                    <li class="uk-parent">
                        <a href="#">Тренеры</a>
                        <ul class="uk-nav-sub">
                            <?foreach($trainers as $trainer):?>
                                <?if($trainer->getActive()):?>
                                    <li><a href="/trainer/<?=$trainer->getID()?>/"><?=$trainer->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
<? require_once ROOT . '/templates/layouts/footer_admin.php' ?>