<?
/** @var entities\Service[] $services */
/** @var entities\Gym[] $gyms */
/** @var entities\Trainer[] $trainers */
require_once ROOT . '/templates/layouts/header_admin.php'?>
<div class="uk-grid uk-grid-small uk-margin-top uk-margin-large-bottom">
    <div class="uk-width-4-5">
        <h1>Тренеры</h1>
        <ul class="uk-breadcrumb">
            <li><a href="/">Дионика</a></li>
            <li class="uk-active"><span>Тренеры</span></li>
        </ul>
        <div class="uk-grid uk-grid-small">
            <?foreach($trainers as $trainer):?>
                <?if($trainer->getActive()):?>
                    <div class="uk-width-1-4 uk-margin-bottom">
                        <div class="uk-panel uk-panel-box">
                            <div class="uk-panel-teaser">
                                <img src="<?=$trainer->getPreviewName() ? '/upload/' . $trainer->getPreviewName() : '/templates/images/placeholder_avatar.jpg'?>">
                            </div>
                            <p>
                                <a class="uk-button uk-button-primary" href="/trainer/<?=$trainer->getID()?>/">
                                    <?=$trainer->getName()?>
                                </a>
                            </p>
                            <p><?=$trainer->getIntro()?></p>
                        </div>
                    </div>
                <?endif;?>
            <?endforeach;?>
        </div>
    </div>
    <div class="uk-width-1-5">
        <div class="uk-panel-box">
            <ul class="uk-nav uk-nav-parent-icon uk-nav-side" data-uk-nav="">
                <li class="uk-parent">
                    <a href="#">Услуги</a>
                    <ul class="uk-nav-sub">
                        <?foreach($services as $service):?>
                            <?if($service->getActive()):?>
                                <li><a href="/service/<?=$service->getID()?>/"><?=$service->getName()?></a></li>
                            <?endif;?>
                        <?endforeach;?>
                    </ul>
                </li>
                <li class="uk-parent">
                    <a href="#">Залы</a>
                    <ul class="uk-nav-sub">
                        <?foreach($gyms as $gym):?>
                            <?if($gym->getActive()):?>
                                <li><a href="/gym/<?=$gym->getID()?>/"><?=$gym->getName()?></a></li>
                            <?endif;?>
                        <?endforeach;?>
                    </ul>
                </li>
                <li class="uk-parent uk-active">
                    <a href="#">Тренеры</a>
                    <ul class="uk-nav-sub">
                        <?foreach($trainers as $trainer):?>
                            <?if($trainer->getActive()):?>
                                <li><a href="/trainer/<?=$trainer->getID()?>/"><?=$trainer->getName()?></a></li>
                            <?endif;?>
                        <?endforeach;?>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<?require_once ROOT . '/templates/layouts/footer_admin.php'?>