<?
/** @var $errors[] */
/** @var entities\Trainer $trainer */
/** @var entities\User[] $usersWithRoleTrainer */
require_once ROOT . '/templates/layouts/header_admin.php'?>
    <div class="uk-grid uk-grid-small uk-margin-top uk-margin-large-bottom">
        <div class="uk-width-4-5">
            <h1>Добавить тренера</h1>
            <ul class="uk-breadcrumb">
                <li><a href="/">Дионика</a></li>
                <li><a href="/trainer/read/">Тренер</a></li>
                <li class="uk-active"><span>Добавить тренера</span></li>
            </ul>
            <?foreach($errors as $error):?>
                <div class="uk-alert uk-alert-danger" data-uk-alert="">
                    <a href="" class="uk-alert-close uk-close"></a>
                    <p><?=$error?></p>
                </div>
            <?endforeach;?>
            <form method="post" enctype="multipart/form-data">
                <div class="uk-form uk-form-stacked uk-margin-bottom">
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="name">Имя</label>
                        <div class="uk-form-controls">
                            <input type="text" id="name" placeholder="Имя" name="name" value="<?=$trainer->getName()?>">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="active">Активность</label>
                        <div class="uk-form-controls">
                            <input type="checkbox" id="active" name="active" <?=$trainer->getActive() ? 'checked' : ''?>>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="intro">Вступление</label>
                        <div class="uk-form-controls">
                            <input type="text" id="intro" placeholder="Вступление" name="intro" value="<?=$trainer->getIntro()?>">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="content">Контент</label>
                        <div class="uk-form-controls">
                            <textarea data-uk-htmleditor="{maxsplitsize:600}" data-uk-check-display="1" id="content" name="content"><?=$trainer->getContent()?></textarea>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="preview">Превью (250x400)</label>
                        <div class="uk-form-controls">
                            <input type="file" id="preview" name="preview">
                        </div>
                    </div>
                </div>
                <input type="submit" name="trainer_create" class="uk-button uk-button-success" value="Добавить">
            </form>
        </div>
        <div class="uk-width-1-5">
            <div class="uk-panel-box">
                <ul class="uk-nav uk-nav-parent-icon uk-nav-side" data-uk-nav="">
                    <li class="uk-parent">
                        <a href="#">Услуги</a>
                        <ul class="uk-nav-sub">
                            <?foreach($services as $service):?>
                                <?if($service->getActive()):?>
                                    <li><a href="/service/<?=$service->getID()?>/"><?=$service->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                    <li class="uk-parent">
                        <a href="#">Залы</a>
                        <ul class="uk-nav-sub">
                            <?foreach($gyms as $gym):?>
                                <?if($gym->getActive()):?>
                                    <li><a href="/gym/<?=$gym->getID()?>/"><?=$gym->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                    <li class="uk-parent">
                        <a href="#">Тренеры</a>
                        <ul class="uk-nav-sub">
                            <?foreach($trainers as $trainer):?>
                                <?if($trainer->getActive()):?>
                                    <li class="/trainer/<?=$trainer->getID()?>/"><a href="/trainer/<?=$trainer->getID()?>/"><?=$trainer->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
<?require_once ROOT . '/templates/layouts/footer_admin.php'?>