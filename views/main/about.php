<?
/** @var entities\Service[] $services */
/** @var entities\Gym[] $gyms */
/** @var entities\Trainer[] $trainers */
require_once ROOT . '/templates/layouts/header_admin.php'?>
    <div class="uk-grid uk-grid-small uk-margin-top uk-margin-large-bottom">
        <div class="uk-width-4-5">
            <h1>О нас</h1>
            <ul class="uk-breadcrumb">
                <li><a href="/">Дионика</a></li>
                <li class="uk-active"><span>О нас</span></li>
            </ul>
            <p>Дионика - идеальное место, где Вы сможете стать лучше. Клуб удобно расположен между улиц Чехова и Топольчанская в Солнечном районе г. Саратова и Вам будет до него комфортно добираться. </p>
            <p>Команда квалифицированных тренеров, просторные и светлые залы для занятий, самое современное оборудование - все это Вы найдете в клубе на Мамонтовой, 4 а. Все это поможет Вам стать лучше.</p>
            <p>Контактная информация:</p>
            <p>тел. 89371455745</p>
            <p>г. Саратов ул. Мамонтовой, 4 а</p>
            <p>Будни 7:00 - 23:00</p>
            <p>Выходные 09:00 - 21:00</p>
        </div>
        <div class="uk-width-1-5">
            <div class="uk-panel-box">
                <ul class="uk-nav uk-nav-parent-icon uk-nav-side" data-uk-nav="">
                    <li class="uk-parent">
                        <a href="#">Услуги</a>
                        <ul class="uk-nav-sub">
                            <?foreach($services as $service):?>
                                <?if($service->getActive()):?>
                                    <li><a href="/service/<?=$service->getID()?>/"><?=$service->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                    <li class="uk-parent">
                        <a href="#">Залы</a>
                        <ul class="uk-nav-sub">
                            <?foreach($gyms as $gym):?>
                                <?if($gym->getActive()):?>
                                    <li><a href="/gym/<?=$gym->getID()?>/"><?=$gym->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                    <li class="uk-parent">
                        <a href="#">Тренеры</a>
                        <ul class="uk-nav-sub">
                            <?foreach($trainers as $trainer):?>
                                <?if($trainer->getActive()):?>
                                    <li><a href="/trainer/<?=$trainer->getID()?>/"><?=$trainer->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
<?require_once ROOT . '/templates/layouts/footer_admin.php'?>