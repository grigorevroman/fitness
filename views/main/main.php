<?
/** @var entities\Service[] $services */
/** @var entities\Gym[] $gyms */
/** @var entities\Trainer[] $trainers */
require_once ROOT . '/templates/layouts/header_admin.php'?>
    <div class="uk-grid uk-grid-small uk-margin-top uk-margin-large-bottom">
        <div class="uk-width-4-5">

            <?if(count($services)):?>
                <p class="uk-h2" style="text-align: center">
                    <a style="color: #444;" href="/service/">Наши услуги</a>
                </p>
                <div class="uk-slidenav-position uk-overlay-active uk-margin-bottom" data-uk-slideshow="{kenburns:true}">
                    <ul class="uk-slideshow">
                        <?foreach($services as $service):?>
                            <?if($service->getActive()):?>
                                <li>
                                    <img src="<?=$service->getPreviewName() ? '/upload/' . $service->getPreviewName() : '/templates/images/placeholder1000x400.jpg'?>" width="" height="" alt="">
                                    <div class="uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
                                        <h3>Услуга: <?=$service->getName()?></h3>
                                        <p><?=$service->getIntro()?></p>
                                        <a class="uk-button uk-button-primary" href="/service/<?=$service->getID()?>/"><?=$service->getName()?></a>
                                    </div>
                                </li>
                            <?endif;?>
                        <?endforeach;?>
                    </ul>
                    <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
                    <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next"></a>
                    <ul class="uk-dotnav uk-dotnav-contrast uk-position-top uk-flex-center uk-margin-top">
                        <?
                        $i = 0;
                        foreach($services as $service):?>
                            <?if($service->getActive()):?>
                                <li data-uk-slideshow-item="<?=$i?>"><a href=""></a></li>
                            <?endif;?>
                            <?
                            $i++;
                        endforeach;?>
                    </ul>
                </div>
                <div class="uk-block uk-block-primary uk-contrast uk-margin-large-bottom">
                    <div class="uk-container">
                        <h3>Начни тренероваться прямо сейчас!</h3>
                        <div class="uk-grid uk-grid-match" data-uk-grid-margin="">
                            <div class="uk-width-medium-1-3 uk-row-first">
                                <div class="uk-panel">
                                    <p>Тренируясь в тренажерном зале, вы будете должным образом следить за своим здоровьем. Начнете правильно питаться, завяжете со своими вредными привычками.</p>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-3">
                                <div class="uk-panel">
                                    <p>Уверенность в себе. Без сомнений если вы будете иметь мускулистое, рельефное тело, то вы станете увереннее в себе. Ваши походка и взгляд станут смелее.</p>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-3">
                                <div class="uk-panel">
                                    <p>Тренажерка укрепляет нервную систему. Ученые доказали, что количество мышечной массы прямо пропорционально скорости нормализации кровяного давления.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?endif;?>

            <?if(count($gyms)):?>
                <p class="uk-h2" style="text-align: center">
                    <a style="color: #444;" href="/gym/">Наши тренажерные залы</a>
                </p>
                <div class="uk-slidenav-position uk-overlay-active uk-margin-bottom" data-uk-slideshow="{kenburns:true}">
                    <ul class="uk-slideshow">
                        <?foreach($gyms as $gym):?>
                            <?if($gym->getActive()):?>
                                <li>
                                    <img src="<?=$gym->getPreviewName() ? '/upload/' . $gym->getPreviewName() : '/templates/images/placeholder1000x400.jpg'?>" width="" height="" alt="">
                                    <div class="uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
                                        <h3>Зал: <?=$gym->getName()?></h3>
                                        <p><?=$gym->getIntro()?></p>
                                        <a class="uk-button uk-button-primary" href="/gym/<?=$gym->getID()?>/"><?=$gym->getName()?></a>
                                    </div>
                                </li>
                            <?endif;?>
                        <?endforeach;?>
                    </ul>
                    <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
                    <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next"></a>
                    <ul class="uk-dotnav uk-dotnav-contrast uk-position-top uk-flex-center uk-margin-top">
                        <?
                        $i = 0;
                        foreach($gyms as $gym):?>
                            <?if($gym->getActive()):?>
                                <li data-uk-slideshow-item="<?=$i?>"><a href=""></a></li>
                            <?endif;?>
                            <?
                            $i++;
                        endforeach;?>
                    </ul>
                </div>
                <div class="uk-block uk-block-primary uk-contrast uk-margin-large-bottom" style="background: #e5123d;">
                    <div class="uk-container">
                        <h3>Лучшее оборудование!</h3>
                        <div class="uk-panel">
                            <p>Основа основ любого тренажерного зала – это его спортивное оснащение. Главный ориентир в выборе – это качество, профессионализм и безопасность. Нашы тренажерные залы, оборудованы тренажерами, являющимися последними техническими разработками. Наши инструкторы выстроят программу занятий специально для Вас.</p>
                        </div>
                    </div>
                </div>
            <?endif;?>

            <?if(count($trainers)):?>
                <p class="uk-h2" style="text-align: center">
                    <a style="color: #444;" href="/trainer/">Наши тренеры</a>
                </p>
                <div class="uk-slidenav-position" data-uk-slider>
                    <div data-uk-slider>
                        <div class="uk-slider-container">
                            <ul class="uk-slider uk-grid-width-medium-1-4 uk-grid-small">
                                <?foreach($trainers as $trainer):?>
                                    <?if($trainer->getActive()):?>
                                        <li>
                                            <div class="uk-panel uk-panel-box">
                                                <div class="uk-panel-teaser">
                                                    <img src="<?=$trainer->getPreviewName() ? '/upload/' . $trainer->getPreviewName() : '/templates/images/placeholder_avatar.jpg'?>">
                                                </div>
                                                <p>
                                                    <a class="uk-button uk-button-primary" href="/trainer/<?=$trainer->getID()?>/">
                                                        <?=$trainer->getName()?>
                                                    </a>
                                                </p>
                                                <p><?=$trainer->getIntro()?></p>
                                            </div>
                                        </li>
                                    <?endif;?>
                                <?endforeach;?>
                            </ul>
                        </div>
                        <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slider-item="previous"></a>
                        <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slider-item="next"></a>
                    </div>
                </div>
            <?endif;?>

        </div>
        <div class="uk-width-1-5">
            <div class="uk-panel-box">
                <ul class="uk-nav uk-nav-parent-icon uk-nav-side" data-uk-nav="">
                    <li class="uk-parent">
                        <a href="#">Услуги</a>
                        <ul class="uk-nav-sub">
                            <?foreach($services as $service):?>
                                <?if($service->getActive()):?>
                                    <li><a href="/service/<?=$service->getID()?>/"><?=$service->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                    <li class="uk-parent">
                        <a href="#">Залы</a>
                        <ul class="uk-nav-sub">
                            <?foreach($gyms as $gym):?>
                                <?if($gym->getActive()):?>
                                    <li><a href="/gym/<?=$gym->getID()?>/"><?=$gym->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                    <li class="uk-parent">
                        <a href="#">Тренеры</a>
                        <ul class="uk-nav-sub">
                             <?foreach($trainers as $trainer):?>
                                 <?if($trainer->getActive()):?>
                                    <li><a href="/trainer/<?=$trainer->getID()?>/"><?=$trainer->getName()?></a></li>
                                 <?endif;?>
                             <?endforeach;?>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
<?require_once ROOT . '/templates/layouts/footer_admin.php'?>