<?
/** @var entities\Service[] $services */
/** @var entities\Gym[] $gyms */
/** @var entities\Trainer[] $trainers */
/** @var entities\FullGym $detailGym */
require_once ROOT . '/templates/layouts/header_admin.php'?>
    <div class="uk-grid uk-grid-small uk-margin-top uk-margin-large-bottom">
        <div class="uk-width-4-5">
            <h1><?=$detailGym->getName()?></h1>
            <ul class="uk-breadcrumb">
                <li><a href="/">Дионика</a></li>
                <li><a href="/gym/">Залы</a></li>
                <li class="uk-active"><span><?=$detailGym->getName()?></span></li>
            </ul>
            <div>
                <img src="<?=$detailGym->getPreviewName() ? '/upload/' . $detailGym->getPreviewName() : '/templates/images/placeholder1000x400.jpg'?>" class="uk-margin-bottom">
            </div>
            <div class="uk-margin-large-bottom"><?=$detailGym->getContent()?></div>
            <?if(count($detailGym->getServices())):?>
                <div class="uk-margin-large-bottom">
                    <p class="uk-h3">Услуги</p>
                    <?
                    /** @var entities\Service $service */
                    foreach($detailGym->getServices() as $service):?>
                        <div class="uk-grid uk-grid-small uk-margin-bottom">
                            <div class="uk-width-3-5">
                                <img src="<?=$service->getPreviewName() ? '/upload/' . $service->getPreviewName() : '/templates/images/placeholder1000x400.jpg'?>">
                            </div>
                            <div class="uk-width-2-5">
                                <p class="uk-h3"><?=$service->getName()?></p>
                                <p><?=$service->getIntro()?></p>
                                <a class="uk-button uk-button-primary" href="/service/<?=$service->getID()?>/">Смотреть</a>
                            </div>
                        </div>
                    <?endforeach;?>
                </div>
            <?endif;?>
        </div>
        <div class="uk-width-1-5">
            <div class="uk-panel-box">
                <ul class="uk-nav uk-nav-parent-icon uk-nav-side" data-uk-nav="">
                    <li class="uk-parent">
                        <a href="#">Услуги</a>
                        <ul class="uk-nav-sub">
                            <?foreach($services as $service):?>
                                <?if($service->getActive()):?>
                                    <li><a href="/service/<?=$service->getID()?>/"><?=$service->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                    <li class="uk-parent uk-active">
                        <a href="#">Залы</a>
                        <ul class="uk-nav-sub">
                            <?foreach($gyms as $gym):?>
                                <?if($gym->getActive()):?>
                                    <li class="<?=$gym->getID() === $detailGym->getID() ? 'uk-active' : ''?>"><a href="/gym/<?=$gym->getID()?>/"><?=$gym->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                    <li class="uk-parent">
                        <a href="#">Тренеры</a>
                        <ul class="uk-nav-sub">
                            <?foreach($trainers as $trainer):?>
                                <?if($trainer->getActive()):?>
                                    <li><a href="/trainer/<?=$trainer->getID()?>/"><?=$trainer->getName()?></a></li>
                                <?endif;?>
                            <?endforeach;?>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
<?require_once ROOT . '/templates/layouts/footer_admin.php'?>